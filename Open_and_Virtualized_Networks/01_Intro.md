# Intro

Assuming the reader to already have a base knowledge about networks, the goal of the present file is to introduce this class giving a recap of what we already should know.

## A switched network

Over the time, humanity has continuously tried to improve its ability to comunticate over long distances in the most efficient way possible. Computer technology has extremly improved long distance communication to the extent on aloowing anyone to real-time-communicate with the other side of the world.

To allow this, computer networks evolved to the modern **switched communication networks**. This technology is the base of the Internet. Its main features:

* Long distance communications between stations called **end device**;
* Switching nodes do not concern with content of data. Their purpose is to provide a switching facility that will move the data from node to node until they reach their destiination;
* A collection of nodes forms a **communications networks**.

### Switching nodes

* A node might be connected to other nodes, or to end devices.
* Network is usually partially connected; eventhough some redundant connections are desirable for reliability.
* There are two different switching technologies:
  * Circuit switching;
  * Packet switching.

* * *
Packet and circuit switching are non covered, since they should be known from computer networks class.
* * *

### Datagram features and issues

> For a recup, for a proper explaination, refer to RdC.

* Each packet is created independently of the others and having no reference to the packets sent before.
* A packet can take any possible route.
* Packet can be received out of order.
* Packets may be lost.
* It is up to the receiver to ask the transmitter for missing packets.
* It is up to the receiver to re-order packets received out of order.

## Software Defined Networking

The idea is to:

* Separate Control plane and Data plane entities;
* Execute or run Control plane software on general purpose hardware;
  * decouple from a specific networking hardware;
  * use commodity server;
* Have programmable data planes: the capability to mantain and program data plane state from central entity.
* An architecture to control the entire network.

### Why?

#### Issue #1

Networks are hard to manage:

* Computation and storage have been virtualized creating more flexible and manageable infrastractures;
* Networks management expect a large share of system admin staff.

#### Issue #2

Networks are hard to evolve:

* Ongoing innovation in systemps software such as new languages, operating systems;
* Networks are stuck in the past: routing algorithms change very slowly and networks management is extremely primkvitive.

#### Issue #3

Network design not based on formal principles:

* OS curses teach fundamental principles: mutual exclusion and other synchronization primitives, files, file systems, threads, and other building blocks;
* Networking courses teach a big bag of protocols: no formal principles, just general guidelines.

### Data plane and Control plane

* * *
TBD
* * *
