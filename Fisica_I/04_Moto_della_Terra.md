# Moto della Terra

Passiamo ora a studiare il moto della terra nel cosiddetto **sistema di riferimento delle stelle fisse**, cioè il SR solidale con quelle stelle talmente ontane da sembrarci fisse, anche se non lo sono. Tale SR è un SRnI.

Il sistema fisico in esame:

<div align="center">
    <img src="src/i10.png" width="350px" />
</div>

Consideriamo il raggio del pianeta (benche esso sia un geoide di rotazione, non una sfera) come 

$$R_T = 6400km,$$

la rotazione della Terra attorno al proprio asse (moto di rotazione) ha una velocità angolare:

$$\omega_{rot} = \dfrac{2π}{1d} \approx 7·10^{-5}s^{-1},$$

mentre quella attorno al Sole (moto di rivoluzione):

$$\omega_{riv} = \dfrac{2π}{1y} \approx 2 · 10^{-7}s^{-1}.$$

Non sorprendendemente:

$$\dfrac{\omega_{rot}}{\omega{riv}} = 365.$$

Ci riferiremo al sistema di riferimento delle stelle fisse come $SR$ e il sistema di riferimento solidale alla Terra con $SR'$. In oltre, fissiamo $o = o'$ il centro della Terra e assumiamo:

$$
    \vec a_{o'}(t) = 0, \;\;\;
    \dot{\vec\omega}_{rot}(t) = 0, \;\;\;
    \forall t.
$$

Consideriamo solo la forza centrifuga e la forza di Coriolis:
$$
    \vec F_{cen}(t) = -m\vec\omega_{rot}(t) \times \Big(
        \vec\omega_{rot}(t) \times \vec r'(t)
    \Big) \\\;\\
    \vec F_{cor} = -2m\vec\omega_{rot}(t) \times \vec v'(t)
$$

## Latitudine e longitudine

<div align="center">
    <img src="src/i11.png" width="350px" />
</div>

* $\theta$ è misura della distanza dall'equatore;
* $\phi$ è misura della distanza dal meridiano di Greenwich.

## Dipendenza di $g$ da $\theta$

considerando $r$ la distanza della massa $m$ dall'asse di rotazione della Terra e $\hat R$ il versore che applicato alla massa punta verso il centro della Terra, nonché $\hat\theta$ che, sempre applicato alla massa, punta verso l'equatore ed è normale a $\hat R$.

Nel SR delle stelle fisse, un corpo di massa $m$ sulla superficie terrestre è soggetto a

$$ \vec a = g_0 \hat R. $$

Vogliamo dimostrare che

$$ \vec a = g \approx g_0\Big( 1 - \dfrac{\omega_{rot}^2R_T}{g_0} cos^2 \theta \Big) $$

chiaramente $g = g_0 \Leftrightarrow \theta = \dfrac π2$.

> Ci poniamo in $\theta = \dfrac π2$

## La forza centrifuga
> Cosa succede se il corpo è in quiete?

Se il corpo è in quiete sulla superficie terrestre, sarà sufficiente studierne la forza centrifuga, giacché per velocità nulle, l'accelerazione di Coriolis sarà nulla.

$$
    \vec g := \vec a' = g_o \hat R - \vec a_{cen}
$$

laddove
$$
    \vec a_{cen} = \vec\omega_{rot} \times \Big( \vec\omega_{rot} \vec \times R \Big) = -\omega_{rot}^2 R_T \cos\theta \hat r
$$

Dunque possiamo riscrivere l'accelerazione centripeta in coordinate polari:
$$
    \vec a_{cen} = \vec a_{cen, R} + \vec a_{cen, \theta}
    \\\;\\
    \vec a_{cen, R} = a_{cen} \cos\theta \hat R = \omega_{rot}^2R_T\cos^2\theta R
    \\\;\\
    \vec a_{cen, \theta} = a_{cen} \cos\Big(\dfrac π2 + \theta\Big) \hat\theta = -\omega_{rot}^2 R_T \cos\theta\sin\theta\hat\theta
$$

Da cui:
$$
    a' = \sqrt{a_R + a_\theta^2} =
    \sqrt{
        \big(g_0 - \omega_{rot}^2R_T\cos^2\theta\big)^2 +
        \omega_{rot}^4R_T^2\sin^2\theta\cos^2\theta
    } =
    \\\;\\
    \sqrt{
        g_0^2 - 2\omega_{rot}R_T\cos^2\theta + \omega_{rot}^4R_T^2\cos^4\theta + \omega_{rot}^4R_T^2\sin^2\theta\cos^2\theta
    } =
    \\\;\\
    \sqrt{
        g_0^2 - 2\omega_{rot}R_T\cos^2\theta + \omega_{rot}^4R_T^2\cos^2\theta \cancel{\big( \cos^2\theta + \sin^2\theta \big)}
    } =
    \\\;\\
    \sqrt{ g_0^2 \Big(
        1 - \dfrac{2\omega_{rot}R_T}{g_0}\cos^2\theta +
        \dfrac{\omega_{rot}^4R_T^2}{g_0^2}\cos^2\theta
    \Big) }
$$

Essendo $\omega^4 \rightarrow 0$:
$$
    \sqrt{ g_0^2 \Big(
        1 - 2\dfrac{\omega_{rot}R_T}{g_0}\cos^2\theta
    \Big) }
    \;\Bigg|_{\omega_{rot} \approx 3·10^{-3}}

$$

Sviluppiamo con Taylor la funzione $f(x) = \sqrt{1 + x}$ per $x = -2 \dfrac{\omega_{rot}R_T}{g_0}\cos^2\theta$:

$$
    f(x) = 1 + \dfrac 12x + O(x^2);
$$

da cui l'approssimazione:

$$
    g \approx g_0 \Big(
        1 - \dfrac{\omega_{rot}^2R_T}{g_0}\cos^2\theta
    \Big).
$$

Ai poli troviamo $g_0 = 9,83\;m/s^2$, all'equatore $g_0 = 9,78\;m/s^2$.

## Forza di Coriolis

Consideriamo i versori $\hat x, \;\hat y$ e $\hat z$ che puntano rispettivamente verso Ovest, Nord e verso il centro della Terra.

L'agolo $\angle(\hat x, \hat y)$ sarà dunque $\dfrac π2$.

Supponiamo che un grave di massa $m$ cada con accelerazione $a'(t) = \vec a - \vec a_{cor}$.

Calcoliamo:
$$
    \vec a'(t) =
    \vec a - \vec a_{cor} =
    \vec g - 2\omega_{rot} \times \vec v'(t) =
    \vec g - 2\omega_{rot} \times \Big(
        \vec v(t) - \omega_{rot} \times\vec r'(t)
    \Big) =
    \\\;\\
    \vec g - 2\omega_{rot} \times \vec gt + 2\omega_{rot} \times \Big(
        \omega_{rot} \times\vec r'(t)
    \Big) \approx
    \vec g - 2\omega_{rot} \times \vec gt
$$

Il prodotto vettoriale va svolto per forza componente-a-componente:
$$
    \vec a = \vec g - \vec h \\
    a_x = g_x - h_x = -h_x =
    -\Big( ω_xg_z \cancel{- ω_zg_y} \Big)t = -ω_xg_zt \\
    a_y = g_y - h_y = -h_y =
    -\cancel{\Big( ω_zg_x - ω_xg_z \Big)t} = 0 \\
    a_z = g_z - h_z = g - h_z =
    g - \cancel{\Big( ω_xg_y - ω_yg_x \Big)t} = g \\
$$

Da cui:
$$
    x(t) = ∫_0^t ∫_0^{t'} a'_x dt'' dt' =
    - \dfrac 13 \omega_{rot} g t^3 \cos\theta,
$$

nonché la già nota:
$$
    z(t) = \dfrac 12 g t^2 - h
$$
