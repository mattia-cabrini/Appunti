\section{Momento e dintorni}

\paragraph{def.} Dato un vettore $\vec a$ applicato in un punto $P$ di un SR in cui fissiamo un punto $O$, si dice \textbf{momento di $\vec a$ rispetto ad $O$}:
$$
    \vec c_O := \vec r_{OP} \times \vec a.
$$

Possiamo poi studiare il modo in cui varia il momento di $\vec a$ in funzione della variazione del polo (chiamiamo \textit{polo} il punto $O$). Supponendo di spostare il polo da $O$ a $O'$ e dunque di avere due distinti moti di $\vec a$:
$$
    \vec c_{O} = \vec r_{OP} \times \vec a
$$
$$
    \vec c_{O'} = \vec r_{O'P} \times \vec a
$$

Studiamo:
$$
    \vec c_{O} = \vec r_{OP} \times \vec a =
    \Big(\vec r_{O'P} + \vec r_{OO'}\Big) \times \vec a =
    \vec r_{OO'} \times \vec a +
    \vec r_{O'P} \times \vec a
$$
$$ 
    \Longrightarrow
    \vec c_O = \vec r_{O'O} \times \vec a + \vec c_{O'}
$$

\subsection{Il momento angolare}

\paragraph{def.} Si dice \textbf{momento angolare} di un punto in moto rispoetto a un polo $O$ il vettore
$$
    \vec L(t) := \vec r(t) \times m\vec v(t) = \vec r(t) \times \vec p(t).
$$

\paragraph{In coordinate polari}
$$
    \vec L(t) = \vec r(t) \times \Big(
        \vec v_r(t) + \vec v_\theta(t)
    \Big) \stackrel{con \; \vec r \; ld \;\vec v_r}{=}
    m\vec r(t) \times \vec v_\theta(t) = \vec L_\theta(t)
$$

\paragraph{Unità di misura del momento angolare}
$$
    \vec L = \Big[ r \; m \; v \Big] = \Big[ L^2MT^{-1} \Big] = \bigg[ \dfrac {kg \; m^2}s \bigg] = \Big[ Js \Big]
$$

\subsection{Momento di una forza}
\paragraph{def.} Si definisce \textbf{momento di una forza} il vettore
$$
    \vec M(t) := \vec r(t) \times \vec F(t)
$$

\paragraph{Unità di misura del momento di una forza} Le \textbf{unità di misura} del momento angolare e del momento di una forza sono diverse:
$$
    \vec M = \Big[ r \; F \Big] = \Big[ L^2MT^{-2} \Big] = \bigg[ \dfrac {kg \; m^2}{s^2} \bigg] = \Big[ N \; m \Big]
$$

\subparagraph{Attenzione!} Non utilizzare $J$! Benché l'unità di misura sembri la stessa, il momento di una forza è un vettore, non uno scalare. In oltre, ricordiamoci che il \textbf{Joule} è unità di misura del lavoro o dell'energia: il momento non rappresenta né l'uno né l'altra.

\subparagraph{Chiarimento} L'ambiguità deriva dal fatto che gli angoli sono a-dimensionati. La corretta unita di misura del momento di una forza sarebbe $\dfrac{N \; m}{rad} = N \; m$. L'a-dimensionalità degli angoli fa sembrare il momento come una forma di energia o di lavoro, ma non lo è.

\subsection{Varianti}
\paragraph{Momento della forza risultante} Supponiamo che a una massa siano applicate $n$ forze $F_1$, $F_1$, $\dots$, $F_n$ e chiamiamo la risultante delle forza $R(t) = \sum_{i = 0}^n F_i(t)$. Il momento sarà:
$$
    \vec M(t) = \vec r(t) \times \vec R(t) =
    \vec r(t) \times \sum_{i = 0}^n F_i(t) =
    \sum_{i = 0}^n \vec M_i(t)
$$

\paragraph{Cambiamento di polo}
$$
    \vec M_{O'}(t) =
    \vec M_O(t) + \vec r_{O'O} \times \vec F(t)
$$

\subsection{Teorema del momento angolare}
\paragraph{Osservazione} Vale sempre che
$$
    \dfrac d{dt} \vec L(t) = \dfrac d{dt} \Big(
        \vec r(t) \times m \vec v(t)
    \Big) =
    m \Big( \cancel{ \dfrac {d\vec r(t)}{dt} \times \vec v(t) } +
        \vec r(t) \times \dfrac {d\vec v(t)}{dt}
    \Big)
$$

Quindi se il polo è un punto fisso nel sistema SR:
$$
    \dfrac d{dt} \vec L(t) =
    m \vec r(t) \times \dfrac {d\vec v(t)}{dt}
$$

\paragraph{Teorema vero e proprio} Se ci troviamo in un SRI, $\dfrac {d\vec v(t)}{dt} = \vec a(t) = \dfrac {\vec F}{m}$, sostiduendo:
$$
    \dfrac d{dt} \vec L(t) =
    \vec r(t) \times \vec F(t) = \vec M(t)
$$

\paragraph{Interpretazione geometrica di $L$}

\begin{figure}
    \centerline{\includegraphics[width=1.25in]{src/i12.png}}
    \caption{Rappresentazione grafica dello pseudo-vettore $\vec L(t).$}
\end{figure}

Chiamata $\mathcal A(\Delta\theta)$ l'area spaziata in un intervallo $\Delta\theta$, sappiamo che $\Delta s = r\Delta\theta$ e che quindi:

$$
    \mathcal A(\Delta\theta) = \dfrac 12 r(t) \Delta s(t) =
    \dfrac 12 r(t) \Big[ r(t) \Delta\theta(t) \Big] = \dfrac 12 r^2(t) \Delta\theta(t) \stackrel{def. \; \omega(t)}{\Longrightarrow}
    d\mathcal A(t) = \dfrac 12 r^2(t) \omega(t) dt
$$

\paragraph{def.} Si definnisce la \textbf{velocità areolae} di un corpo in moto rispetto a un polo:
$$
    \dfrac d{dt} \mathcal A(t) = \dfrac 12 r^2(t) \omega(t) = \dfrac 12 r(t) v(t) = \dfrac{L(t)}{2m}
$$

\paragraph{Conservazione del momento agolare} In un SRI, se $\vec R(t) = \vec{0}$, il momento angolare si conserva:
$$
    \vec R(t) = \vec 0 \Longrightarrow \dfrac{d}{dt} \vec L(t) =
    \vec M(t) = \vec 0
$$

Notare che \textbf{non} vale l'implicazione contraria.

\paragraph{Significato o interpretazione della legge di conservazione del momento}
\begin{list}{-}{}
    \item \textbf{Conservazione dell'energia}: l'energia meccanica $E_{mecc}$ si conserva: il lavoro delle forza dissipative è nullo.
    \item \textbf{Conservazione momento "lineare"}: la risultante delle forza in un SRI è nulla (2NL).
    \item \textbf{Conservazione del momento angolare}: momento risultante nullo.
\end{list}

Tutte queste leggi sono utili per risolvere alcuni modelli senza dovre utilizzare equazioni differenziali.

\subsection{Formule utili per il moto curvilineo}
\paragraph{} Moto circolare con raggio $r$ e massa $m$ e con polo nel piano di rotazione
$$
    L = mrv = m\omega r^2.
$$

Spostamento da $A$ a $B$:
$$
    W_{AB} = \int_A^B \vec F(t) d\vec s = \int_A^B \Big(
        \vec F_\tau(t) + \vec F_\tau(t)  
    \Big) d\vec s =
$$
$$
    \int_A^B \vec F_\tau(t) d\vec s =
    \int_{\theta_A}^{\theta_B} r \vec F_\tau(t) d\theta = \int_A^B M(\theta) d\theta
$$

\subsection{Forza centrale}
\paragraph{def.} Si dice che una forza agente su una porzione di spazio è \textbf{centrale} se è descrivibile come $\vec F: \mathbb R^3 \rightarrow \mathbb R^3$, campo vettoriale $\big[$ Il che significa che a ogni punto dello spazio $(x, \; y, \; z)$ è associato un valore $\vec F(x, y, z)$. $\big]$, tale per cui la direzione passi sempre per lo stesso punto $O$ — detto \textit{centro della forza} — e il modulo dipenda solo dalla posizione $\big[$ Il che significa che $\vec F(\vec r)$ si può scrivere come $\vec F(\vec r)) = F(r) \hat r$ $\big]$.

Alcuni esempi di forze centrali sono: la forza di gravità; la forza elettrostatica, la tensione di una fune che funge da braccio per un moto circolare uniforma; e altre.

\paragraph{Forze centrali e momenti} Se una forza è centrale, il momento angolare del moto da essa causato, con polo nel centro della forza stessa, si conserva.
$$
    \vec F = F(r) \hat r \Longleftrightarrow
    \vec F \parallel \vec r \Longrightarrow
    \vec M_O = 0 \Longrightarrow \dfrac{d}{dt} \vec{L} = 0.
$$

Se il polo è centro della forza, $\vec L$ si conserva. Ne consegue che un moto in un piano definito da $\vec r$ e $\vec v$, con $\vec L\perp \vec r$, $\vec v$ ha velocità areolare costante: $\dot {\mathcal A}(t) = \mathcal A$.

\subsubsection{Il lavoro di una forza centrale}
$$
    W_{AB} = \int_A^B \vec F(\vec r) d\vec s =
    \int_A^B = F(t) \hat r \Big( ds_r\hat r + ds_\theta \hat \theta \Big).
$$

Laddove è stata scomposta $\vec F$ e considerata in coordinate polari dal centro della forza.
$$
    W_{AB} = \int_A^B F(r) ds_r = F(r_B) - F(r_A).
$$

Questo dimostra che \textbf{le forze centrali sono conservative}.

\subsubsection{Superfici equipotenziali}
Essendo le forze centrali conservative:

\begin{enumerate}
    \item ad esse può essere associata una funzione \textit{energia potenziale} $E_p$;
    \item il modulo della forza dipende esclusivamente dalla distanza dal centro della forza $\vec r$.
\end{enumerate}

Questo significa che esistono infiniti punti dello spazio in cui — essendo essi siti alla medesima distanza dal centro della forza — il modulo della forza $\vec F$ è costante; e non solo è costante il modulo $F$, ma anche l'energia potenziale $E_p$. L'insieme di questi punti forma una superficie che prende il nome di \textbf{superficie equipotenziale}.
