\section{Gravitazione}

\subsection{Leggi di Keplero}

\paragraph{Prima legge di Keplero} I pianeti percorrono orbite ellittiche intorno al sole, il quale, occupa uno dei due fuochi.
\paragraph{} Essendo la forza di gravità una forza centrale, il suo moto angolare si conserva, dunque il moto è planara.

\paragraph{Seconda legge di Keplero} I pianeti spaziano aree uguali in tempi uguali. Che nient'altro significa se non
$$
    \dot{\mathcal A}(t) = \dfrac 12 r^2(t) \omega(t) = k
$$

Da cui ricaviamo che la velocità del pianeta è massima in perielio e minima in afelio.

\paragraph{def.} \textbf{Afelio}: punto della traiettoria del pianeta più vicino al Sole.
\paragraph{def.} \textbf{Perielio}: punto della traiettoria del pianeta più lontano al Sole.

\paragraph{Terza legge di Keplero}
$$
    T^2 = kr^3, \;\;\; con \; T = \dfrac{2\pi}\omega
$$
Laddove $r$ non rappresenta un raggio, ma il semiasse maggiore dell'ellisse traiettoria.

\subsection{Legge di fravitazione universale di Newton}

\paragraph{Il nostro obiettivo} Deduciamo l'espressione dellla forza di attrazione gravitazionale universale dalle leggi di Keplero.

\paragraph{Sulle approssimazioni} Per semplicità, consideriamo orbite circolari. Anche se questa potrebbe sembrare un'approssimazione importante, dato che cambia la natura del sistema fisico, la possiamo comunque fare, giacché le orbite dei pianeti, benché ellittiche, sono quasi circolari.

\paragraph{} L'ipotesi dell'orbita circolare, insiema alla K2L, implica che il moto sia circolare uniforme:
$$
    \dfrac{d}{dt}\mathcal A(t) = \dfrac 12 r^2 \dfrac{d\theta}{dt} =
    const. \Longrightarrow \dfrac{d\theta}{dt} = const. \Longrightarrow
    \vec F = \vec F_n = m\omega^2r(-\hat r) \Longleftrightarrow
    \omega = \dfrac{d\theta}{dt}.
$$

\subparagraph{Applicando K3L}
$$
    F = m\omega^2r =
    m\bigg(\dfrac{2\pi}{T}\bigg)^2r = \dfrac{4\pi^2mr}{T^2}
    \;\land\; T^2 = kr^3 \Rightarrow
    F = \dfrac{4\pi^2m\cancel r}{kr^{\cancel 32}} =
    \dfrac{4\pi^2m}{kr^2}
$$

\subparagraph{Applicando N3L} Applichiamo ora la 3${}^a$ legge di Newton (azione-reazione); sapendo che $\vec F_{1, 2} = -\vec F_{2, 1}$:
$$
    F_{1, 2} = F_{2, 1} \Longleftrightarrow
    \dfrac{\cancel{4\pi^2}m_1}{k_{1,2}\cancel{r_{1,2}^2}} =
    \dfrac{\cancel{4\pi^2}m_2}{k_{2,1}\cancel{r_{2,1}^2}}
    \Longleftrightarrow m_1k_{2,1} = m_2k_{1,2}
$$

\subparagraph{La costante universale $G$} Definiamo la costante di gravitazione universale $G$, talvolta indicata con $\gamma$ nel seguente modo:
$$
    m_1k_{2,1} = \dfrac{4\pi^2}{G} \Longleftrightarrow
    4\pi^2 = Gm_1k_{2,1} = Gm_2k_{1,2}
$$

\subparagraph{Sostituzioni} Con un po' di algebra, si sostituisce in $F_{1,2}$ la costante $4\pi^2 = Gm_2k_{1,2}$:
$$
    F_{1,2} = \dfrac{4\pi^2m}{r^2} =
    G\dfrac{\cancel{k_{1,2}}m_1m_2}{\cancel{k_{1,2}}r_{1,2}^2} =
    G\dfrac{m_1m_2}{r^2} = F_{2,1}.
$$

\paragraph{La legge gravitazionale universale} Abbiamo così ottenuto la legge di gravitazione universale:
$$
    F_{1,2} = G \dfrac{m_1m_2}{r_{1,2}^2}.
$$

\subsection{La costante di gravitazione universale} Chiamiamo $G$ la costante di gravitazione universale. Essa vale\footnote{Frutto di misurazione sperimentale. La prima misurazione è attribuita a Cavendish, 1798}:
$$
    G = 6,67 \cdot 10^{-11} kg^{-1} m^3 s^{-2}
$$

\subsection{Gravitazione: stessa forza, diversi effetti}
\paragraph{} Vengono elencati di seguito gli effetti dovuti alla gravitazione sulle masse $m_1$ ed $m_2$:
\begin{list}{-}{}
    \item $\vec F_{1,2} = -\vec F_{2,1}$, nonché $F_{1_2} = F_{2,1}$
    \item $F_{1,2} = F_{2,1} \Longleftrightarrow m_1a_1 =  m_2a_2 \Longleftrightarrow a_1 = \dfrac{m_2a_2}{m_1}$, Questo significa che per $m_1 << m_2$ si osserva $a_1 >> a_2$.
    Nel caso Sole-Terra, si ha $m_T \approx 10^{24} kg$ e $m_S \approx 10^{30} kg$.
    \item Essendo $\vec F$ centrale: $\hat F = - \hat r$.
    \item Data la legge di gravitazione universale [UGL]: $F(r) \propto r^{-2}$.
\end{list}

\subsection{Calcolo di $g$ dal ciclo lunare}
\paragraph{L'idea} Data la costanza di $g$, essa può essere calcolata da qualunque corpo attratto dalla Terra, non solo dai gravi; questo anche senza conoscere $G$ o $m_T$.

\subparagraph{I calcoli}
$$
    F_{TL} = G\dfrac{m_1m_2}{r_{TL}^2} =
    m_L a_{n, L} = m_L \dfrac{v^2}{R_{TL}} =
    m_L a_{n, L} = m_L \omega^2 R_{TL} \Longrightarrow
$$
$$
    \dfrac{Gm_T\cancel{m_L}}{r_{TL}^2} = \cancel{m_L} \omega^2 R_{TL}
    \Longrightarrow Gm_t = \omega^2 r_{TL}^3
$$

\subparagraph{Periodo di rivoluzione} Assumiamo il periodo di rivoluzione della luna pari a $T_L = \dfrac{2\pi}{\omega_L} \approx 27d$.

\subparagraph{Distanza Terra-Luna} Assumiamo la distanza Terra-Luna $r_{TL} \approx 3,8 \cdot 10^8m$.

\subparagraph{Sostituzioni} Sodtituiamo ora i valori che abbiamo assunto sulla base di osservazioni astronomuche:
$$
    Gm_T = \dfrac{4 \pi^2 \cdot 3,8^3 \cdot 10^{24}}
    {27^2 \cdot 24 \cdot 3,6^2 \cdot 10^6} =
    \dfrac{2,2 \cdot 10^{\cancel{27}15}}{5,4 \cdot 10^{\cancel{12}0}} =
    4 \cdot 10^{14} \Big[\dfrac{m^3}{s^2}\Big] \stackrel{da\;disp.}{=} 5,12 \cdot 10^{14}
$$

\subparagraph{Il risultato}
$$
    g = G\dfrac{m_T}{r_T^2} =
    \dfrac{4 \cdot 10^{14}}{\big(6,37 \cdot 10^6\big)^2} = 9,85 m/s^2
    \stackrel{appros.\;di}{\approx} 9,81 m/s^2
$$

\subsection{Energia potenziale gravitazionale}
\paragraph{L'energia potenziale} Essendo $F_g$ una forza centrale, e dunque conservativa, possiamo determinarne l'energia potenziale $E_p$.
\paragraph{I conti}
$$
    \vec F_{1,2}(\vec r_{1,2}) = -\vec\nabla E_p(\vec r_{1,2}) =
    -\dfrac{dE_p}{r}
$$
\subparagraph{Da cui}
$$
    E_p(\vec r_{1,2}) =
    - \int_0^r G\dfrac{m_1m_2}{r_{1,2}^{\prime2}} dr^\prime =
    -G\dfrac{m_1m_2}{r_{1,2}}.
$$

\paragraph{Caratteristiche}
\begin{list}{-}{}
    \item Come si può vedere dalla formula: $E_p(r) \propto r^{-1}$.
    \item Sempre dall'espressione: $\lim_{r \rightarrow +\infty} E_p(r) = 0$.
    \item È meno immediato ma è importante osservare che $0 < r < +\infty \Longrightarrow E_p(r) = 0$. Questo è per certi versi controintuitivo: significa che più le due masse si avvicinano, minore è l'energia potenziale. Potrebbe non essere immediato, ma a pensarci un attimo ha perfettamente senso. Il fatto che l'$E_p$ si riduca con l'avvicinarsi delle masse significa che viene compiuto un lavoro positivo in termini di spostamento, cioè in termini di energia cinetica.
    \item Considerando un sistema fisico in cui due corpi si attraggono, risulta evidente che $\Delta E_k = -\Delta E_p$.
    \item $E_k(t\rightarrow\infty) = \dfrac 12m_1v^2(\infty) \Longrightarrow \dfrac 12m_1v^2(\infty) =$\\
    $\dfrac 12m_1v^2(r_{1,T}) + E_p(r_{1,T}) = \dfrac 12m_1v^2(r_{1,T})-G\dfrac{m_1m_2}{r_{1,T}} \leq E_k(r_{1,T}).$
\end{list}

\subsection{Il campo gravitazionale}
\paragraph{def.} Prende il nome di \textbf{campo gravitazionale generato da}, o dovuto a, \textbf{una massa $m$}:
$$
    \vec G_1 := \dfrac{\vec F_{1,i}}{m_i} = -G \dfrac{m_1}{r_i^2} \hat r.
$$

Tale "campo" dipende unicamente dalla massa che ne è causa.

\paragraph{Un'interpretazione} Un modo per intendere il campo gravitazionale è il seguente: il campo non è altro che una relazione tra un punto dello spazio e la componente della forza gravitazionale che agirebbe su una massa che si trovasse in quel punto. Vale a dire che una massa che si trovi in un punto $\vec r_2$ sarà attratta dal corpo $m_1$ con una forza $F_{1,2} = \vec G(\vec r) m_2$.

\paragraph{Per cultura} In generale, per studiare i fenomeni dovuti alle forze non di contatto, viene definito un campo (gravitazionale, elettrostatico, di induzione magnetica).

\subsubsection{Il potenziale del campo gravitazionale}
\paragraph{} Così come è stato definito il campo gravitazionale, è possibile definire il campo gravitazionale. Si fa come per il campo $\vec G$. Il potenziale del campo graviazionale $\vec G$ è dato da:
$$
    V_1(r) := \dfrac{E_{p,i}}{m_i} = - G \dfrac{m_1}r.
$$

\paragraph{Ralazione tra $\vec G$ e $\vec V$} Vale la relazione:
$$
    \vec G_1(t) = - \vec\nabla V_1(r).
$$

\paragraph{Dove si può muovere un corpo?} Sappiamo che $E_{mecc} = E_k + E_p = \dfrac 12 m v^2 + E_p(\vec r)$. Sapendo anche che $E_k \geq 0$, otteniamo l'importante relazione:
$$
    E_p(\vec r) \leq E_{mecc}.
$$

\subparagraph{} Questo significa che un corpo può muoversi solo laddove $E_P(\vec r) \leq E_k$.

\subsection{Equazioni del moto in presenza di forza centrale}
\paragraph{} Dopo aver visto un esempio importante di forza cetrale, studiamo ora le equazioni del moto — e dunque anche il moto — docuto alla presenza di una forza centrale.
\paragraph{Impostazione del problema} Per prima cosa, essendo la forza diretta lungo la direzione radiale, ci conviene passare alle coordinate polari. Chiamiamo $O$ il centro della forza e $\theta$ l'angolo formato tra $O$, $\vec r$ e l'asse osrizzontale. Chiamiamo $r$ la distanza tra $O$ e la massa di cui vogliamo studiare il moto.

\subparagraph{Velocità in coordinate polari}
$$
    \vec v = \vec v_r + \vec v_\theta =
    \dot r \hat r + r \dot\theta \hat\theta;
$$
$$
    L = mrv_\theta = mr^2\dot\theta \Longrightarrow
    L^2 = m^2r^4\dot\theta^2;
$$
$$
    E_k = \dfrac 12mv^2 = \dfrac 12m \Big(\dot r^2 + r^2\dot\theta^2\Big)
    = \dfrac 12m\hat r^2 + \dfrac{L^2}{2mr^2};
$$
$$
    E_{mecc} = E_p + \dfrac 12m\dot r^2 + \dfrac{L^2}{2mr^2};
$$
$$
    \dot E_{mecc} = \dot E_p + \dfrac 12m2\dot r\ddot r +
    \dfrac{L^2}{2m}\dfrac{-2}{r^3}\dot r = 0.
$$

\subparagraph{NB} Nell'ultima espressione è comparsa l'accelerazione, che è ciò che vogliamo trovare:
$$
    \dfrac{dE_P}{dr} \cancel{\dfrac{dr}{dt}} + 
    m\cancel{\dot r}\ddot r - \dfrac{L^2}{mr^3}\cancel{\dot r}
    \Longrightarrow m \ddot r = \dfrac{L^2}{mr^3} - \dfrac{dE_p}{dr}
$$

\subsection{Energia potenziale efficacie}
\paragraph{def.} Prende il nome di \textbf{energia potenziale efficacie}
$$
    E_{eff} := E_p + \dfrac{L^2}{2mr^2}.
$$

\subparagraph{Da cui}
$$
    E_{mecc} = \dfrac 12 m \dot r^2 + E_{eff}.
$$

\subparagraph{Osservazione} La conservazione di $E_{mecc}$ trasforma il problema da 3D a 1D. Il moto lungo $\hat r$ è infatti determinato studiando il potenziale efficacie $E_{eff}(r)$.\\
In particolare notiamo che per $r \rightarrow 0$, $E_{eff} = \dfrac{L^2}{2mr^2} \rightarrow \infty$. Questo significa che se un pianeta si avvicina al sole, la forza gravitazionale indotta tra le diue masse è repulsiva e $\lim_{r \rightarrow 0} \vec F = +\infty$. Questo impedice l'avvicinamento dei pianeti al sole.

\paragraph{Momento angolare costante} Risulta chiaro come all'avviginarsi ($r \rightarrow 0$) del pianeta al Sole, la velocità debba necessariamnte aumentare; tuttavia la conservazione del momento angolare deve necessariamente limitare la crescita di $v$ e — dunque — la decrescita di $r$.

\paragraph{Regione ammessa} Possiamo studiare attraverso $E_{eff}$ le regioni in ci è ammesso il moto. In particolare sappiamo che
$$
    \dfrac 12 m \dot r^2 = E_{mecc} + E_{eff}
$$
Il che significa che se $r^2 > 0$, allora $E_{mecc} \geq E_{eff}$.

\subparagraph{Caso del momento angolare nullo} Ovviamente se $L = 0$, $E_{mecc} = E_k + E_p > E_p = E_{eff}$, $\forall r$.

\subparagraph{Caso del momento angolare non nullo} Se $L \neq 0$, possiamo calcolare il minimo di $E_{eff}$:
$$
    \dfrac{d}{dt} E_{eff} = \dfrac{d}{dr} \dfrac{L^2}{2mr^2} -
    \dfrac{d}{dr} \Big(\dfrac{GMm}{r}\Big) = 0 \Longrightarrow
    r_{min} \dfrac{L^2}{GMm^2} \Longrightarrow
    E_{eff, min} = -\dfrac{G^2M^2m^3}{2L^2} < 0.
$$
Questo significa che quando l'energia cinetica è minima ($E_{mecc} = E_{eff}$), il pianeta è vincolato a muoversi a una certa distanza dal centro della forza, cioè è costretto in un orbita circolare.

\paragraph{In sintesi} A seconda di quale sia la relazione tra $E_{mecc}$ e $E_{eff}$, sono sintetizzate di seguito:
\begin{list}{-}{}
    \item $E_{mecc} > E_{eff, min} \Longrightarrow r_{min} < r \leq r_{max}$, il che significa che l'orbita è vincolata a una certa fascia.
    \item $E_{mecc} < 0 \Longleftrightarrow r_{max} < \infty$, $r(t)$ funzione periodica.
    \item $r_{max} = \infty$ , traiettoria aperta.
    \item $E_{mecc} = \dfrac{GmM}{3\epsilon d}\Big(\epsilon^2 - 1\Big)$, $\epsilon$: eccentricità dell'orbita.
\end{list}

\begin{figure}
    \centerline{\includegraphics[width=3in]{src/i13.png}}
    \caption{Orbita dei pianeti in base alla relazione tra $E_{mecc}$ e $E_{eff}$.}
\end{figure}

\subsection{eorema di Gauss}

\paragraph{Perché} Il teorema di Gauss permette di semplificare la trattazione dell'interazione gravitazionale e in generale di tutte le forze $F(r) \propto \dfrac 1{r^2}$, riducendo la dinamica di corpi con dimensioni significative (come per esempio i pianeti) alla dinamica dei punti materiali.

\paragraph{A cosa serve} Il teorema di Gauss, concretamente riguarda la misura dell'intensità del campo gravitazionale generato da sistemi di corpi massivi in una certa porzione di spazio.

\paragraph{Flusso di un campo vettoriale $\vec G_m$} Dato $\vec G: \mathbb{R}^3 \rightarrow \mathbb{R}^3$ e una superficie $\Sigma$ con $\hat n \perp \Sigma$, si definisce \textbf{flusso di $\vec G$ attraverso $\Sigma$}:
$$
    \Phi_\Sigma(\vec G) = \oint_\Sigma \vec G \cdot \hat n d\Sigma
$$

\subparagraph{NB} Si consideri $\hat n$ e $\vec G$ uscenti dalla siperficie.

\subparagraph{Considerazioni} Si può facilmente osservare che $\Phi_\Sigma$ è massimo per $\Sigma \perp \vec G$, mentre è minimo se $\Sigma \parallel \vec G$. O, alternativamente, è massimo se $\hat n \parallel \vec G$, mentre è minimo se $\hat n \perp \vec G$

\paragraph{} Supponiamo che nella superficie sia presente una massa $M$. Il teorema di Gauss (in forma integrale) afferma che il flusso del campo $\vec G_M = -G\dfrac{M}{r^2} \hat r$ generato dalla massa $M$ posta all'interno di una superficie chiusa $\Sigma$ è
$$
    \Phi_\Sigma(\vec G_M) = -4\pi GM.
$$
\subparagraph{NB, 1} Il segno meno è dovuto alla scelta di $\hat n$ uscente; per $\hat n$ entrante, si avrebbe segno $+$.
\subparagraph{NB, 2} Non sono stati posti limiti alla forma della superficie e non sono sono stati posti limiti alla posizione della massa all'interno della superficie.

\paragraph{DIM.}
$$
    \Phi_\Sigma(\vec G) = - \oint_\Sigma G_\Sigma \cdot \hat n d\Sigma =
    - \oint_\Sigma \dfrac{GM}{r^2} \hat r \cdot \hat n d\Sigma =
    - GM\oint_\Sigma \dfrac{1}{r^2} \hat r \cdot \hat n d\Sigma =
    - GM\oint_\Sigma \dfrac{1}{r^2} d\Sigma_{\perp} =
    - GM\oint_\Sigma \Omega = -4\pi GM
$$

\subsection{Principio di sovrapposizione degli effetti}
\paragraph{Il campo gravitazionale generato da $n$ masse} Date $n$ masse $m_1$, $m_2$, $\dots$, $m_n$, ci chiediamo quale sia il campo gravitazionale da esse causato. Ricordiamo che il campo gravitazionale è definito come $\vec G_1 = \dfrac{F_{m_1, m_2}}{m_2}$.

\subparagraph{La forza risultante} Osserviamo prima di tutto che la forza gravitazionale risultante indotta su una massa $m_0$ è data da:
$$
    \vec F = \sum_{i = 1}^n \vec F_{i, 0} =
    - \sum_{i = 1}^n G\dfrac{m_im_0}{r_{i, 0}^2} \hat r_{i, 0}.
$$

\subparagraph{Da cui}
$$
    \vec G(r) = \dfrac{1}{m_0} \vec F =
    \dfrac{1}{m_0} \sum_{i = 1}^n \vec F_{i, 0} =
    - \cancel{\dfrac{1}{m_0}} \sum_{i = 1}^n G
    \dfrac{m_i\cancel{m_0}}{r_{i, 0}^2} \hat r_{i, 0} =
    - \sum_{i = 1}^n G\dfrac{m_i}{r_{i, 0}^2} \hat r_{i, 0} =
    \sum_{i = 1}^n \vec G_i.
$$

\subparagraph{CVD} Abbiamo dimostrato che il campo gravitazionae risultante è la somma dei campi gravitazionali.

\subsubsection{Gauss}
\paragraph{} Date $n$ masse $m_i$, vogliamo ora determinare il flusso del campo gravitazionale di queste masse attraverso una generica superficie $\Sigma$.
$$
    \Phi_\Sigma = \oint_\Sigma \vec G \cdot \hat n d\Sigma =
    \oint_\Sigma \sum_i \vec G_i \cdot \hat n d\Sigma =
    \sum_i \oint_\Sigma \vec G_i \cdot \hat n d\Sigma =
    \sum_i \Phi_\Sigma(\vec G_i) = -4\pi G\sum_i m_i
$$

\subsection{Distribuzione continua di massa}
\paragraph{} Studiamo ora il campo gravitazionale dato da una distribuzione continua di massa. Suppoviamo cioè che la massa non sia concentrata in un punto, ma che un volume di forma generica $\tau$ sia composto da innumerevoli punti aventi massa. Definiamo dunque il cmapo gravitazionale non già direttamente in funzione della massa complessiva dei punti del volume $\tau$, bensì a quello dovuto alla massa di un volume infinitesimo $d\tau$. Il campo gravitazionale si definisce dunque a partire da
$$
    d\vec G_m = -G\dfrac{dm}{r_{m, P}^2} \hat r_{m, P} =
    -G\dfrac{\rho(P)d\tau}{r_{m, P}^2} \hat r_{m, P}.
$$

Ne segue:
$$
    \vec G_m = \int_\tau d\vec G_m =
    \int_\tau -G\dfrac{\rho(P)d\tau}{r_{m, P}^2} \hat r_{m, P} d\tau
$$

\subparagraph{Precisazione} Abbiamo indicato con $\rho(x, y, z)$ la densità volumica del volume $\tau$.

\paragraph{Caso della simmetria}
\subparagraph{Sfera cava} Supponendo che $\tau$ sia una sfera e che la massa sia distribuita omogeneamente sulla superficie della sfera che indicheremo con $\Sigma^\prime$, cioè che $\sigma = \dfrac{dm}{d\Sigma^\prime}$ sia costante, e che $\tau$ abbia raggio $r$, osserviamo che:
$$
    \Phi_\Sigma(\vec G_m) = \oint_\Sigma \vec G_m \cdot \hat n d\Sigma =
    \dots = -G_m(r) 4\pi r^2 = -4\pi Gm \Longrightarrow
    G_m(r) = \dfrac{Gm}{r^2}.
$$

\subparagraph{Significato} Questo significa che per $R > r$ il campo gravitazionale è lo stesso generato dalla massa puntiforme posta al ventro della sfera e di massa $m = 4\pi r^2 \sigma$. % se per esempio volessimo studiare l'attrazione gravitazionale dovuta a un pianeta, potremmo studiarla solo come integrazione di infinitesimi campi gravitazionali dovuti alle masse infinitesime che compongono il pianeta, ma in modo più semplice, considerando tutta la massa concentrata in un punto $O$ detto \textbf{centro di massa} e il campo gravitazionale esterno a una sfera di raggio $r$.

\subparagraph{Sfera piena} Supponendo che $\tau$ sia una sfera e che la massa sia distribuita omogeneamente nel volume, cioè che $\rho = \dfrac{dm}{d\tau}$ sia costante, e che $\tau$ abbia raggio $r$, osserviamo che:
$$
    \Phi_\Sigma(\vec G_m) = \oint_\Sigma \vec G_m \cdot \hat n d\Sigma =
    \dots = -G_m(R) 4\pi r^2 = -4\pi Gm \Longrightarrow
    G_m(R) = \dfrac{Gm}{r^2}.
$$
Cioè all'esterno di $\tau$ il risultato non cambia, è come se fosse una sfera cava: la distribuzione della massa non è rilevante.

\subparagraph{Per una superficie interna a $\tau$?} Per una superficie interna a $\tau$, d'altro canto, il flusso del campo gravitazionale cambia. Se considerazzimo una superficie sferica di raggio $R < r$ interna a $\tau$ che ciamiamo $\Sigma_{int}$, il campo gravitazionale dovuto alla massa di $\tau$ si riduce. In particolare la massa $M$ sarà data da
$$
    m(R) = \rho \dfrac 43 \pi R^3 =
    \dfrac{m}{\cancel{\dfrac 43 \pi} r^3} \cancel{\dfrac 43 \pi} R^3 =
    m \dfrac{R^3}{r^3}.
$$
Dunque:
$$
    \Phi_{\Sigma_{int}} = -4\pi Gm(R) = -4\pi Gm\dfrac{R^3}{r^3}.
$$

Da cui:
$$
    \Phi_{\Sigma_{int}} = -4\pi Gm\dfrac{R^3}{r^3}
    \stackrel{r > R}{<}
    -4\pi Gm = \Phi_\Sigma
$$

\subparagraph{Significato} All'interno della sfera, il campo gravitazionale è minore che all'esterno.

\subparagraph{Considerazione aggiuntiva} Notare poi che:
$$
    \Phi_{\Sigma_{int}} = G_m(R) \dfrac Rr
$$

\subparagraph{Precizazione}Per i casi di simmetria, la superficie $\Sigma$ è sempre esterna a $\tau$.
