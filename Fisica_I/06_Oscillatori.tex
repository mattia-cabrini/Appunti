\section{Oscillatori e dintorni}

Abbiamo già studiato come l'oscillatore armonico semplice abbia equazione legge oraria
$$
    a(t) = -\omega^2 x(t)
$$

con:
$$
    x(t) = A \sin\Big( \omega t + \phi \Big)
$$
$$
    v(t) = - \omega A \cos\Big( \omega t + \phi \Big)
$$
$$
    v(t) = - \omega^2 A \sin\Big( \omega t + \phi \Big)
$$

Ora vogliamo capire come varia la legge oraria nel caso in cui la forza armonica sia perturbata.

\subsection{Oscillatore armonico perturbato da forza esterna costante}

Supponiamo che la forza armonica $F_e$ sia perturbata da una massa $F_g$ che agisce nella stessa direzione di $F_e$. Quello che succede, e che vogliamo dimostrare, è che la molla si estende fino a che la forza elastica non controbilanca la forza perturbante. Questo nel caso in cui la molla al primo istante sia a riposo. Se la molla non fosse a riposo, la massa inizierebbe ad oscillare. Vediamo come.

Iniziamo studiando la situazione di equilibrio:
$$
    F_e = F_g \Longleftrightarrow ky = mg \Longrightarrow y = \dfrac{mg}k
$$

La legge oraria:
$$
    m \vec a = \vec R:
$$
$$
    m \ddot y(t) = mg - ky(t) \Longrightarrow
    \ddot y(t) = g - \dfrac k m y(t) \Longrightarrow
    \ddot y(t) + \omega^2 y(t) = g \;\land\; \omega ^2 = \dfrac k m
$$

Da cui:
$$
    y(t) = A\sin(\omega t + \phi) + \dfrac g{\omega^2}
$$

\subsection{Oscillatore armonico perturbato dalla forza di attrito}

Immaginiamo ora che la massa si muova su un piano scabro. In particolare, spingiamo la massa $m$ oltre il centro di equilibrio della molla.

La massa sarà dunque soggetta non solo alla forza elastica, ma anche alla forza di attrito:
$$
    \vec R = \vec F_e + \vec F_{attr} \;\;\; con \;
    \begin{cases}
        \vec F_e(t) = -km \hat x \\
        \vec F_{attr} = -\mu_{s, d} mg\hat v = -\mu_{s, d} mg\hat x
    \end{cases}
$$
$$
    ma(t) \hat x = -kx(t) \hat x + \mu_{s, d} mg \hat x
    \Longrightarrow
    a(t) = -\dfrac km x(t) + \mu_{s, d}g
    \Longrightarrow
$$
$$
    a(t) + \omega^2 x(t) = \mu_dg
    \;\; \land \;\;
    \omega^2 = \dfrac km
    \Longrightarrow
    a(t) + \omega^2x(t) = \mu_dg
    \Longrightarrow
$$
$$
    \ddot x(t) + \omega^2 x(t) = \mu_dg
    \Longrightarrow
    x(t) = A\sin(\omega t + \phi) + \dfrac {\mu_dmg}k
$$

A questo punto andiamo a considerare le condizioni iniziali:
$$
    \begin{cases}
        x_0 = A \sin \phi + \dfrac {\mu_dmg}k \\
        v_0 = 0 = \omega A \cos \phi
    \end{cases}
    \Longrightarrow
    \begin{cases}
        A = x_0 - \dfrac {\mu_dmg}k \\
        \phi = \dfrac \pi 2
    \end{cases}
$$

Da cui la corretta legge oraria:
$$
    x(t) = A\sin(\omega t + \phi) + \dfrac {\mu_dmg}k =
    \Big( x_0 - \dfrac {\mu_dmg}k \Big) \sin \Big( t \sqrt{ \dfrac km } + \dfrac \pi 2 \Big) + \dfrac {\mu_dmg}k
$$

Notare che lo spazio di frenata non è simmetrico rispetto al centro di equilibrio della molla, ma è più piccolo.
$$
    x(t_f) = 2\dfrac {\mu_dmg}k - x_0 > - x_0.
$$

Il ché è ciò che intuitivamente ci aspettiamo. In particolare, determiniamo il tempo di frenata:
$$
    v(t_f) = 0 = Q \cos(\omega t + \dfrac \pi 2)
    \Longleftrightarrow
    \omega t_f + \dfrac \pi 2 = 3\dfrac \pi 2
    \Longleftrightarrow
    \omega t_f = \pi 
$$

Dopo che la massa $m$ è arrivata in $x_f$, il suo moto si invertirà. Studiamo ora questo fenomeno scrivendo direttamente la legge oraria — la quale, per motivi evidenti, avrà la stessa forma della LO del primo moto —:
$$
    x(t) = A_f \sin(\omega t + \phi) - \dfrac {\mu_dmg}k.
$$

Attenzione però: $A_f \neq A$ e $\phi_f \neq \phi$. Troviamo i parametri imponendo alla legge oraria le condizioni del moto:
$$
    \begin{cases}
        x(t_f) = A_f \sin(\omega t_f + \phi_f) - \dfrac {\mu_dmg}k \\
        v(t_t) = 0 = -\omega A_f \cos \phi_f
    \end{cases}
    \Bigg|_{t_f = \dfrac \pi \omega}
    \Longrightarrow
    \begin{cases}
        A_f = x_0 + 3\dfrac {\mu_dmg}k \\
        \phi_f = \dfrac \pi 2
    \end{cases}
$$

E così via...

Il moto sarà interotto quando sarà raggiunta la condizione
$$
    F_{attr, s} > F_e.
$$

\subsection{Oscillatore armonico smorzato da attrito viscoso}

$$
    ma(t) = R(t) = F_e(t) + F_{attr}(t) = -kx(t) - hv(t)
$$
$$
    \Longrightarrow
    a(t) = -\dfrac km x(t) - \dfrac hm v(t) =
    \omega^2 x(t) - 2\gamma v(t)
$$
$$
    \Longrightarrow
    a(t) + 2\gamma v(t) + \omega^2 x(t) = 0
$$

Riconosciamo l'equazione differenziale e determiniamo l'equazione caratteristica:
$$
    \lambda^2 + 2\gamma\lambda + \omega^2 = 0
$$
$$
    \lambda_{1,\;2} = -\lambda \pm \sqrt{\lambda^2 - \omega^2}
$$

Abbiamo dunque che:
$$
    x(t) = Ae^{-\lambda_1} + Be^{\lambda_2} =
    e^{-\lambda t}\Big(
        Ae^{t\sqrt{\lambda^2-\omega^2}} +
        Be^{-t\sqrt{\lambda^2-\omega^2}}
    \Big)
$$

Studiamo la relazione tra $\gamma$ e $\lambda$ e definiamo diversi tipi di smorzamento.

\paragraph{Smorzamento forte} Vale a dire talmente forte da non permettere l'oscillazione.
$$
    \gamma^2 > \omega^2
$$

\begin{figure}
    \centerline{\includegraphics[width=3.5in]{src/i7.png}}
    \caption{Lo smorzamento forte : dove le linee nera, blu, verde e rossa indicano rispettivamente \\ 
    \centerline{$\gamma = 0$, $\dfrac \gamma\omega = 1.0001$, $\dfrac \gamma\omega = 2$, e $\dfrac \gamma\omega = 3$.}}
\end{figure}

Dove le linee nera, blu, verde e rossa indicano rispettivamente
$$
    \gamma = 0; \;\;\;
    \dfrac \gamma\omega = 1.0001; \;\;\;
    \dfrac \gamma\omega = 2; \;\;\;
    \dfrac \gamma\omega = 3.
$$

\paragraph{Smorzamento critico} Si tratta del fenomeno che si verifica se

$$
    \lambda^2 = \omega^2.
$$

In questa eventualità, la legge oraria assume la forma
$$
    x(t) = e^{-\lambda} \Big( At + B \Big)
$$

\begin{figure}
    \centerline{\includegraphics[width=3.5in]{src/i8.png}}
    \caption{Lo smorzamento critico.}
\end{figure}

\paragraph{Smorzamento debole} Il fenomeno che si verifica per l'ultimo caso rimasto, lo \textbf{smorzamento debole}, si verifica se
$$
    \gamma^2 < \omega^2; \;\;\; \tilde\omega := \sqrt{\omega^2 - \gamma^2} < \big|\omega\big|.
$$

Nel caso dello smozamento debole il moto diventa
$$
    x(t) = Ae^{\lambda_1t} + Be^{\lambda_2t} = e^{-\lambda t}\Big(
        Ae^{i\tilde\omega t} + Be^{-i\tilde\omega t}
    \Big)
$$
$$
    \Longrightarrow
    x(t) = e^{-\gamma t} \Big(
        (A + B)\cos(\tilde\omega t) + i(A + B)\sin(\tilde\omega t)
    \Big)
$$

ponendo $a = A + B$; $b = i(A + B)$:
$$
    x(t) = e^{-\gamma t} \Big(
        a\cos(\tilde\omega) + b\sin(\tilde\omega t)
    \Big)
$$

ponendo $a=\tilde A \sin \tilde\phi$; $b=\tilde A \cos \tilde\phi$
$$
    x(t) = e^{-\gamma t} \tilde A \sin(\tilde\omega t + \tilde\phi).
$$

\begin{figure}
    \centerline{\includegraphics[width=3.5in]{src/i9.png}}
    \caption{Lo smorzamento debole: laddove i grafici nero, blu e verde rappresentano: $e^{-\gamma t}$, $-e^{-\gamma t}$ e $\dfrac \omega\gamma = 20$.}
\end{figure}

Laddove i grafici nero, blu e verde rappresentano:
$$
    e^{-\gamma t}; \;\;\;
    -e^{-\gamma t}; \;\;\;
    \dfrac \omega\gamma = 20.
$$

\subsection{Oscillatore armonico forzato}
\paragraph{} Consideriamo ora lo stesso oscillatore armonico smorzato dell'attrito viscoso che però è soggetto anche a una forza sinusoidale
$$
    \vec F_s = Fs \sin(\omega_s t) \hat x.
$$

Le equazioni del moto saranno date da:
$$
    m\vec a(t) = -k\vec x(t) - h\vec v(t) + \vec F_s
    \Longrightarrow
$$
$$
    ma(t) + hv(t) + kx(t) = F_s \sin(\omega_s t)
    \Longrightarrow
$$
$$
    a(t) + 2\lambda v(t) + \omega^2 x(t) = \dfrac {F_s}m \sin(\omega_s t)
$$

Abbiamo già trovato precedentemente
$$
    x_o(t) = e^{-\gamma t} \Big(
        A \sin(\omega t) + B \cos(\omega t)
    \Big);
$$

si può dimostrare che
$$
    x_*(t) = A_s \sin(\omega_s t + \phi_s)
$$

e che dunque:
$$
    x(t) = x_o(t) + x_*(t) = e^{-\gamma t} \Big(
        A \sin(\omega t) + B \cos(\omega t)
    \Big) + A_s \sin(\omega_s t + \phi_s)
$$

Omettendo i calcoli, perché non sono importanti per capire la lezione teorica:
% tan ɸ non dovrebbe avere il segno opposto?
$$
    \tan \phi_s = \dfrac{2\gamma\omega_s}{\omega_s^2-\omega^2};
    \;\;\;
    A_s = \dfrac{F_s}{m} \dfrac{1}{\sqrt{(\omega_s^2-\omega^2)^2+4\gamma^2\omega_s^2}}
$$

\subsection{Studio della risposta a forza sinusoidale}
\paragraph{Caso 1}
$$
    \omega_s << \omega
    \Longrightarrow
    \phi_s \approx 0; \;
    \dfrac {F_s}{\omega^2 m} = \dfrac {F_s}k
$$
$$
    \Longrightarrow
    x(t) \approx \dfrac {F_s}k \sin( \omega_s t )
$$

\paragraph{Caso 2}
$$
    \omega_s >> \omega
    \Longrightarrow
    \phi_s \approx -\pi ; \;
    \dfrac {F_s}{m\omega_s^2}
$$
$$
    \Longrightarrow
    x(t) \approx \dfrac {-F_s}{m\omega_s^2} \sin( \omega_s t )
$$

\paragraph{Caso 3}
$$
    \omega_s = \omega
    \Longrightarrow
    \phi_s \approx - \dfrac \pi 2; \;
    \dfrac {F_s}{m\omega_s^2}
$$
$$
    \Longrightarrow
    x(t) \approx \dfrac {-F_s}{2m\gamma\omega_s} \cos( \omega_s t ) =
    \dfrac {-F_s}{h\omega_s} \cos( \omega_s t )
$$

\subsubsection{Massimizzazione dell'ampiezza}
Essendo l'ampiezza dipendente dalla pulsazione, è possibile determinare per quale valore della pulsazione essa è massima/minima:
$$
    \dfrac {d}{d\omega_s} A_s(\omega_s) =
    \dfrac{F_s}{m}
    \dfrac {8\gamma^2\omega_s+4\omega_s\big(\omega_s^2-\omega^2\big)}
    {2\Big(4\gamma^2\omega_s^2+\big(\omega_s^2-\omega^2\big)^2\Big)^{3/2}} =
    0
$$
$$
    \Longrightarrow
    8\gamma^2\omega_s+4\omega_s\big(\omega_s^2-\omega^2\big) = 0
$$
$$
    \Longrightarrow
    \omega_s^2 = \omega^2 - 2\gamma^2 \leq \omega^2
$$
$$
    \Longrightarrow
    A_{s,\;max} = \dfrac {F_s}{m}
    \dfrac {1}{\sqrt{4\gamma^2\omega^2-4\gamma^4}} =
    \dfrac {F_s}{2m\gamma}
    \dfrac {1}{\sqrt{\omega^2-\gamma^2}}
    \geq A_s(\omega_s)
$$


\textbf{Coffe Break}
\begin{quote}
    O Tips\&Tricks    
\end{quote}
\subparagraph{} Per chi si fosse perso, $\omega$ e $\omega_s$ rappresentano rispettivamente la pulsazione del moto sidusoidale e la pulsazione della forzante.

\subsection{Risonanza}
$$
    A_s(\omega_s) = \dfrac {F_s}{m} \dfrac{1}{\sqrt{\big(\omega_s^2 - \omega^2\big)^2+4\gamma^2\omega_s^2}}; \\\;\\
    A_{s,\;max}(\omega_s) = \dfrac {F_s}{2m\gamma\sqrt{\omega^2-\gamma^2}}
$$

L'aumento della risonanza \textbf{risonanza} determina l'incremento dell'ampiezza del moto in condizioni di:

\begin{list}{-}{}
    \item \textbf{smorzamento debole}
    \item \textbf{frequenza forzante vicina alla frequenza propria} (la frequenza propria è la frequenza dell'oscillatore armonico non perturbato).
\end{list}

\subsection{Trasferimento di potenza}
\subsubsection{Istantaneo}
$$
    P_s(t) = \vec F_s(t) \cdot \vec v(t) =
    F_s \sin(\omega_s t) \dot x(t) =
    F_s \sin(\omega_s t) \omega_s A_s \cos(\omega_s + \phi_s) =
$$
$$
    F_s\omega_sA_s\;\sin(\omega_s t)\cos(\omega_s t)\cos\phi_s -
    F_s\omega_sA_s\;\sin(\omega_s t)\sin(\omega_s t)\sin\phi_s
$$

\subsubsection{Media sul periodo $T = \dfrac{2\pi}{\omega_s}$}
$$
    \bar P_s(t) = \dfrac 1T \int_0^T P_s(t) dt =
$$
$$
    F_s\omega_sA_s \dfrac 1T \bigg(
        \int_0^T \sin(\omega_s t)\cos(\omega_s t)\cos\phi_s dt -
        \int_0^T \sin(\omega_s t)\sin(\omega_s t)\sin\phi_s dt
    \bigg) =
$$
$$
    F_s\omega_sA_s \dfrac 1T \bigg(
        \cancel{
            \cos\phi_s \int_0^T \sin(\omega_s t)\cos(\omega_s t) dt
        } -
        \sin\phi_s \int_0^T \sin^2(\omega_s t) dt
    \bigg) =
$$
$$
    F_s\omega_sA_s \dfrac 1{\cancel {T}} \bigg(
         - \dfrac {\cancel{T}}2
    \bigg) \sin\phi_s =
    - \dfrac 12 F_s \omega_s A_s \sin\phi_s
$$

Siccome:
$$
    \sin \phi_s = \dfrac{\tan\phi_s}{\sqrt{1+\tan^2\phi_s}} =
    \dfrac { \dfrac{2\gamma\omega_s}{\omega_s^2-\omega^2} }
    {\sqrt{ 1 + \bigg( \dfrac{2\gamma\omega_s}{\omega_s^2-\omega^2} \bigg)^2 }} = \dots =
$$
$$
    \bar P_s = - \gamma m \omega_s^2 A_s^2
    \Longleftrightarrow
    \big|  \bar P_s \big| = \dots \leq \dfrac 12 F_s \omega_s A_s
$$

Da questo segue che \textbf{la potenza trasferita è massima quando la risonanza è massima}.
