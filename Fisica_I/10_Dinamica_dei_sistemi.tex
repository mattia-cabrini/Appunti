\section{Dinamica dei sistemi}

\subsection{Cinematica di N punti materiali}
\paragraph{Notazione} Introduciamo la notazione per le prossime pagine. L'i-esimo punto materiale avrà massa $m_i$, posizione $\vec r_i$, velocità $\vec v_i$ e accelerazione $\vec a_i$.

Abbiamo già visto che la per la forza risultante su un punto vale la relazione $\vec F_i = m_i \vec a_i$. La quantità di moto $p_i = m_i\vec v_i$. Il momento angolare $\vec L_i = \vec r_i \times \vec p_i$.

\subsection{Definizioni importanti}

\paragraph{def.} Si definisce \textbf{sistema di $\textbf{\textit{N}}$ punti} un insieme di N punti aventi coordinate $\vec r_i = [x_i, y_i, z_i]$, e massa $m_i$, con $i \in [1, N]$.

\paragraph{Massa totale} Indicheremo con $M$ la massa totale del sistema di $N$ punti: $M = \sum_i m_i$.

\subsubsection{Centro di massa}
\paragraph{def.} Si definisce \textbf{centro di massa} il punto $\vec r_{cm}$ media dei punti di cui è costituito il sistema ponderata dalla massa dei punti stessi.
$$
    \vec r_{cm} = \dfrac{\sum_i m_i \vec r_i}{\sum_i m_i} =
    \dfrac 1M \sum_i m_i \vec r_i
$$

\paragraph{Velocità e accelerazione} Note le velocità di ogni punto, sarà possibile studiare il sistema di N punti, non nella sua discretizzaione, ma come un unica massa $M$ posta nel centro di massa $\vec r_{cm}$. 

\subsubsection{Baricentro}
\paragraph{def.} Si definisce \textbf{centro di massa} il punto $\vec r_b$ media aritmetica dei punti di cui il sistema è costituito.
$$
    \vec r_b = \dfrac 1N \sum_i \vec r_i
$$

\subsubsection{Trasformazioni in SR e cinematica di $\vec r_{cm}$}
\paragraph{Trasformzazione in diversi SR} Il passaggio da un $SR$ a un $SR^\prime$ avviene, per la posizione, come abbiamo già studiato:
$$
    \vec r_i^\prime = r_i - r_{OO^\prime}
$$

\paragraph{Trasformazione di ${\vec r_i}$} Si dimostra
$$
    \vec r_{cm}^\prime =
    \dfrac 1M \sum_i m_i \big(r_i - r_{OO^\prime}\big) =
    \dfrac 1M \sum_i \big(m_i r_i\big) - r_{OO^\prime} =
    r_{cm} - r_{OO^\prime}
$$

\paragraph{Velocità del centro di massa} La velocità del centro di massa è definita come la velocità per un generico punto, e risulta essere pari alla media ponderata delle velocità dei punti di cui si compone il sistema:
$$
    \vec v_{cm} = \dfrac{d\vec r_{cm}}{dt} =
    \dfrac{d\sum_i m_i \vec r_i}{dt} =
    \dfrac 1M \sum_i m_i \vec v_i
$$

\paragraph{Accelerazione del centro di massa} Anche l'accelerazione del centro di massa si definisce come l'accelerazione di qualunque punto e, similmenta alla velocità, risulta essere la media pesata delle accelerazioni dei punti di cui si compone il sistema:
$$
    \vec a_{cm} = \dfrac{d\vec v_{cm}}{dt} =
    \dfrac{d\sum_i m_i \vec v_i}{dt} =
    \dfrac 1M \sum_i m_i \vec a_i
$$

\paragraph{Quantità di moto} La quatità di moto del sistema è la soma delle quantità di moto:
$$
    \vec p = \sum_i \vec p_i =
    \sum_i m_i \vec v_i =
    M \sum_i \dfrac{m_d\vec r}{Mdt} =
    M \vec v_{cm}
$$

\paragraph{Forze interne e forze esterne} Ogni punto è soggetto a forze interne e forze esterne. Il punto i-esimo è soggetto a una forza $\vec F_i = \vec F_{i, int} + \vec F_{i, est}$. Chiamando $\vec F_{i,j}$ la forza esercitata sul punto $j$ dal punto $i$, sapiamo per la 3NL che se il punto $j$ è soggetto alla forza $\vec F_{i,j}$, il punto $j$ sarà sogetto a una forza uguale in modulo ma opposta in vero $\vec F_{j, i} = - \vec F_{i, j}$. Quasto implica che la risultante delle forza interne sia nulla:
$$
    \sum_{(i, j) = (1, 1)}^{N} \vec F_{i, j} =
    \sum_{i > j} \big( \vec F_{i, j} + \vec F_{j, i} \big) =
    \sum_{i > j} \big( \vec F_{i, j} - \vec F_{i, j} \big) =
    \sum_{i > j} 0 =
    0.
$$

D'altra pate, deve essere vero che ogni forza esterna produza una forza esterna, sempre per la 3NL:
$$
    \sum_i \vec F_i = \sum_i \vec F_{i, est}.
$$

\paragraph{2NL per i sistemi di punti, I equazione cardinale} La forza totale applicata sul sistema è data dunque da:
$$
    \vec F = \sum_i F_i = \sum_i \vec F_{i, est} = \vec F_{est} =
    \sum_i m_i \vec a_i = \sum_i \dfrac{d\vec p_i}{dt} =
    \dfrac{d\vec P}{dt} = M \dfrac{\vec v_{cm}}{dt} = M \vec a_{cm}.
$$

\paragraph{Momento angolare di un sistema di N punti} Il momento di un sistema di $N$ punti, con polo $O$ è la somma dei momenti di ciascuna componente:
$$
    \vec L = \sum_i \vec L_i =
    \sum_i \big( \vec r_i \times m_i\vec r_i \big)
    \Longrightarrow
    \dfrac{d \vec L}{dt} =
    \sum_i \dfrac{d \vec r_i}{dt} \times m_i \vec v_i +
    \sum_i \vec r_i \times m_i \dfrac{\vec v_i}{dt}
$$

siccome in generale il polo $O$ è in moto:
$$
    \vec v_O \neq \vec 0
    \Longrightarrow
    \dfrac{d \vec r_i}{dt} = \vec v_i - \vec v_O
$$
$$
    \dfrac{d \vec L}{dt} =
    \sum_i \big( \vec v_i - \vec v_O \big) \times m_i \vec v_i +
    \sum_i \vec r_i \times m_i \vec a_i
$$
$$
    \dfrac{d \vec L}{dt} =
    \cancel{\sum_i \vec v_i \times m_i \vec v_i} -
    \sum_i \vec v_O \times m_i \vec v_i +
    \sum_i \vec r_i \times m_i \vec a_i
$$

In conclusione:
$$
    \dfrac{d \vec L}{dt} =
    \sum_i \vec r_i \times m_i \vec a_i -
    \sum_i \vec v_O \times m_i \vec v_i =
    \sum_i \vec r_i \times \vec F_i -
    \sum_i \vec v_O \times m_i \vec v_i
$$

\paragraph{II equazione cardinale} Per sistemi di riferimento inerziali [\textbf{SRI}]:
$$
    \dfrac{d \vec L}{dt} =
    - \vec v_O \times \vec P + \sum_i \vec M_{i, est} =
    - \vec v_O \times \vec P + M_{est}
$$

\paragraph{NB}
$$
    \vec v_O \times \vec P = 0
    \Longleftrightarrow
    \vec 0 \equiv \vec r_{cm} \lor
    % ¿¿¿ O \parallel cm ??? \lor
    \vec v_O = \vec 0
$$

\subsubsection{Leggi di trasformazione da $SR$ a $SR^\prime$}
\paragraph{Notazione} Identifichiamo un punto fisso in un sistema di riferimento $SR$. Tale punto fisso è il centro di massa del sistema di $N$ punti. Definiamo ora il sistema di riferimento $SR^\prime$ solidare con il punto fisso.
\paragraph{NB} Si osservi che per definizione di $SR^\prime$.

\paragraph{Leggi di trasformazione}
$$
    \vec r_i^\prime = \vec r_i - \vec r_{cm}
$$
$$
    \vec v_i^\prime = \vec v_i - \vec v_{cm}
$$
$$
    \vec a_i^\prime = \vec a_i - \vec a_{cm}
$$

Siccome $SR^\prime$ non è rotante ($\omega_{SR^\prime}$):
$$
    \vec r_{cm} = \vec 0
$$
$$
    \vec v_{cm} = \vec 0
$$
$$
    \vec a_{cm} = \vec 0
$$

\paragraph{Momento angolare rispetto a $O^\prime$} Per definizione, il momento angolare del sistema di $N$ punti, rispetto al polo $O^\prime \equiv \{cm\}$ è dato da:
$$
    \vec L^\prime = \sum_i \vec r_i^\prime \times m_i \vec v_i^\prime =
    \sum_i \vec r_i^\prime \times m_i (\vec v_i - \vec v_{cm}) =
    \sum_i \vec r_i^\prime \times m_i \vec v_i =
    \vec L \neq
    \vec r_{cm} \times \vec P
$$

\paragraph{Momento delle forze rispetto a $O^\prime$} Per definizione, il momento delle forze del sistema di $N$ punti, rispetto al polo $O^\prime \equiv \{cm\}$ è dato da:
$$
    \vec M^\prime =
    \sum_i \vec M_i^\prime =
    \sum_i \vec r_i^\prime \times m_i \vec a_i^\prime =
    \sum_i \vec r_i^\prime \times m_i (\vec a_i - \cancel{\vec a_{cm}}) =
    \sum_i \vec r_i^\prime \times \vec F_{i, est} =
    \vec M_{est}^\prime
$$

\subparagraph{NB} Questo risultato significa che le forze apparenti non influiscono sul momento delle forze.

\paragraph{$\mathcal R\{ \vec L, \vec M \}$}
$$
    \dfrac{d\vec L}{dt} = \vec M_{est}^\prime
$$

\subsection{Primo teorema di K\"onig}
\paragraph{Ipotesi} Assumiamo polo $O$ in quiete in $SR$.
\paragraph{Teorema}
$$
    \vec L = \sum_i \vec r_i \times m_i \vec v_i =
    \sum_i (\vec r_i^\prime + \vec r_{cm}) \times
    m_i(\vec v_i^\prime + \vec v_{cm}) =
$$
$$
    \sum_i \vec r_i^\prime \times \vec v_i^\prime +
    \cancel{\sum_i \vec r_i^\prime \times \vec v_{cm}^\prime} +
    \cancel{\sum_i \vec r_{cm}^\prime \times \vec v_i^\prime} +
    \sum_i \vec r_{cm}^\prime \times \vec v_{cm}^\prime
$$
$$
    \Longrightarrow
    \vec L = \vec L^\prime + \vec r_{cm} \times M \vec v_{cm} =
    \vec L^\prime + \vec L_{cm}
$$

\subsection{Lavoro compiuto da un sistema di $N$ punti}
$$
    W = \sum_i W_i = \sum_i \int \vec F_i \cdot d\vec s_i
    \stackrel{W = W_{int} + W_{est}}{=}
    \sum_i \int \vec F_{i, int} \cdot d\vec s_i + 
    \sum_i \int \vec F_{i, est} \cdot d\vec s_i =
$$
$$
    \cancel{\int \sum_i \vec F_{i, int} \cdot d\vec s_i} + 
    \int \sum_i \vec F_{i, est} \cdot d\vec s_i = W_{est}
$$

\subsection{Energia cinetica di un sistema di $N$ punti}
$$
    E_k = \sum_i E_{k, i} = \dfrac 12 \sum m_iv_1^2.
$$

\subsection{Lavoro compiuto per spostare il sistema da $A$ a $B$}
$$
    W_{AB} = E_{k, B} - E_{k, A} = \Delta E_k = \sum_i \Delta E_{k, i}.
$$

\paragraph{NB} Salto la parte relativa all'energia meccanica del sistema perché è del tutto simile all'energia meccanicadel punto materiale.

\subsection{II teorema di K\"onig}
\paragraph{} L'energia cinetica di un sistema di $N$ punti è data da:
$$
    E_k = \sum_i \dfrac 12 m_i v_i^2 = \sum_i \dfrac 12 m_i (v_i^\prime + v_{cm})^2 =
    \sum_i \dfrac 12 m_i v_i^{\prime 2} +
    \sum_i \dfrac 12 m_i v_{cm}^2 +
    \cancel{\sum_i m_i v_i^\prime v_{cm}}
    \Longrightarrow
    E_k = E_k^\prime + \dfrac 12 M v_{cm}
$$

\subsection{Urti}
\paragraph{def.} Si definisce \textbf{urto} la collisione di due sistemi fisici. La collisione avviene per un tempo $dt$, minore della sensibilità dei nostri cronometri; non possiamo studiare il sistema durante gli utri ma solo prima e dopo.

\paragraph{Classificazione} Come sarà noto dalle scuole superiori, esistono due tipi di urti: l'\textbf{urto elastico} e l'\textbf{urto anelastico}.

\paragraph{L'urto elastico} Si parla di \textit{urto elastico} quando i due sistemi \textbf{non} risultano essersi deformati dopo l'urto. In questo caso, si conserva l'energia cinetica del sistema e la quantità di moto:
$$
    \Delta\vec P = \vec 0 \;\land\; \Delta E_k = 0.
$$

\paragraph{L'urto anelastico} Si parla di \textit{urto anelastico} quando i due sistemi risultano essersi deformati dopo l'urto. In questo caso si conserva la quantità di moto del sistema. L'energia cinetica \textbf{non}
 si conserva perché la deformazione del sistema causa necessariamente il compimento di un lavoro e quindi la dissipazione di energia.
 $$
    \Delta\vec P = \vec 0 \;\land\; \Delta E_k = E_{k, f} - E_{k, ini} = W_{deformazione} \neq 0.
$$

\subparagraph{NB} Non esistono urti perfettamente anelastici se non sullo sfondo dell'apparire.

\subsection{Corpo rigido}
\paragraph{def.} Si definisce \textit{corpo rigido} un sistema di $N$ punti in cui la distanza $d_{ij}$ tra il punto $i$ e il punto $j$ non cambia mai: $\Delta d_{i, j} = 0$.

\paragraph{Gradi di libertà} Un corpo rigido che sia composto da un numero di punti $N > 3$ ha \textbf{sei} gradi di libertà\footnote{cfr. \href{https://www.primevideo.com/detail/0OFLVYKE8HPC1CPU8I2LETVDT5/ref=atv_sr_fle_c_Tn74RA__1_1_1?sr=1-1&pageTypeIdSource=ASIN&pageTypeId=B07WMZX929&qid=1621435948}{Stargate}.}.

\subsubsection{Distribuzione di massa}
\paragraph{NB} Vedere Analisi II per le definizioni di densità lineare, superficiale e volumica.
\paragraph{Centro di massa per distripuzioni continue}
$$
    \vec r_{cm} = \dfrac{\int_m dm^\prime}{\int_m dm^\prime} =
    \dfrac{\int_\tau \vec r \rho(\vec r) d\tau}{M} =
$$