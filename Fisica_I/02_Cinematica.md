# Cinematica

> La descrizione del moto dei corpi, senza investigarne le cause

**Sistema di riferimento [SR]**
La porzione di Universo isolabile dove si possono compiere misurazioni (≠ sistema di coordinate [SC]).

**Punto materiale**
*def.* oggetto molto piccolo rispetto al SR. Ciò significa che la definizione dipende dal SR.

**Punto materiale in moto**
*def.* Un punto materiale si dice **in moto** se le misure della sua distanza rispetto a un punto fisso nel SR variano nel tempo.

## Moto 1D

Nel SR esiste un solo asse e il punto può solo avvicinarsi o allontanarsi dal punto di riferimento **O**.

Possiamo supporre di misurare la distanza del punto da **O** per diversi tempi. Supponendo di fare un numero di misure → ∞, possiamo disegnare un grafico che associ ad ogni istante t<sub>n</sub> la distanza del punto da **O** in quell'istante. Il risultato è una curva continua. Chiamiamo la curva **funzione posizione** e la indichiamo con

$$
x(t).
$$

**Velocità media**
*def.* Si dice *velocità media* nell'intervallo *[t<sub>0</sub>, t<sub>1</sub>]* di un punto materiale, il rapporto tra lo spazio per corso dal punto e il tempo che ha impiegato a percorrerlo.

$$
v_m = \dfrac{Δx}{Δt} = \dfrac{x(t_1)-x(t_0)}{t_1-t_0} = v_{[t_0, t_1]}

$$

**Interpretazione geometrica**
Dal punto di vista geometrico, la velocità media non è altro che il coefficiente angolare della retta passante per i punti

$$
(t_0, x_0)\;\;\;e\;\;\;(t_1, x_1).

$$

Questo ci consente di scrivere la posizione del punto in funzione del tempo, della posizione iniziale e della velocità media:

$$
x(t_1) = v_m(t_1 - t_0) + x(t_0).

$$

**Lo spostamento**
È importante non confondere la **funzione posizione** con la **funzione spostamento**. La funzione posizione “localizza” il punto nel SR in ogni dato istante. La funzione spostamento, invece, misura di quanto il punto si sia spostato dalla posizione iniziale in un dato istante.

$$
Δx(t) = x(t) - x(t_0).

$$

**CIE**

$$
 v_m = \dfrac{\Delta x}{\Delta t} =
 \Big[L\;·\;T^{-1}\Big] =
 \Big[\dfrac{m}{s}\Big]\;\;\;
 spesso:\;\;\; = \Big[ \dfrac{km}{h} \Big].

$$

**La velocità istantanea**
La velocità media di un punto non fornisce informazioni dettagliate sul moto del punto; ovvero due punti con medesima velocità media possono avere moti molto diversi. Introduciamo il concetto di **velocità istantanea**, ovvero di velocità media in un intervallo infinitesimo *[t<sub>0</sub>, t<sub>0</sub> \+ dt]*:

$$
v(t_0) = \dfrac{x(t_0 + dt) - x(t_0)}{dt},

$$

anche senza essere dei piccoli geni:

$$
v(t) = \dfrac{dx}{dt}(t) = \dot{x}(t).

$$

**La posizione in funzione della velocità**
Date le definizioni di velocità istantanea e di funzione spostamento, è facila calcolare la posizione in funzione dello spostamento:

$$
v(t) = \dfrac{dx}{dt} \; \Longrightarrow \; v(t)dt = dx \; \Longrightarrow \; x(t) = x_0 + ∫_{t_0}^{t}v(t)dt.

$$

Mentre lo spostamento:

$$
Δx(t) = x(t) - x(t_0) = ∫_{t_0}^{t}v(t)dt.

$$

**Relazione tra *v<sub>m</sub>* e *v*, il moto uniforme**
Definiamo **moto nuniforme** il moto che fa velocita istantanea *v(t)* costante e dimostriamo che se la velocità istantea è costante, llora la velocità media è pari alla velocità istantanea.

$$
 v_m = \dfrac {Δx} {Δt} = \dfrac {∫_{t_0}^{t_0 + Δt} v(t)dt} {Δt} =
 \dfrac {v(t) ∫_{t_0}^{t_0 + Δt} dt} {Δt} = \dfrac {v(t) \cancel{Δt}} {\cancel{Δt}} = v(t)

$$

**Lo spazio percorso**
*def.* definiamo lo spazio percorso da un punto, come la somma di tutte le distanze infinitesime persorse dal punto nell'intervallo *[t<sub>0</sub>, t]*:

$$
s(t) = ∫_{t_0}^{t}|v(t’)|dt’.

$$

**Accelerazione**
Per distinguere i moti di oggetti che raggiungono sì lo stesso punto di arrivo, ma in tempi e modi diversi, abbiamo introdotto i concetti di:

- *velocità media*: quanto tempo impiegano i due punti materiali a raggiungere la destinazione;
- *velocità istantanea*: il punto che si muove verso la destinazione, percorre spazzi infinitesimi sempre nello stesso tempo? torna indietro e poi va nuovamente avanti? a un certo punto si ferma e poi riparte?

Ovvero abbiamo introdotto le velocità per studiare i comportamenti di due enti che si muovono dallo stesso punto iniziale allo stesso punto finale. Ora introduciamo una nuova grandezza, **l'accelerazione**, per studiare i diversi moti di due corpi che si muovono dalla stessa velocità iniziale alla stessa velocità finale.

**Accelerazione media**
*def.* Definiamo accellerazione media la variazione di velocità istantanea di un punto materiale in un intervallo di tempo *[t<sub>0</sub>, t<sub>1</sub>]*.

$$
a_m = \dfrac{v(t_1)-v(t_0)}{t_1-t_0} = \dfrac{Δv}{Δt} = a_{[t_0, t_1]}

$$

*def.* Definiamo accelerazione istantanea, la variazione di velocità istantane ain un intervallo infinitesimo *[t<sub>0</sub>, t<sub>0</sub> \+ dt]*.

$$
a(t) = \dfrac{v(t_0 + dt)-v(t_0)}{dt} = \dfrac{dv}{dt} = \dot{v}(t) = \dfrac{d^2x}{dt^2} = \ddot{x}(t)

$$

Per ragioni ovvie:

$$
v(t) = v_0 + ∫_{t_0}^{t} a(t’)dt’

$$

**La legge oraria a partire da *a(t)* costante, *v<sub>0</sub>* e *x<sub>0</sub>***

$$
x(t) = x_0 + ∫_{t_0}^{t} v(t’)dt’ = x_0 + ∫_{t_0}^{t}\Big(v_0+∫_{t_0}^{t} a(t’)dt’\Big) dt’

$$

Essendo *a(t) = a, ∀t*:

$$
 x(t) = x_0 + ∫_{t_0}^{t}\Big(v_0+∫_{t_0}^{t} a\;dt’\Big) dt’
 = x_0 + v_0∫_{t_0}^{t} dt’ + a∫_{t_0}^{t} (t’-t_0) dt’ \\\;\\
 = x_0 + v_0\Big[\;t\;\Big]_{t_0}^{t} + a\Big[\dfrac{1}{2}t^2-t_0t\Big]_{t_0}^{t} =
 x_0 + v_0(t-t_0) + \dfrac{1}{2} a (t-t_0).

$$

**Formula di *v(x)* se *a(x) = a, ∀x***

$$
 a(x) = \dfrac {dv(x)} {dt} = \dfrac {dv(x)} {dx} · \dfrac {dx(t)} {dt} \Longrightarrow
 a(x) = v(x) \dfrac {dv(x)} {dx} \Longrightarrow a(x)dx = v(x)dx \\\;\\
 ∫_{x_0}^{x} a(x’)dx’ = ∫_{v_0}^{v(x)} v’(x)dv’(x) \Longrightarrow
 a(x - x_0) = \Big[\dfrac {v^2(x)} {2}\Big]_{v_0}^{v(x)}

$$

(fin qui vale sempre, anche per accelerazione non costante, di seguito si considera accelerazione costante)

$$
 a(x-x_0) = \dfrac {v^2(x) - v_0^2} {2} \Longrightarrow
 v^2(x) = v_0^2 + 2a(x-x_0)

$$

* * *

**Coffee break**

> O tips & tricks

La velocita finale di un grave lasciato cadere nel vuoto da un'altezza *h* è sempre

$$
v = \sqrt{2|g|h}.

$$

* * *

**Il moto smorzato (sempre 1D)**
*def.* Definiamo moto smorzato il moto la cui accelerazione dipende linearmente dalla velocità, ovvero del tipo:

$$
a(t) = -kv(t),\;\;\;con\;[k]=\big[T^{-1}\big]=\Big[\dfrac{1}{s}\Big].

$$

Vediamo passo-passo come trovare la funzione posizione:

$$
 a(t) = \dfrac{dv}{dt} = -kv(t) \Longrightarrow
 \dfrac{dv}{v} = -kdt \Longrightarrow
 ln\Big(\dfrac{v(t)}{v_0}\Big) = -k(t-t_0) \Longrightarrow
 \dfrac{v(t)}{v_0} = e^{-k(t-t_0)}.

$$

Evidenziamo l'espressione della velocità e po troviamo l'espressione della posizione:

$$
 v(t) = v_0 e^{-k(t-t_0)} \Longrightarrow
 x(t) = ∫_{t0}^{t} v_0e^{-k(t-t_o)} dt \Longrightarrow
 x(t) = \dfrac{v_0}{ke^{-t_0}} \Big( e^{t_0} - e ^{-kt} \Big) + x_0

$$

**Moto armonico**
*def.* Definiamo *moto armonico* un moto che abbia una posizione del tipo:

$$
x(t) = A\; sin(ωt + ɸ) + B\; cos(ωt + ɸ).

$$

Laddove:

- *A* è l'ampiezza del moto;
- *ω* è la pulsazione (o frequenza);
- il periodo è *T = 2π/ω*;
- *Φ* è la fase iniziale.

La velocità è data dunque da:

$$
v(t) = \dfrac{dx}{dt} = ωA\; cos(ωt + Φ) - ωB\; sin(ωt + Φ).

$$

L'accelerazione:

$$
a(t) = \dfrac{dv}{dt} = -ω^2A\; sin(ωt + Φ) - ω^2B\; cos(ωt + ɸ) = -ω^2\; x(t).

$$

## Moto nD

Finché abbiamo la necessità di studiare il moto di un punto materiale lungo una sola dimensione, possiamo usare uno scalare per rappresentare le grandezze fisica *posizione*, *velocità* e *accelerazione*. Ma se il moto è n-dimensionale, è necessario non solo sapere qual è l'intensità della velocità o accelerazione, ma anche la direzione in cui queste definiscono il moto. Anche la posizione deve poi essere rappresentata nelle n dimensioni, ovvero, la posizione iniziale di un punto materiale sarà data dalle sue coordinate:

$$
 \vec{x_0} = \begin{bmatrix} 0 \\ 0 \end{bmatrix},

$$

anche la funzione posizione dovrà essere scritta in forma vettoriale. In particolare, il moto va scomposto lungo le due [n] dimensioni, va definita una funzione posizione per ogni dimensione. Consideriamo per esempio il moto di un proiettile che viene sparato in aria; la pistola imprime ad esso una velocità di 10m/s a un inclinazione di 30° con l'asse delle x. La velocità sarà ripartita l'ungo l'asse delle ascisse per un fattose pari a cos(30°) = √3/2 e lungo l'asse delle prdinate per un fattore pari a sin(30°) = 1/2, ovvero:

$$
 \vec{v}(t) = \begin{bmatrix} {5\;\sqrt{3}} \\ {5} \end{bmatrix} \;m\big/s.

$$

Se volessimo poi determinare la funzione posizione a partire da una posizione iniziale *x<sub>0</sub> = (0, 3)*, cioè con la bocca della pistoa a 3m d'altezza:

$$
 x_x = v_x(t)t + x_{0, x}; \;\;\; x_y = v_y(t) t + x_{0, y}; \\\;\\
 \vec{x}(t) = \begin{bmatrix} 5\;\sqrt{3}\;t \\ 5t + 3 \end{bmatrix} \;m.

$$

## Sui vettori

Dei vettori ricordiamo solo un'operazione (per le altre si veda ALeG). Se un vettore dipende da una o più variabili, esso può essere derivabile:

$$
 \vec{a}(t) = \vec{a}_x(t)\hat{u}_x(t) + \vec{a}_x(t)\hat{u}_y(t) + \vec{a}_z(t)\hat{u}_z(t)
 \\\;\\
 \dot{\vec{a}}(t) = \dfrac{d\vec{a}}{dt}(t) =
 \frac{d\Big(\vec{a}_x(t)\hat{u}_x(t) + \vec{a}_x(t)\hat{u}_y(t) + \vec{a}_z(t)\hat{u}_z(t)\Big)}{dt} = \\\;\\
 \frac{d\vec{a}_x(t)\hat{u}_x(t) + d\vec{a}_x(t)\hat{u}_y(t) + d\vec{a}_z(t)\hat{u}_z(t)}{dt} =
 \dfrac{d\vec{a}_x}{dt} \hat{u}_x(t) + \dfrac{d\vec{a}_y}{dt} \hat{u}_y(t) + \dfrac{d\vec{a}_z}{dt} \hat{u}_z(t).

$$

Se poi gli assi non dipendono dal tempo:

$$
 \dfrac{d\vec{a}}{dt}(t) = \dfrac{d\vec{a}_x}{dt} \hat{u}_x + \dfrac{d\vec{a}_y}{dt} \hat{u}_y + \dfrac{d\vec{a}_z}{dt} \hat{u}_z.

$$

**Regole di derivazione**

- $$
    \dfrac{d}{dt}\vec{a}(t) = \dfrac{d}{dt}\Big(a(t)\hat{a}(t)\Big) = \dfrac{d}{dt}a(t)\hat{a}(t)+a(t)\dfrac{d}{dt}\hat{a}(t)

    $$

- $$
    \dfrac{d}{dt}\Big(k(t)\vec{a}(t)\Big) = \dfrac{d}{dt}k(t)\vec{a}(t)+k(t)\dfrac{d}{dt}\vec{a}(t)

    $$

- $$
    \dfrac{d}{dt}\Big(\vec{a}(t)·\vec{b}(t)\Big) = \dfrac{d}{dt}\vec{a}(t)·\vec{b}(t)+\vec{a}(t)·\dfrac{d}{dt}\vec{b}(t)

    $$

- $$
    \dfrac{d}{dt}\Big(\vec{a}(t)\times\vec{b}(t)\Big) = \dfrac{d}{dt}\vec{a}(t)\times\vec{b}(t)+\vec{a}(t)\times\dfrac{d}{dt}\vec{b}(t)

    $$

- $$
    \dfrac{d}{dt}\Big(\vec{a}(t)+\vec{b}(t)\Big) = \dfrac{d}{dt}\vec{a}(t)+\dfrac{d}{dt}\vec{b}(t)

    $$

* * *

**Salto a piè pari le definizioni di posizioni, velocità e accelerazione in forma vettoriale**

Basta sapere che d'ora in poi la funzione posizione sarà indicata con la lettera *r*, eventualmente sarà un vettore.

* * *

## Il moto curvilineo

Studiando il moto di un punto materiale nello spazio, introduciamo il versore
$$
\vec \tau (t), \;\vec n(t)
$$
tangente la curva descritta dal moto del punto.

Per moti infinitesimi:
$$
d\vec r(t) = ds(t) \hat\tau(t).
$$

E di conseguenza:
$$
\vec v(t)=\frac{d\vec r(t)}{dt}=\dfrac{ds(t)}{dt}\hat\tau(t)=\dot s(t)\hat\tau(t).
$$

Siccome è immediato che
$$
 \vec v(t) = v(t) \hat v(t),
$$
è immediato anche che
$$
 \vec v(t)=\dot s(t)\hat\tau(t)\Longrightarrow v(t)=\dot s(t)\land\hat v(t)=\hat\tau(t)
$$

L'accelerazione:
$$
 \vec a (t) = \dfrac {d\vec v(t)} {dt} = \dfrac {d\Big[v(t)\hat\tau(t)\Big]} {dt} =
 \dfrac {dv(t)} {dt}\hat\tau(t) + v(t) \dfrac {d\hat\tau(t)} {dt} = \\\;\\
 \dfrac{dv(t)}{dt}\hat\tau(t) + v(t)\dfrac{d\hat\tau(t)}{ds}\dfrac{ds(t)}{dt} =
 \dfrac{dv(t)}{dt}\hat\tau(t) + v^2(t)\dfrac{d\hat\tau(s)}{ds}
$$

Introduciamo il vettore
$$
\vec n(s) = \dfrac{\hat\tau(s)}{ds},\;\;con\;\hat n(s)=\sin\theta\hat u_x+\cos\theta\hat u_y
$$
normale alla curva dello spostamento.

E dunque:
$$
\vec a(t) = \dfrac{dv(t)}{dt}\hat\tau(t) + v^2(t)\vec n(t)
$$

**Riassunto grafico delle definizioni**

<div align="center">
<img src="src/i3.png" alt="i3.png" width="417" height="311">
</div>

Chiaramente nel caso del moto rettilineo *n = 0*. Data la figura:
$$
 \vec n(s) = n(s) \hat n(s) = \dfrac {d\hat\tau(t)} {ds} =
 \lim_{\Delta s \rightarrow 0} \dfrac {\hat\tau(s+\Delta s)-\hat\tau(s)} {ds} \\\;\\
 n(s) =
 \lim_{\Delta s \rightarrow 0} \Big|\dfrac {\hat\tau(s+\Delta s)-\hat\tau(s)} {ds}\Big| =
 \lim_{\theta\rightarrow 0} \Big|\dfrac {2sin\dfrac\theta2} {R\theta}\Big| = \dots = \dfrac 1 R
 \\\;\\
 \vec a(t) = \dfrac {dv(t)} {dt} \hat \tau(t) + \dfrac {v^2(t)} {R} \hat n(t)
$$

Introduciamo l'accelerazione centripeta *a<sub>n</sub>* (normale alla curva del moto) e tangenziale *a<sub>𝝉</sub>* (sempre alla curva definita dallo spostamento):
$$
 a_n (t) = \dfrac {v^2(t)} R;\;\;\; a_\tau(t) = \dfrac {dv(t)} {dt}.
$$

Riscriviamo l'accelerazione:
$$
\vec a(t) = a_\tau(t)\hat\tau(t) + a_n(t)\hat n(t).
$$

## Equazioni del moto nello spazio

$$
 \dfrac {\vec r(t)} {dt} = \vec v(t) \Longrightarrow \vec r(t) = \vec r(0) + ∫_0^t \vec v(t’)dt’
$$
$$
\dfrac {\vec v(t)} {dt} = \vec a(t) \Longrightarrow \vec v(t) = \vec a(0) + ∫_0^t \vec a(t’)dt’
$$

* * *

**Salto a piè pari i cambi di coordinate**

Riporto solo i sistemi per il cambiamento di coordinate.

* * *

## Coordinate polari

$$
 \begin{cases}
  r_x(t) = R(t) \cos \theta(t) \\
  r_y(t) = R(t) \sin \theta(t)
 \end{cases} \Longrightarrow
 \begin{cases}
  \theta(t) = \tan^{-1}\frac{r_y(t)}{r_x(t)},\;con\;\theta \in [0, 2π) \\
  r(t) = \sqrt{r_x^2(t) + r_y^2(t)} ≥ 0
 \end{cases} \\\;\\
 \Longrightarrow \vec v(t) = \dfrac {d\vec r(t)} {dt} =
 \dfrac{dr(t)}{dt}\hat r(t) + r(t)\dfrac{d\hat r(t)}{dt} =
 v_r(t)\hat r(t) + r(t) \frac{d\theta(t)}{dt} \hat \theta(t) = \\\;\\
 v_r(t)\hat r(t) + v_\theta(t)\hat\theta(t) = \vec v_r(t) + \vec v_\theta(t) \\\;\\
 \xRightarrow{(no\;dim.)} \vec a(t) = \vec a_r(t) + \vec a_\theta(t)
$$

## Coordinate cilindriche

$$
 \begin{cases}
  r_x(t) = R(t) \cos \theta(t) \\
  r_y(t) = R(t) \sin \theta(t) \\
  r_z(t) = r_z(t)
 \end{cases} \Longrightarrow
 \begin{cases}
  r_\theta(t) = \tan^{-1}\frac{r_y(t)}{r_x(t)},\;con\;\theta \in [0, 2π) \\
  r_R(t) = \sqrt{r_x^2(t) + r_y^2(t)} ≥ 0 \\
  r_z(t) = r_z(t)
 \end{cases}
$$

## Coordinate sferiche

$$
 \begin{cases}
  r_x(t) = R(t) \sin\phi(t)\cos\theta(t) \\
  r_y(t) = R(t) \sin\phi(t)\sin\theta(t) \\
  r_z(t) = R(t) \cos\phi(t) \\
  \phi\in[0, π] \land \theta\in[0, π) \land R(t) ≥ 0
 \end{cases} \Longrightarrow
 \begin{cases}
  R(t) = \big|\vec r(t)\big| \\
  \theta(t) = \tan^{-1} \dfrac {r_y(t)} {r_x(t)} \\
  \phi(t) = \cos^{-1} \dfrac z {\big|\vec r(t)\big|}
 \end{cases}
$$

* * *
Per altre specificazioni sul cambiamento di coordinate, vedere gli appunti di Analisi II.
* * *

## Moto circolare

Si chiama **moto circolare** qualunque moto il cui spostamento definisca una circonferenza.

Per questo tipo di moto studiamo la **velocità angolare**
$$
 \omega(t) := \lim_{\Delta t\rightarrow 0} \dfrac {\theta(t+\Delta t) - \theta(t)} {\Delta t} = \dfrac {d\theta(t)} {dt}
$$
e dato che
$$
 d\theta = \dfrac {ds(t)} R, \\\;\\
 \omega(t) = \dfrac 1 R \dfrac{ds(t)} {dt} = \dfrac {v(t)} R.
$$
Chiaramente:
$$
\big[\omega\big] = \big[\theta\big]\big[T^{-1}\big] = \Big[\dfrac{rad}s\Big]
$$

Nel caso del moto circolare **uniforme**, ovvero a velocità costante:
$$
 \omega(t) = \dfrac v R = \sqrt \dfrac {a_n} R
$$

*def.* Si definisce **periodo** il tempo (costante) che impiega il punto a descrivere un'intera circonferenza (in parole più semplici, il tempo che il punto impiega a variare l'angolo di *2π rad*):
$$
T = \dfrac {2π} \omega = \Big[\dfrac{rad}{{rad}/s}\Big] = \big[s\big].
$$

*def.* Si definisce **frequenza** l'inverso del periodo:
$$
 \nu = \dfrac \omega {2π} = \dfrac 1 T = \Big[\dfrac 1 s\Big] = \big[Hz\big].
$$

Studiamo anche l'**accelerazione angolare**:
$$
 \alpha(t) := \dfrac {d\omega}{dt} = \dfrac 1 R \dfrac {v(t)} {dt} = \dfrac {a_\tau(t)} R.
$$
Di cui:
$$
 \big[\alpha\big] = \big[\omega\big]\big[t^{-1}\big] = \Big[\dfrac{rad}{s^{-2}}\Big].
$$

Nel caso del moto circolare uniforme, tale grandezza è nulla.

Senza dare troppe spiegazioni:
$$
 \omega(t) = \omega(t_0) + ∫_{t_0}^t \alpha(t’)dt’.
$$

Restiamo poi di stucco nello scoprire che:
$$
 \theta(t) = \theta(t_0) + ∫_{t_0}^t \omega(t’)dt’.
$$

Dunque — ancora più sorprendente —, nel caso del moto circolare uniformemente accelerato (ovvero valido anche per l'uniforme):
$$
\theta(t) = \theta(0) + \omega(t) t + \dfrac 1 2 \alpha(t) t^2.
$$

## Il vettore *velocità angolare

Fin ora abbiamo evitato di definire il vettore *velocità angolare* in quanto non è possibile scriverlo come rapporto tra differenziali a causa della non commutatività delle rotazioni angolari nello spazio. Abbiamo sempre parlato di **modulo della velocità angolare**. È arrivato il momento di parlare della velocità angolare in forma  vettoriale.

*def.* Prende il nome di vettore **velocità angolare** il vettore
$$
 \vec\omega(t):\;\vec v(t)=\vec\omega(t)\times\vec r(t)
$$

Dunque:
*def.* Prende il nome di vettore **accelerazione angolare** il vettore
$$
\vec\alpha(t):=\dfrac{d\vec\omega(t)}{dt}
$$

**Calcoli**
$$
 \vec a(t):=\dfrac{d\vec v(t)}{dt} =
 \dfrac d{dt} \Big[\vec\omega(t)\times\vec r(t)\Big] = \\\;\\
 \dfrac{d\vec\omega(t)}{dt}\times\vec r(t)+\vec\omega(t)\times\dfrac{d\vec r(t)}{dt} = \\\;\\
 \dfrac{d\omega(t)}{dt}\hat\omega(t)\times r(t)\hat r(t) +
 \omega(t)\hat\omega(t)\times\vec v(t) = \\\;\\
 \dfrac{d\omega(t)}{dt}\hat\omega(t)\times R\;\hat r(t) +
 \omega(t)\hat\omega(t)\times\Big[\vec\omega(t)\times\vec r(t)\Big] = \\\;\\
 \dfrac{a_\tau(t)}{\cancel R}\hat\omega(t)\times\cancel R\;\hat r(t) +
R\;\omega^2(t)\hat\omega(t)\times\Big[\hat\omega(t)\times\hat r(t)\Big] = \\\;\\
 a_\tau(t)\hat\tau(t) +
R\;\omega^2(t)\hat n(t)
$$

## Posizione e velocità nel caso di moto circolare uniforme

Per quanto abbiamo già detto sul moto circolare, riscriviamo di seguito alcune formule che riassumono la circostanza di velocità costante diversa da zero.

$$
 \vec r(t) = \begin{bmatrix}
  R\;\cos\theta(t) \\
  R\;\sin\theta(t)
 \end{bmatrix} = \begin{bmatrix}
  R\;\cos\big(\omega t + \theta_0\big) \\
  R\;\sin\big(\omega t + \theta_0\big)
 \end{bmatrix}
$$

E dunque:

$$
 \vec v(t) = \dot{\vec r}(t) = \begin{bmatrix}
  -R\;\dot\theta(t)\sin\theta(t) \\
  R\;\dot\theta(t)\cos\theta(t)
 \end{bmatrix} = \begin{bmatrix}
  -\omega R\;\sin\theta(t) \\
  \omega R\;\cos\theta(t)
 \end{bmatrix}
$$

Per poi concludere:

$$
 \vec a(t) = \dot{\vec v}(t) = \ddot{\vec r}(t) = \begin{bmatrix}
  -\omega^2R\;\cos\theta(t) \\
  -\omega^2R\;\sin\theta(t)
 \end{bmatrix} =
  -\omega^2\;\vec r(t) =
  \cancel{a_\tau\hat\tau}+a_n\hat n(t)
$$

* * *
**Coffee Break**
Abbiamo visto e ripetuto varie volte come la velocità *v* sia in termini di modulo, sia in termini vettoriali sia devinita come
$$
\vec v(t) = \dot{\vec r}(t);
$$
si può dunque affermere che la velocità angolare sia in efetti la derivata rispetto al tempo della posizione angolare di un punto? Ovvero, è utile definire
$$
\omega(t) = \dot\theta(t)?
$$
La risposta è **NO**.

**Attenzione alle proprietetà degli operatori in oggetto**
Il motivo per cui è sbagliato definire la velocità angolare come derivata della posizione angolare è che la rotazione per dimensioni *N ≥ 3* **non è un'operazione commutativa**.

Supponiamo un punto che si trovi alle coordinate *r<sub>0</sub> = [1, 0, 0]* compia due rotazioni: una sul piano *XY* di *θ = π* e una sul piano *YZ* di *⍺ = π/2*. La posizione del punto varierà nl seguente modo:

posizione dopo la rotazione sul piano *XY*:
$$
r_1 = \begin{bmatrix}-1\\0\\0\end{bmatrix};
$$

posizione dopo la rotazione sul piano *YZ*:
$$
r_2 = \begin{bmatrix}-1\\-1\\0\end{bmatrix}.
$$

Ritorniamo in posizione r<sub>0</sub> e procediamo all'inverso:

posizione dopo la rotazione sul piano *YX*:
$$
r_1 = \begin{bmatrix}0\\1\\0\end{bmatrix};
$$

posizione dopo la rotazione sul piano *XY*:
$$
r_2 = \begin{bmatrix}0\\1\\0\end{bmatrix}.
$$

Abbiamo dunque provato che le rotazioni non sono comutative. L'unica eccezione è costituita dalle rotazioni sllo stesso piano. In *2D*, infatti, le rotazioni sono commutative.
 **Dim.**
 Supponiamo che il punto sia in posizione r<sub>0</sub> e che compia due rotazioni *ß* e *ɣ*:
 dopo *ß*:
 $$
 r_\beta=\begin{bmatrix}
  \cos\beta\;-\sin\beta\\
  \sin\beta\;\;\;\;\cos\beta
 \end{bmatrix}
 r_0;
 $$
 dopo *ɣ*:
$$
 r_{\beta\gamma} =

 \begin{bmatrix}
  \cos\gamma\;-\sin\gamma\\
  \sin\gamma\;\;\;\;\cos\gamma
 \end{bmatrix}

 r_\beta =
$$
$$
 \begin{bmatrix}
  \cos\gamma\;-\sin\gamma\\
  \sin\gamma\;\;\;\;\cos\gamma
 \end{bmatrix}
 \cdot
 \begin{bmatrix}
  \cos\beta\;-\sin\beta\\
  \sin\beta\;\;\;\;\cos\beta
 \end{bmatrix}
 r_0 =
$$
$$
 \begin{bmatrix}
  \cos\beta\cos\gamma-\sin\beta\sin\gamma\;\;\;-\cos\beta\sin\gamma-\sin\beta\cos\gamma\\
  \sin\beta\cos\gamma+\cos\beta\sin\gamma\;\;\;-\sin\beta\sin\gamma-\cos\beta\cos\gamma
 \end{bmatrix}r_0;
 $$

 Per contro, è facile dimostrare che se la sequenza fosse inversa il risultato sarebbe medesimo poiché sarebbe sufficiente scambiare i due angoli nell'ultima espressione e – stante la ben nota proprietà commutativa dell'operazione *moltiplicazione* – la matrice non verrebbe ad essere mutata.
* * *

## Qualche altro tipo di moto

> Ovvero basta banalità

**Il moto ellittico**
Sarà nota al lettore dalle scuole superiori la definizione analitica di ellisse e dunque la sua generica formula:
$$
 \dfrac{x^2}{R_x^2}+\dfrac{y^2}{R_y^2}=1,
$$

ora, dato che vogliamo studiare la fisica eliminiamo *x* e *y*:
$$
\dfrac{r_x^2(t)}{R_x^2}+\dfrac{r_y^2(t)}{R_y^2}=1.
$$

Possiamo anche riscrivere questa relazione in un sistema equivalente in cui la *r* dipenda dall'angolo *θ*:
$$
 \begin{cases}
  r_x(t) = R_x\cos\theta(t) = R_x\cos\theta(\omega t+\phi)\\
  r_y(t) = R_y\sin\theta(t) = R_y\sin\theta(\omega t+\phi)
 \end{cases}
$$

**Il moto elicoidale**
Un elica, per farla breve, è la traiettoria descritta da un punto in moto circolare uniforme ripetto a un piano e una retta ad esso ortogonale – retta passante per il centro del cerchio e ∥ al vettore velocità angolare – che non si limita a girare lungo il cerchio, ma anche in un certo verso lungo la retta.
$$
 \begin{cases}
  r_x(t) = R\cos\theta(t) = R\cos\theta(\omega t+\phi)\\
  r_y(t) = R\sin\theta(t) = R\sin\theta(\omega t+\phi)\\
  r_z(t) = vt
 \end{cases}
$$

**Il moto cicloide**
Una cicloide, sempre per farla breve, è la traiettoria descritta da un punto in moto circolare uniforme ripetto a un piano e una retta ad esso parallela – retta passante per il centro del cerchio e ∥ al vettore posizione quando *θ(t) = θ<sub>0</sub>* – che non si limita a girare lungo il cerchio, ma anche in un certo verso lungo la retta.

Nello specifico caso in cui la retta sia ∥ all'asse *X* e θ<sub>0</sub> = 0:
$$
 \begin{cases}
  r_{P,\;x}(t) = R_P\cos\theta(\omega t) - R\omega t - R_P\\
  r_{P,\;y}(t) = R_P\sin\theta(\omega t) + R + R_P
 \end{cases}
$$

## Descrizione di un moto in SR relativi

Fin ora abbiamo considerato solo moti in un SR fermo. Ovvero abbiamo considerato il SR come un qualcosa di assoluto e che ci permette di studiare il moto di un punto materiale la cui variazione di posizione è legata alla variazione delle coordinate valutate lungo gli assi (ortogonali) del SR (che hanno intersezione nell'origine.

Ma cosa succederebbe se dovessimo studiare il moto di un punto materiale rispetto al moto di un altro punto materiale? Sappiamo bene, dalle scuole medie, che il moto è relativo al SR in considerazione: una macchina ha una certa velocità rispetto una persona seduta su una sedia nel dehor di un bar che dà sulla strada di una grande città in un caldo pomeriggio d'estate; la stessa macchina avrà però una velocità diversa ripetto a un'altra macchina che le viene in contro dall'altra corsia di quella strada di quella grande città, in quel caldo pomeiggio d'estate.

Per studiare questo fenomeno, diciamo che consideriamo un *SR* fermo, immobile, come gli altri sistemi che abbiamo studiato: la ersona seduta nel dehor.

Dopo di ché consideriamo il secondo sistema di riferimento, quello che si muoe rispetto al primo, *SR'*, che ha origine donde si trovi la prima macchina.

Dunque introduciamo il vettore posizione
$$
\vec r_{oo'}(t)
$$
che rappresenta la posizione della prima macchina rispetto a *SR*, al tempo *t* (posizione dell'origine dei *SR'*).

E poi ci interessa anche
$$
\vec r'(t),
$$
la posizione del punto materiale in oggetto (la seconda macchina) nel sistema *SR'*.

A questo punto, possiamo determinare la posizione del punto materiale rispetto al sistema di riferimento in moto *SR'* (la posizione della seconda macchina rispetto alla prima) come:
$$
\vec r(t) = \vec r_{oo'}(t)+\vec r(t).
$$

Possiamo poi notare che:
$$
\vec r_{oo'}(t)=-\vec r_{o'o}(t),
$$
il che significa che la posizione di *O'* rispetto a *O* è pari alla posizione di *O* rispetto a *O'* moltiplicata per *-1*.

* * *
**NB** Questo ragionameto si applica a moti le cui velocità siano
$$
 v_* << c.
$$
Se le velocità fossero prossime a quelle della luce, si dovrebbe tenere conto dei cosiddetti *fenomeni relativistici*, che non verranno considerati in questo corso, in quanto esso affronta temi in cui gli effetti relativistici possono essere considerati infinitesimali e, dunque, trascurabili.
* * *

Per quanto riguarda le velocità:
$$
 \vec r'(t)=\vec r(t)-r_{oo'}(t) \\\;\\
 \vec v'(t)=\dfrac{d\vec r(t)}{dt}-\dfrac{d\vec r_{oo'}(t)}{dt} =
 \vec v(t) - \vec v_{o'}(t) \\\;\\
 \vec v'(t) = \begin{bmatrix}
  v_x(t) -v_{x,\;o'}(t) \\
  v_y(t) -v_{y,\;o'}(t) \\
  v_z(t) -v_{x,\;o'}(t) \\
 \end{bmatrix} = \begin{bmatrix}
  \dot r_x(t) -\dot r_{x,\;oo'}(t) \\
  \dot r_y(t) -\dot r_{y,\;oo'}(t) \\
  \dot r_z(t) -\dot r_{x,\;oo'}(t) \\
 \end{bmatrix} = \begin{bmatrix}
  \dot r_x'(t) \\
  \dot r_y'(t) \\
  \dot r_z'(t) \\
 \end{bmatrix}
$$

Utilliziamo la formula di **Poisson**:
$$
 \dfrac{d\vec k(t)}{dt}=\vec\omega_k(t)\times\vec k(t)
$$

e dunque andiamo a ricalcolare la velocità:
$$
 \dfrac{d\vec r'(t)}{dt}=
 \dfrac{d\Big[
  r_x'(t) \hat x'(t) +
  r_y'(t) \hat y'(t) +
  r_z'(t) \hat z'(t)
 \Big]}{dt}=\\\;\\

 \bigg(
  \dot r_x'(t) \; \hat x'(t) +
  \dot r_y'(t) \; \hat y'(t) +
  \dot r_z'(t) \; \hat z'(t)
 \bigg)+\\ \bigg(
  \dot r_x'(t) \; \dfrac{d\hat z'(t)}{dt} +
  \dot r_y'(t) \; \dfrac{d\hat z'(t)}{dt} +
  \dot r_z'(t) \; \dfrac{d\hat z'(t)}{dt}
 \bigg)=\\\;\\

 \vec v'(t) +
 r_x'(t) \Big[\vec\omega_{SR'}(t) \times \hat x'(t) \Big] +
 r_y'(t) \Big[\vec\omega_{SR'}(t) \times \hat y'(t) \Big] +
 r_z'(t) \Big[\vec\omega_{SR'}(t) \times \hat z'(t) \Big] =\\\;\\

 \vec v'(t) +
 \bigg[ \vec\omega_{SR'}(t) \times \vec r'(t) \bigg] =
 \vec v(t) - \vec v_{o'}(t) \;\;\; \{per\;def.\} \\

 \Longrightarrow \\
 \vec v(t) = \vec v'(t) - \bigg[
 \vec v_{o'}(t) + \vec\omega_{SR'}(t) \times \vec r'(t)
 \bigg]
$$

**Leggi di trasformazione dell'accelerazione**
$$
 \vec a(t)=\dfrac{d\vec v(t)}{dt}=
 \dfrac{d\vec v'(t)}{dt} +
 \dfrac{d\Big[\vec\omega_{SR'}(t)\times\vec r'(t)\Big]}{dt} +
 \dfrac{d\vec v_{o'}(t)}{dt}
$$

* Parte I:
$$
\vec a_{o'}(t) = \dfrac{\vec v_{o'}(t)}{dt}
$$

* Parte II:
$$
 \dfrac{d\Big[\vec\omega_{SR'}(t)\times\vec r'(t)\Big]}{dt} =

 \dfrac{d\vec\omega_{SR'}(t)}{dt} \times \vec r'(t) +
 \vec\omega_{SR'}(t) \times \dfrac{d\vec r'(t)}{dt} = \\\;\\

 \dfrac{d\vec\omega_{SR'}(t)}{dt} \times \vec r'(t) +
 \vec\omega_{SR'}(t) \times \bigg[ \vec v'(t) - \Big(
  \vec v_{o'}(t) + \vec\omega_{SR'}(t) \times \vec r'(t)
 \Big)\bigg]
$$

* Parte III:
$$
 \dfrac{d\vec v'(t)}{dt} =
 %% \vec a(t) - \vec a_{o'}(t) +
 %% \vec\omega_{SR'}(t) \times \vec r'(t) =
 \vec a'(t) + \vec\omega_{SR'}(t) \times \vec r'(t)
$$

In sintesi:
$$
 \vec a(t) =

 \vec a_{o'}(t) +
 \dfrac{d\vec\omega_{SR'}(t)}{dt} \times \vec r'(t) + \\
 \vec\omega_{SR'}(t) \times \bigg[ \vec v'(t) - \Big(
  \vec v_{o'}(t) + \vec\omega_{SR'}(t) \times \vec r'(t)
 \Big)\bigg] +\\
 \vec a'(t) + \vec\omega_{SR'}(t) \times \vec r'(t) = \\\;\\

 \dots

 \\\;\\
 \vec a(t) = \vec a_{o'}(t) +
 \dfrac{d\vec\omega_{SR'}(t)}{dt} \times \vec r'(t) + \\
 \vec\omega_{SR'}(t) \times \Big(
  \vec\omega_{SR'}(t) \times \vec r'(t)
 \Big) + \\
 2\vec\omega_{SR'}(t) \times \vec v(t).
$$

## I sistemi di riferimento inerziali

Un ***SRI*** è un *SR'* tele per cui sia verificata la seguante condizione:

il punto *O* in *SR* (orivine di *SR'*) è molto lontano ed è isolato da qualunque oggetto osservato e si muove di moto rettilineo uniforme (eventualmente con velocità nulla).

In generale, dto un *SR* e un sistema di riferimento inerziale *SRI* rispetto a *SR*, qulunque altro sistema di riferimento inerziale *SRI'* rispetto a *SR* sarà in moto rettilineo uniforme rispetto a *SRI*.

Il vantaggio dei SRI è che in essi non c'è trasformazione per l'accellerazione. Oltre che un vantaggio, questa è anche un'importante considerazione. Infatti quello che stiamo dicendo e che si può dimostrare banalmente (L20210322 / Lez 10) è che scegliendo qualunque SRI (ovvero qualunque SR *non *accelerato*) il moto trasformato non risulta accelerato. In altre parole, per accelerare un moto, ovverosia per modificare la velocità e dunque la posizione di un punto, è necessaria un interazione. **L'accelerazione non è un fenomento giustificabile con la relatività del moto** [filosofia]. Ancora in altre parole: le leggi della meccanica **non** dipendono dal SR.

* * *
**Coffee Break**
> O Tips&Tricks

Chiarimento sulla formula di Poisson:
$$
 \dfrac{d\vec k(t)}{dt}=\vec\omega(t)\times\vec k(t)
$$

che vale per
$$
 \big|\vec k(t)\big|=k,\;\forall t.
$$

Nella prima relazione, il vettore
$$
\vec k(t)
$$

indica la posizione di un punto che ruota rispetto su un piano rispetto e applicato a un punto O (moto circolare con centro in O). Mentre
$$
 \vec\omega(t)
$$
è un vettore con:
* direzione normale al pino in cui avviene la rotazione;
* verso orientato nei confronti dell'osservatore che vede avvenire la rotazione in senso **antiorario**;
* modulo mutevole nel tempo e pari alla velocità angolare istantanea del punto in rotazione.
* * * 