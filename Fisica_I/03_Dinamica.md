# Dinamica

> Ovvero: delle cause dei moti.

Le cause dei moti, per quel che concerne la meccanica – e dunque la Fisica newtoniana –, sono studiate attraverso le **forze**. Una forza, applicata a un'oggetto può provocarne un'accelerazione. L'interazione tra le forze e il moto sono state studiate empiricamente per secoli e, grazie a Newoton e in parte a Galileo, possiamo riassumere il risultato di questi studi in tre leggi: **le leggi di Newton** o **leggi fondamentali della dinamica**.

## Prima legge della dinamica

> O *legge di inerzia*

In un SRI, un corpo isolato (non soggetto ad alcuna forza) permane nel suo stato di moto rettilineo uniforme (eventualmente con velocità nulla, nel qual caso si parla di *stato di quiete*).

## Seconda legge della dinamica

> O *equazione del moto*

In un SRI inerziale, un corpo di massa *m* si muove con accelerazione *a* s.se è soggetto a una forza *F* tale per cui:
$$
 \vec F = m\vec a =
 \Big[kg \; \dfrac m {s^2}\Big] =
 \Big[N\Big]=
 \{Newton\}
$$

**NB** Dati due corpi *1* e *2*, chiameremo
$$
\vec F_{2, 1}
$$
la forza che il corpo *2* applica (o induce) sul corpo *1*. Il vettore *F* ha come punto di applicazione il punto *1*, cioè il corpo su cui la forza è applicata (o indotta).

Chiaramente un corpo potrebbe essere anche soggetto a diverse forze docute a corpi diversi. Supponendo che un corpo *0* sia soggetto a *n* forze dovute ai corpi *1*, *2*, *3*, *4*, ..., *n*, allora la forza totale applicata sul corpo *0* sarà:
$$
 \vec F_0 = ∑_{i = 1}^n \vec F_{0, i}.
$$

<!--
Attenzione: non ho riportato i disegni, ma tanto uno se li imagina, no? Attenzione a non farsi trarre in inganno dagli esempi! Non sempre le forze indotte dal corpo *A* sulo corpo *B* hanno per direzione la retta che congiunge *A* a *B*! Si veda per esempio la forza di Lorentz.
-->

## Terza legge della dinamica

> O *principio di azione e reazione*

L'interazione tra due corpi *A* e *B* produce una forza *F<sub>A,  B</sub>* sul corpo a e una forza *F<sub>B, A</sub>* tali che:
$$
 F_{A, B} = -F_{B, A}.
$$

**NB** Anche se i vettori sono messi in relazione come nella formula, è sempre bene ricordare che i punti di applicazione sono *diversi*.

**Notazione**
Indicheremo sempre con
$$
\vec F_{X, Y}
$$

una forza applicata in *Y* e dovuta a *X*.

**Contatto sì, contatto no?**
Soprà ho scritto di proposito “applica (o induce)” perché una forza può essere dovuta a un fenomeno di contatto o a un fenomeno *non* di contatto. Una sedia appoggiata su un pavimento non è spinta fino al centro della terra per via dell forza di gravità, questo perché il pavimento applica alla sedia una forza detta *reazione vincolare* che bilancia la forza dovuta al fenomeno della gravità e rende nulla la risultante delle forze applicate alla sedia. La reazione vincolare è un esempio di fenomeno di contatto e duqu di forza applicata. Il fenomeno della gravità, d'altra parte, è un fenomeno che consiste nell'attrazione di due corpi aventi massa anche se sono distanti fra loro; dunque la forza non è *di contatto*, essa è indotta anche a distanza.

## Quantità di moto

*def.* Definiamo **quantità di moto** il vettore dovuto al prodotto tra la massa di un punto materiale e la velocità del punto stesso.
$$
 \vec p(t) = m(t) \vec v(t) = m(t) \dfrac {\vec r(t)}{dt} =
 \Big[kg\;\dfrac{m}{s}\Big] = \Big[Ns\Big]
$$

**La seconda legge se *m(t)* è costante**
$$
\vec F(t) = \dfrac {d\vec p(t)} {dt}
$$

**Dim.**
$$
\vec F(t) = m\vec a(t) = m(t) \dfrac {d\vec v(t)} {dt}
$$

ma se:
$$
 \dfrac {d\vec p(t)} {dt} =
 \cancel{ \dfrac {dm(t)} {dt} \vec v(t) } +
 \dfrac {d\vec v(t)} {dt} m(t) =
 m(t) \vec a(t)
$$

`CVD`

* * *
**Coffee Break**
> O Tips&Tricks

Per noi la massa sarà sempre costante. Tuttavia se stessimo considerando il moto di un aereo, e dovessimo trovarci a calcolare le forze agenti su di esso, dovremmo valutare – per esempio – la massa dovuta aggiuntiva docuta ai passeggeri e la massa diminuita a causa della combustione del carburante.

Ma questo non è un problema di Fisica I, quindi per ora stiamo tranquilli.
* * *

## Impulso

*def.* Si definisce impulto il vettore
$$
 \vec J(t) = ∫_0^t \vec F(t')dt'
 \stackrel{per\;m(t)\;cost.}{=}
 ∫_0^t \dfrac {d\vec p(t')}{\cancel {dt'}} \cancel{dt'} =
 \vec p(t) - \vec p(0) = \Delta\vec p(t).
$$

## Quali forze?

Noi studiamo le relazioni tra corpi macrospcopici a livello macroscopici. Tuttavia questi effetti possono essere ridotti a fenomeni microscopici. In particolare, esistono quattro tipi di forze fondamentali:

* la forza **gravitazionale**: la vedremo in dettaglio;
* la forza **elettromagnetica**: ne studieremo le basi, il dettaglio in Fisica II;
* la forza **nucleare debole** []non la studieremo;
* la forza **nucleare forte** [non la studieremo].

## La forza peso

> Ma tu quanto pesi?
> 70kg.
> No, non voglio sapere qual è la tua massa, voglio sapere quanto pesi!
> Ah, scusa! 686,7N!
> Grazie, buona giornata <3

Abbiamo già visto che i corpi aventi massa, sono soggetti a *g*, tuttavia abbiamo parlato di *g* come di "accelerazione gravitazionale", non come di forza. Ebbene, *g* è l'accelerazione dovuta al alla forz indotta sul corpo dalla Terra per via del fenomeno gravitazionale. *g*, in particlare, si ricava per via sperimentale, mentre la forza a cui è soggetto il corpo, F<sub>P</sub> si ricava dalla 2NL (2<sup style="font-size: 10px">nd</sup> Newton Law):
$$
 \vec F(t) = m(t)\vec a(t) = \vec F_p = m(t)\vec g
$$

Chiaramente:
$$
 \vec g = \ddot{\vec y}(t) \Longleftarrow
 y(t) = y_0 + v_0 t + \dfrac 1 2 g t^2
$$

## Reazione vincolare

Come abbiamo accennato, la superficie terrestre applica sui corpi ad essa appoggiati una reazione vincolare, ovvero una forza opposta a quella che "spinge" il corpo verso il centro della terra e la annulla.

Anche altre superfici possono essere soggette al fenomeno della reazione vincolare.

La reazione vincolare a una supericie [piano] è normale al piano e si indica
$$
 \vec N
$$

## La forza elastica

La forza elatica è la forza che da origine a un moto armonico semplice. Altro non è che la forza che caratterizza una molla.

Quello che ci interessa è studiare cosa succede se si estende una massa vincolata a una molla fino alla massima estensione della molla (massima estensione che non degradi lo stato della molla) e poi si "lascia andare" la massa. L'esperienza quotidiana ci suggerisce che la molla attrarrà a se la massa.

Supponendo che la massa abbia una posizione iniziale *l<sub>0</sub>* e che venga estesa fino a *l<sub>0</sub> + ∆l*, quello che succede è che istante per istante la massa è sempre soggetta a una forza del tipo
$$
 \vec F_e(t) = -k∆\vec l(t)
$$

ovvero la forza elastica è sempre opposta al verso di estensione della molla. E questo fa sì che la molla, successivamente, si decomprima.

In generale bisognerebbe tenere anche conto delle forza di attrito, del materiale di cui è composta la molla e altri fattori per determinare lo smorzamento del moto. Il moto armonico viene generato e può essere studiato trascurando questi ulteriori fattori, solo per estensioni molto minori rispetto al limite elastico della molla (la *massima estensione* di cui sopra).

## Studiamone il moto

Sappiamo che
$$
 \vec F(t) = m \vec a(t).
$$

Ovvero che
$$
 \vec F_e(t) = m \dfrac{d\vec v}{dt} \Longrightarrow
 -k\;l(t) = m \;\ddot l(t).
$$

Svilluppando:
$$
 \ddot l(t) = -\dfrac k m \;l(t).
$$

Evidentemente
$$
l(t) = \sin(t).
$$

E dunque *l(t)* è moto armonico con
$$
 \omega^2 =
 \dfrac k m \Longrightarrow \omega =
 \sqrt{\dfrac k m}
$$

Ma il moto armonico semplice è del tipo
$$
 x(t) = A \sin(\omega t + \phi),
$$

*A* e *ɸ* si possono ricavare dalle condizioni iniziali:
$$
 \begin{cases}
  l(0) = A \sin \phi  \\
  \dot l(0) = \omega A \cos \phi
 \end{cases}
$$

## Trasmissione di una forza via corpo esteso

Per corpo esteso intendiamo una corda, o una fune, collegata a una massa *m* a cui è vincolata. L'applicazione di una forza al capo libero della fune in verso opposto alla massa, provocherà la trasmissione della forza alla massa.

Possiamo immaginare la corda come un insieme di piccoli elementi fra loro vincolati da forze che non studieremo. La corda è unidimensionale, nonché inestensibile (e quindi incomprimibile). Una forza applicata un estremo della fune che supponiamo essere vincolata all'altro estremo applica una forza contraria in verso ma pari in modulo (3NL) lungo la corda. Tale forza è detta tensione. Attraverso la tensione, la forza si trasmette all'altro capo della corda.

Se la fune non è ideale, la forza trasmessa alla massa potrebbe essere minore rispetto alla forza direttamente applicata alla fune.

## Attrito

*def.* Per **Attrito** si intende una forza di contatto con verso opposto al senso del moto.

Le forze di attrito si prestano a vantaggio o a svantaggio del moto a econda del contesto:

* se si vuole mettere in moto un oggetto, l'attrito è uno **svantaggio** in quanto esso dissipa la forza che accelera l'oggetto. Ovvero, l'oggetto posto in moto essendo soggetto alla forza di attrito definita come sopra, subirà per via di questi una accelerazione negativa e rallenterà il moto fino al punto di porvi fine, in altre parole: lo **dissiperà**. Se il concetto non vi fosse chiaro, fate scivolare un quaderno lungo il pavimento di casa e ne avrete un'idea;
* se si vuole ottenere uno stato di quiete – per esempio – lungo un piano inclinato, l'attrito è invece un **vantaggio** perché permette **aderenza**.

La quantitificazione dell'attrito di un materiale su un altro, avviene attraverso un coefficiente d'attrito.

Le forze di attrito sono dovute a interazioni elettromagnetiche microscopiche.

## Attrito radente

Per **attrito radente** si intende il particolare attrito che agisce tra due corpi che "strisciano" l'uno sull'altro.

L'attrito radente può essere **statico** o **dinamico**. L'attrito radente statico si sviluppa su un corpo soggetto a una forza che non per via dell'attrito non è in moto. Provate a spostare un armadio: non è sufficiente applicare una forza qualunque per spostarlo, se la forza è troppo piccola, esso non si muoverà, per via dell'attrito. L'attrito radente dinamico è invece l'attrito dovuto allo strofinamento tra la superficie e il corpo quando il corpo è in moto. In generale, dato il coefficiente di attrito tra il corpo e la superficie:
$$
 \vec F_{attr}(t) = -\mu_{s, d} N(t) \hat F.
$$

Notare come la forza d'attrito sia proporzionale alla reazione fincolare dell'oggetto sulla superficie.

Indichiamo poi rispettivamente l'attrito radente statico e dinamico con
$$
\mu_s, \;\mu_d
$$

## Attrito viscoso

Un altro tipo di attrito è l'attrito **viscoso**, ovvero l'attrito che si sviluppa tra un corpo in moto in un fluido e il fluido. In generale:
$$
\vec F_{attr}(t) = -k \vec v(t)
$$

## Attrito volvente

    *** UN ALTRA VOLTA ***

## Il pendolo semplice

<div align="center">
 <img src="src/i4.png" width="350px" />
</div>

Per studiare il moto del pendolo è importante tenere a mente le forze che agiscono sulla massa e sulla corda (o braccio). In larticolare, preso l'asse *x* perpendicolare all'asse del pendolo e l'asse *y* solidale all'asse, consideriamo che:

* ad ogni istante *t* il braccio del pendolo descrive un certo angolo con l'asse, che chiameremo
$$
\theta(t);
$$

* la massa è soggetta alla forza peso
$$
m \vec g
$$

* il braggio sarà soggetto a una tensione opposta al verso radiale
$$
\vec T_f(t);
$$

* il braccio avrà lunghezza
$$
l.
$$

Aiutandoci con la figura, possiao determinare la risultante delle forze lungo i due versori:
$$
 \vec R = \vec T_f + m\vec g =
 R_r \hat r(t) + R_\theta\hat \theta(t)
 \\\;\\
 R_r = mg \cos\theta - T_f=
 -ma_n

 \\\;\\
 R_\theta = mg \cos \Big(\dfrac π 2 - \theta \Big) =
 mg \sin\theta =
 -ma_\tau
$$

Ora sviluppiamo i calcoli alla luce di quanto detto precedentemente sul moto circolare e **ricaviamo la tensione *T***:
$$
 R_r = -ma_n(t) = -m \dfrac {v^2} l = mg \cos\theta(t) - T_f(t)
 \\\;\\
 \Longrightarrow T_f(t) =
 m \Big(g \cos\theta(t) + \dfrac {v^2(t)} l\Big).
$$

Sempre sviluppando i calcoli alla luce del moto circolare uniforme ricaviamo il **periodo di oscillazione**:
$$
 -\cancel ma_\tau = \cancel mg\sin\theta(t) \Longrightarrow
 \ddot\theta(t) = - \dfrac g l \sin\theta(t).
$$

Per piccole oscillazioni:
$$
 \lim_{x \rightarrow 0} \dfrac {\sin x} x = 1 \Longrightarrow
 \ddot\theta(t) = - \dfrac g l \theta(t)
$$

Ovvero:
$$
 \theta(t) = \theta(0) \sin(\omega t + \phi); \;\;\;
 \omega = \sqrt{ \dfrac g l }; \;\;\;
 T = 2 \pi \sqrt{ \dfrac g l }
$$

**NB** Non si tratta del moto circolare uniforme. Dunque *ω* non è *dθ/dt*.

Si può dimostrare che
$$
 s(t) = l\theta(t)
$$

* * *
Salto il pendolo conico @ L20200329.
* * *

## Sistemi di riferimento non inerziali

> Ovverosia soggetti ad accelerazione o a rotazione rispetto a *SR*.

Vediamo subito le leggi di trasformazione.

* **Posizione**
$$
 \vec r'(t) = \vec r(t) - \vec r_{oo'}(t).
$$

* **Velocità**
$$
 \vec v(t) = \vec v(t) - \Big[
 \vec v_{o'}(t) + \vec\omega_{SR'}(t) \times \vec r'(t)
 \Big],
$$

con:
$$
 \; \vec v_{o'} = \dfrac {d\vec r_{oo'}} {dt}
$$

* **Accelerazione**
$$
 \vec a(t) = \vec a(t) - \Big[
  \vec a_{o'} (t) +
  \vec a_{eul}(t) +
  \vec a_{cen}(t) +
  \vec a_{cor}(t)
 \Big],
$$

con:
$$
 \vec a_{o'} (t) = \dfrac {d\vec v_{o'}(t)} {dt}
$$
$$
 \vec a_{eul}(t) =
 \dfrac{d\vec\omega_{SR'}(t)}{dt} \times \vec r'(t)
$$
$$
 \vec a_{cen}(t) = \vec\omega_{SR'}(t) \times \Big[
  \vec\omega_{SR'}(t) \times \vec r'(t)
 \Big]
$$
$$
 \vec a_{cor}(t) = 2 \vec\omega_{SR'}(t) \times \vec v'(t)
$$

* **Forza**
$$
 per\;def.\;\vec F(t) = m \vec a(t)
$$
$$
 \vec F'(t) = m \vec a'(t) =
 m \vec a(t) + \text{forze\;agenti\;sul\;SRnI}
$$
$$
 \vec F_{app} := \vec F'(t) - \vec F(t) = m \vec a'(t) - m \vec a(t)
$$

## Forze apparenti / fittizie / inerziali

*def.*
$$
 \vec F_{app} = -m \Big[
  \vec a_{o'} (t) +
  \vec a_{eul}(t) +
  \vec a_{cen}(t) +
  \vec a_{cor}(t)
 \Big]
$$
$$
 \vec F_{tr} = -m \Big[
  \vec a_{o'} (t) +
  \vec a_{eul}(t) +
  \vec a_{cen}(t)
 \Big]
$$
$$
 \vec F_{eul} = -m \Big[
  \vec a_{eul}(t)
 \Big]
$$
$$
 \vec F_{cen} = -m \Big[
  \vec a_{cen}(t)
 \Big]
$$
$$
 \vec F_{cor} = -m \Big[
  \vec a_{cor}(t)
 \Big]
$$

## Principio di equivalenza forte (Relatività generale)

* **P. debole**: la massa inerziale (misura di resistenza alla variazione del moto) è equivalente alla massa gravitazionale (misura di attrazione dei corpi).
* **P. forte**: le leggi della fisica sono le stesse in tutti i sistemi di riferimento. In particolare, i risultati di un esperimento condotto in presenza di gravità sono equivalenti a quelli ottenuti in un opportuno SRnI.
