\section{Misure}

\subsection{Di cosa si tratta}
\paragraph{def.} Definiamo "misurazione" il risultato di un operazione di misura.

La misurazione è il confronto di una grandezza fisica con un campione della stessa grandezza. Generalmente questo confronto è realizzato attraverso degli strumenti di misurazione.

Il risultato di una misurazione, o di una elaborazione statistica su N misurazioni produce come risultato:

\begin{list}{-}{}
    \item{} la miglior stima della grandezza, $x_{stima}$;
    \item{} l'\textbf{incertezza}, che quantifica quanto siamo sicuri che la stima sia accurata e precisa;
    \item{} se serve, un\textbf{unità di misura}.
\end{list}
 
Per esempio, la lunghezza di un tavolo può essere espressa come:

$$
    l = (2,15 \; \pm \; 0,01)\;m.
$$

La misura di una grandezza rappresenta il rapporto tra la grandezza considerata e un campione di riferimento. Il numero è generalmente accompagnato da un'unità di misura; quando non lo è, si dice \textit{adimensionato}.

La misurazione può essere effettuata in tre modi.

\subsection{Tipi di misurazione}

\paragraph{Misura diretta} Utilizzando uno strumento di misura, si confronta la grandezza con un'altra della stessa specie scelta come unità. Ovvero si determina quante volte l'unità è contenuta nella grandezza. Il risultato è sempre un numero razionale positivo.
\subparagraph{NB} la misurazione diretta presuppone che $1 u + 1 u = 2 u.$

\paragraph{Misura indiretta} Alcune grandezze non sono definite per confronto, ma come funzione di altre grandezze. Data una grandezza fisica $y = f(x_1, x_2, x_3, \dots)$, può essere che essa non sia misurabile direttamente. In questo caso si procede misuranso singolarmente le grandezze $x_1,\;x_2,\;x_3,\;\dots$ e poi calcolando y e trattando opportunamente le incertezze sulle singole grandezze.

\paragraph{Misura con strumento tarato}

Uno strumento tarato è uno strumento che garantisce una corrispondenza biunivoca tra la misura della grandezza in oggetto e il valore leggibile sullo strumento. La funzione che garantisce tale corrispondenza si dice \textbf{curva di taratura} o \textbf{di calibrazione}.

\subparagraph{E.g.} misurazione con righello.

\subparagraph{I parametri degli strumenti tarati}
\begin{list}{-}{}
    \item \textbf{Sensibilità}: ovvero la minima variazione che la grandezza deve riscontrare perché lo strumento la possa percepire.
    \item \textbf{Precisione}: ovvero la massima differenza tra il valore misurato della grandezza e la grandezza: il massimo errore.
    \item \textbf{Prontezza}: ovvero il tempo che lo strumento impiega per mostrare una variazione della misura in seguito alla variazione della grandezza; la rapidità che lo strumento impiega per essettuare una misurazione.
    \item \textbf{Portata}: ovvero il massimo valore della grandezza misurabile dallo strumento.
\end{list}

\subsection{Analisi dimensionale di una misura}
Il valore di una grandezza fisica va sempre accompagnato dalla sua unità di misura. In un sistema di misura ogni grandezza può essere definita dalle grandezze fondamentali.

\paragraph{Attenzione} in ogni equazione, tutti i membri devono avere la stessa unità di misura.

\subsection{Incertezza \& Co.}
L'incertezza può derivare da divrsi fattori:
\begin{list}{-}{}
    \item stato del misurando;
    \item procedimento di misurazione (errori nel metodo);
    \item campione/strumento di misura utilizzati (taratura impropria, campione impreciso, ecc.);
    \item operatore (capacità manuali limitate, errori di distrazione, ecc.).
\end{list}

\subparagraph{Osservazione} Per incertezza non si intende \textit{sbaglio}: nessuna misura è esente da icertezza, non importa quanto sia effettuata bene. L'unica cosa che si può fare è accertarsi che le incertezze siano le più piccole possibili.

\paragraph{La miglior stima e altre definizioni}
Si ipotizzi di misurare la durata di un fenomeno utilizzando un cronometro. Se il fenomeno è di breve durata, la principale fonte di errore sarà il tempo di reazione (incognito) nel far partire e fermare il cronometro. Questo tipo di incertezza può essere simata ripetendo la misurazione molte volte.

Date le misurazioni $x_1,\; x_2,\; x_3,\; \dots$ diemo le seguenti definizioni.

\subsubsection{Miglior stima} 
$$
    t_n = \dfrac{1}{n} \cdot \sum_i x_i
$$

\subsubsection{Intervallo possibile} 
$$
    x \in [x_{min}, \;x_{max}]; \;\;\;\;\; \Delta t = x_{max} - x_{min}
$$

\subsubsection{Incertezza} 
$$
    \dfrac{1}{2}\cdot\Delta t
$$

\subsubsection{Il risultato} 
$$
    (t_n \pm \dfrac{1}{2}\cdot\Delta t) u
$$

\subsubsection{Definizioni ulteriori}
\paragraph{Cifre significative} L'incertezza non può avere più di due cifre significative, se a prima cifra significativa è un uno o un due, in generale conviene tenere due cifre significative.

\paragraph{Discrepanza} Se due misure dello stesso fenomeno sono in disaccordo vi è una discrepanza. Se la discrepanza è minore dell'incertezza, le due misure sono consistenti e la discrepanza non è significativa. Se invece la discrepanza è maggiore delle incertezze, allora le misure sono inconsistenti e la discrepanza è significativa.

\paragraph{Incertezza (o errore) strumentale} A volte i risultati di diverse misurazioni possono essere uguali tra loro perché la misurazione è stata effettuata con il medesimo strumento tarato male.

\paragraph{Valore accettato} Per misurazioni che sono state ripetute molte volte, esiste in genere un “valore accettato” molto preciso, anceh se sempre affetto da incertezza. Per esempio, l'accellerazione di gravità a Torino è
$$
    g = 9,80549 \; \pm \; 0,00001 \; m/s^2.
$$

\paragraph{Precisione} Caratteristica globale e di tipo generico che tiene conto di ripetibilità e accuratezza.

\paragraph{Ripetibilità} Grado di riproducibilità della misurazione.

\paragraph{Accuratezza} Capacità dello strumento di dare misure prossime al campione nazionale di misurazione.

\paragraph{Tipi di incertezza}
\subparagraph{Tipo A} Errori statistici o casuali; sono docuti a diversi fattori imprevedibili.
\subparagraph{Tipo B} Errori dovuti al metodo o alle apparecchiature.

\begin{figure}
    \centerline{\includegraphics[width=5in]{src/i1.png}}
    \caption{Incertezze di tipo ab; aB; Ab; AB}
\end{figure}

\subsection{Analisi statistica}
Applicabile per errori di Tipo B piccoli.

% -----------------------------------
Lamiglior stima della misura è la media delle misurazione:
$$
    \bar{x} = \dfrac{1}{n} \sum_ix_i
$$

mentre l'incertezza media è data dalla deviazione standard:
$$
    \delta_x = \sigma_x = \sqrt{\dfrac{1}{N - 1} \sum_i{(x_i-\bar{x})^2}}
$$

mentre l'incertezza è data da:
$$
    \delta_{\bar{x}} = \dfrac{\delta_x}{\sqrt{N}}.
$$

In generale una grandezza non è misurabile direttamente ma è una funzione di diverse misurazioni.
$$
    q = f(x_1, x_2, x_3, \dots).
$$

\paragraph{Cerchiamo ora di capire come questi errori si propagano attravrso i calcoli}

Se q fosse solo $f$ di $x$, si avrebbe

$$
    \delta q = q(\bar{x} + \delta x) - q(\delta x),
$$

da cui (regole differenziali):

$$
    \delta q = \Big|\dfrac{dq}{dx}\Big|\delta x,
$$

se poi $q$ è $f$ di $x$ e $y$:
$$
    \delta q = \Big|\dfrac{dq}{dx}\Big|\delta x + \Big|\dfrac{dq}{dy}\Big|\delta y \; \left[+ \Big|\dfrac{dq}{dx_3}\Big|\delta x_3\right].
$$

Se $x$ e $y$ sono indipendenti,
$$
    \delta q = \sqrt{\left(\dfrac{dq}{dx}\delta x\right)^2 + \left(\dfrac{dq}{dy}\delta y\right)^2}
$$

\subparagraph{Esempio}
La misura di $\theta$ è $(30 \pm 3)^\circ$; quanto vale $sin(\theta)$?

Ovviamente

$$
    \bar{\theta} = 30^\circ;\;\;\; \delta\theta = 3^\circ\;\;\; e\;\;\; q(\theta) = sin(\theta).
$$

Dunque:

$$
    \delta sin(\theta) = \big|\dfrac{d}{d\theta}sin(\theta)\big|\;\delta\theta = \big|cos(\theta)\big|\;\delta\theta = 0,50 \pm 0,04
$$

\subsection{Stima su somma e differenza}

Considerato
$$
    q = x + y, \;\;\; con \;\; x = \bar{x} \pm \delta x \;\; e \;\; y = \bar{y} \pm \delta y,
$$

si ricava facilmente
$$
    q_{max \;[min]} = x_{max \;[min]} + y_{max \;[min]}
$$

da cui:
$$
    \delta q = \delta x + \delta y
$$

Se $x$ e $y$ sono indipendenti:
$$
    \delta q = \sqrt{\delta x^2 + \delta y^2}
$$

\subsection{Stima su prodotti e quozienti}
$$
    \dfrac{\delta q}{q} = \Big|\dfrac{\delta x}{x}\Big| + \Big|\dfrac{\delta y}{y}\Big| + \Big|\dfrac{\delta z}{z}\Big| + \dots \;.
$$

Per misura indipedente e incertezza di tipo A:
$$
    \dfrac{\delta q}{q} = \sqrt{\Big(\dfrac{\delta x}{x}\Big)^2 + \Big(\dfrac{\delta y}{y}\Big)^2 + \Big(\dfrac{\delta z}{z}\Big)^2}.
$$

\subsection{Stima su potenze}
$$
    \dfrac{\delta q}{q} = \dfrac{1}{q} \Big|\dfrac{\partial q}{\partial x}\Big| \delta x = \dfrac{1}{x^n} \big|n\;x^{n-1}\big|\delta x = n\dfrac{\delta x}{x}.
$$

In generale:
$$
    \dfrac{\delta q}{q} = a\Big|\dfrac{\delta x}{x}\Big| + b\Big|\dfrac{\delta y}{y}\Big| + \dots\;.
$$

Per misure indipendenti:
$$
    \dfrac{\delta q}{q} = \sqrt{\Big(a\dfrac{\delta x}{x}\Big) + \Big(b\dfrac{\delta y}{y}\Big) + \dots} \;.
$$

\subsection{I ragionamenti alla base}

\paragraph{La media aritemetica} Perché utiliziamo la media aritmetica come miglior stima? La miglior stima, per buonsenso, è il valore che minimizza l'errore. La miglior stima, in altre parole, è il valore per cui $\delta_x$ è minimo. Dnque possiamo impostare il problema come di seguitio:

$$
    \dfrac{\partial\Big( \sum_{i = 1}^{N} \big| x_i - \bar{x} \big| ^2 \Big) }{\partial x_i} = 0 \Longrightarrow
    2\sum_{i = 1}^{N} \big(x_i - \bar{x} \big) = 0 \Longrightarrow
    % \sum_{i = 1}^{N} x_i = \sum_{i = 1}^{N} \bar{x} \Longrightarrow
    \sum_{i = 1}^{N} x_i = N \bar{x} \Longrightarrow
    \bar{x} = \dfrac{1}{N} \sum_{i = 1}^{N} x_i
$$

% ------------------------
\paragraph{L'incertezza come somma degli scarti} Ipotesi: prendiamo la somma degli scarti come misura dell'incertezza del nostro esperimento e la media degli scarti come ncertezza media. Ha senso?

Definiamo lo scarto della misurazione $x_i$ come
$$
    d_i = x_i - \bar{x},
$$

si può dimostrare che la sommatoria degli scarti è sempre zero; vediamo come.
$$
    \sum_{i = 1}^{N} d_i =
    \sum_{i = 1}^{N} \big(x - \bar{x} \big) =
    \sum_{i = 1}^{N} x_i - \sum_{i = 1}^{N} \bar{x} =
    \sum_{i = 1}^{N} x_i - \sum_{i = 1}^{N} \Big( \dfrac{1}{N} \sum_{i = 1}^{N} x_i \Big) =
    \sum_{i = 1}^{N} x_i - \sum_{i = 1}^{N} x_i = 0
$$

Ciò significa dunque che l'idea di usare lo scarto medio come incertezza non può funzionare.

Le due possibili soluzioni sono
\begin{list}{-}{}
    \item $\sum_{i = 1}^{N} \big|d_i\big|$
    \item $\sum_{i = 1}^{N} d_i^2$
\end{list}

Scegliamo la seconda opzione. (daremo dopo una giustificazione). Definiamo la \textbf{varianza campionaria}:
$$
    s_x^2 = \sum_{i = 1}^{N} d_1^2.
$$

\subsubsection{La distribuzione normale}
\paragraph{Forma compatta del valor medio}
Consideriamo i risultati di $N$ misurazioni di $x$, supponendo che il valore $x_i$ sia stato campionato $n_i$ volte. Avremo:
$$
    x_i \in \{x_1, x_1, \dots, x_2, x_2, \dots, x_3, x_3, \dots\}
$$
$$
    \bar{x} = \dfrac {1} {N} \sum_{i = 1}^{N} \big(n_i \cdot x_i\big);\;\;\; N = \sum_{i = 1}^{N} n_i
$$

Andiamo ora a definire la frequenza $f_i$ con cui il calore $x_i$ compare tra le misurazioni e riscriviamo la miglior stima in $f$ di tale frequenza:

$$
    f_i = \dfrac {n_i} N \Longrightarrow
    \bar x = \sum_{i = 1}^{N} x_i f_i
$$

\paragraph{Il grafico della distribuzione normale} Consideriamo i risultati di $N$ misurazioni di $x$, supponendo che il valore $x_i$ sia stato campionato $n_i$ volte. 

\begin{figure}
    \centerline{\includegraphics[width=3.5in]{src/i2.png}}
    \caption{Il grafico della distribuzione normale: la gaussiana.}
\end{figure}

Avremo:
$$
    x_i \in \{x_1, x_1, \dots, x_2, x_2, \dots, x_3, x_3, \dots\}
$$
$$
    \bar{x} = \dfrac {1} {N} \sum_{i = 1}^{N} \big(n_i \cdot x_i\big);\;\;\; N = \sum_{i = 1}^{N} n_i
$$

Andiamo ora a definire la frequenza $f_i$ con cui il calore $x_i$ compare tra le misurazioni e riscriviamo la miglior stima in $f$ di tale frequenza:

$$
    f_i = \dfrac {n_i} N \Longrightarrow
    \bar x = \sum_{i = 1}^{N} x_i f_i
$$

\subparagraph{La probabilità}
\begin{list}{-}{}
    \item Probabilità di ottenere un valore in $[a, b]$:
$$
\int_a^b f(x) dx;
$$

    \item normalizzazione:
$$
\int_{-\infty}^{+\infty} f(x) dx = 1;
$$

\item valor medio:
$$
\int_{-\infty}^{\infty} x f(x) dx = \mu \;\;\; \Big[ = \bar{x} \Big];
$$

    \item incertezza
$$
\int_{-\infty}^{+\infty} \big( x - \mu \big)^2 f(x) dx = \sigma_x^2.
$$
\end{list}

\subparagraph{La gaussiana} Dato un numero di misure grandissime, la frequenza tendera a una curva detta gaussiana, definibile come:
$$
    f_G(x) = \dfrac{ 1 }{ \sqrt{ 2\pi } \sigma_x } \; e^{ - \dfrac{ (x - \mu)^2 }{ 2 \sigma_x^2 } }.
$$

\subsubsection{Teorema del limite centrale}
No dim.

L'insieme di tutte le possibili medie campionarie di campioni di N misure casuali hanno asintoticamente una distribuzione normale di media $\mu$ e deviazione standard $\dfrac{\sigma_x}{\sqrt N}$. Qui $\mu$ e $\dfrac{\sigma_x}{\sqrt N}$ sono la media e la deviazione standard – rispettivamente – della popolazione da cui si sceglie il campione.

\subsubsection{Il valor medio, miglior stima di \textmu}
Una volta capito che $\mu$ è l'analogo della miglior stima introdotta come media delle misurazioni discrete, diamo una definizione della miglior stima $\mu$ “nel continuo”: $\mu$ è quel valore che massimizza la probabilità degli $x_i$. sapendo che la probabilità segue l'andamento di $f_G$,

$$
    \dfrac{\partial P(x_1, x_2, \dots, x_n)}{\partial\mu} = 0 \Longrightarrow
    \sum_{i = 1}^{N} \big(x_i - \mu\big) = 0 \Longrightarrow
    \mu = \dfrac{1}{N} \sum_{i = 1}^{N} x_i = \bar{x}
$$

% -----------
\subsection{Teorema}
Dati $\mu$ e $\sigma$, se calcolassimo la varianza campionaria

$$
    s_x^2 = \sum_{i=1}^{N} \dfrac{(x_i-\bar{x})^2}{N-1}
$$

per ogni campione di N elementi, la media delle varianze campionarie è la varianza della popolazione di elementi $\omega_i$ di $\tilde N \geq N$:

$$
    \sigma_x^2 = \sum_{i=1}^{\tilde{N}} \dfrac{(\omega_i-\mu)^2}{\tilde{N}}
$$

\paragraph{Dim.}
$$
 \sum_{i=1}^{N}\big(x_i-\bar{x}\big)^2 =
 \sum_{i=1}^{N}\big(x_i-\mu+\mu-\bar{x}\big)^2 =
$$

sviluppo considerando $(a+b)^2$, con $a = x_i - \mu$, $b = \mu - \bar{x}$:
$$
    \sum_{i=1}^{N} \big(x_i-\mu)^2 + N(\mu-\bar{x})^2 + 2(\mu-\bar{x})\sum_{i=1}^{N}(x_i-\mu)
$$

Notiamo che

$$
    2(\mu-\bar{x})\sum_{i=1}^{N}(x_i-\mu) =
    2(\mu-\bar{x})N\bar{x}-N\mu =
    2N(\mu - \bar{x})(\bar{x}-\mu) =
    -2N(\mu - \bar{x})^2.
$$

Dunque abbiamo:
$$
    \sum_{i=1}^{N}\big(x_i-\bar{x}\big)^2 = \sum_{i=1}^{N} \big(x_i-\mu)^2 - N(\mu - \bar{x})^2. \;\;\;\;\;\;(X)
$$

Siccome ogni $x_i$ è un membro della popolazione $\omega_i$ , mediamente:
$$
    (x_i - \mu)^2 = (\omega_i - \mu)^2
$$

Quindi:
$$
    \sum_{i=1}^{N}\big(x_i-\mu\big)^2 =
    \sum_{i=1}^{N}\big(\omega_i-\mu\big)^2 =
    N\sigma_x^2
$$

Per il teorema del limite centrale, e ricordando *(X)*:
$$
    \sum_{i=1}^{N}\big(x_i-\bar{x}\big)^2 = N\sigma_x^2-\sigma_x^2 = (N-1)\sigma_x^2 = (N - 1)\sigma_x^2.
$$

\textbf{CVD}

Saltiamo a piè pari la parte sulle incertezze (vedere solo la parte sopra)

