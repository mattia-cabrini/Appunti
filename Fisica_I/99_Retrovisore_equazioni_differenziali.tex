\section{Specchietto per la risouzione di equazioni differenziali}

Lo scopo di questa pagina è quello di elencare le principali procedure per la risoluzione di equazioni differenziali di I e II ordine di utilità per il corso di Fisica I.

\paragraph{Notazione}
\begin{list}{-}{}
    \item Sarà sempre incognita $y(t)$.
    \item Si utilizza per indicare la derivata ora la notazione di Newton $\dfrac{dy}{dt}$ ora la notazione di Lagrange $y'(t)$.
\end{list}

\subsection{Caso banale}
$$
    y'(t) = f(t)
$$

Soluzione:
$$
    y(t) = \int f(t) dt + c
$$

\subsection{Eq. differenziali del I ordine}
$$
    y'(t) + a(t)y(t) = f(t)
$$

\paragraph{Procedimento} 
Si moltiblicano entrambi i membri per
$$
    e^{A(t)}, \;\;\; con \;
    A(t) = \int a(t) dt
$$

Si trova dunque l'eq.:
$$
    e^{A(t)}y'(t) + e^{A(t)}a(t)y(t) = f(t)
$$

Si interpreta il primo membro e si ottiene:
$$
    \dfrac{d}{dt} \bigg(e^{A(t)}y(t)\bigg) = f(t)
$$

Si integra e si ottiene la **soluzione**:
$$
    e^{A(t)} y(t) = \int f(t) dt
    \\\;\\ \Longrightarrow
    y(t) = e^{-A(t)} \Big( F(t) + c \Big) =
    y(t) = e^{-\int a(t) dt} \Big( F(t) + c \Big)
$$

\subsection{Cauchy}
$$
    y(t_0) = y_0 \Longrightarrow c = e^{A(t_0)} y_0
$$

\subsection{Ordine n}
$$
    a_ny^n(t) + a_{n-1}y^{n-1}(t) + ... a_1y'(t) + a_0y(t) = f(t)
$$

\subsubsection{Soluzione generale}
Si può dimostrare che la soluzone generale di tale equazione è data da
$$
    y(t) = \sum_{k = 1}^n c_ky_k(t) + y_*(t)
$$

Laddove la sommatoria rappresenta la soluzione generale dell'equazione (sufficiente se *f(t)* è identicamente nulla); essa è anche detta soluzione dell'equazione omogenea. Mentre il secondo cotributo,
$$
    y_*(t),
$$

è una soluzione particolare.

\subsubsection{Procedimento generale}
Si risolve l'equazion algebrica associata al polinomio:
$$
    a_nz^n + a_{n-1}z^{n-1} + ... + a_1z + a_0 = 0
$$

Si trova la soluzione generale; per ogni soluzione $\alpha$ con molteplicità *m*:
$$
    y_{o, \;\alpha}(t) = e^{\alpha t} + xe^{\alpha t} + x^2e^{\alpha t} + \dots + x^{m - 1}e^{\alpha t}
$$

La soluzione generale sarà data da:
$$
    y_o(t) = c_1y_{o, \;\alpha}(t) + c_2y_{o, \;\beta}(t) + \dots
$$

Se la radice $p = (\alpha + i\beta)$ è complessa:
$$
    y_{o \;p} = e^{\alpha t}\cos\big( \beta t \big) + xe^{\alpha t}\cos\big( \beta t \big) + \dots + x^{m - 1}e^{\alpha t}\cos\big( \beta t \big) \\
    e^{\alpha t}\sin\big( \beta t \big) + xe^{\alpha t}\sin\big( \beta t \big) + \dots + x^{m - 1}e^{\alpha t}\sin\big( \beta t \big) \\
$$

\subsubsection{Casi semplici di ordine II}

\paragraph{Esempio 1} Radici distinte.
$$
    z^2 - 2z -3 = 0
$$

È chiaro che le radici sono *-1* e *3*. La soluzione:
$$
    y(t) = c_1e^{-t} + c_2e^{3t}.
$$

\paragraph{Esempio 2} Radici coincidenti.
$$
    z^2 + 4z + 4 = 0
$$

L'unica radice è *-2* con *m = 2*. La soluzione:
$$
    y(t) = c_1e^{-2t} + c_1xe^{-2t} = \Big(c_1 + c_2x\Big) e^{-2t}.
$$

\paragraph{Esempio 3} Radice complessa.
$$
    z^2 + 2z + 5 = 0
$$

L'unica radice è *-1 + 2i*. La soluzione:
$$
    y(t) = c_1e^{-t}\cos(2t) + c_2e^{-t}\sin(2t) =
    \Big( c_1\cos(2t) + c_2\sin(t) \Big) e^{-t}.
$$

\subsubsection{Soluzione particolare per eq. differenziali di ordine II}
Metodo di variazione delle costanti

Una volta trovata la soluzione omogenea di un'equazione differenziale di II ordine, espressa nella forma
$$
    y_o = c_1 y_{o,1} + c_2 y_{o, 2},
$$

per trovare la soluzione particolare è sufficiente considerare $c_1$ e $c_2$ come dipendenti da *t* e risolvere rispetto ad esse il sistema:
$$
    \begin{cases}
        c_1'(t)y_{o,1}(t) + c_2'(t)y_{o,2}(t) = 0 \\
        c_1'(t)y'_{o,1}(t) + c_2'(t)y'_{o,2}(t) = f(t)
    \end{cases}.
$$

La soluzione particolare sarà
$$
    y_*(t) = c_1(t)y_{o, 1}(t) + c_2(t)y_{o,2}(t).
$$

\paragraph{Esempio}
$$
    y''(t) + 2y'(t) = 1
$$

\paragraph{Svolgimento}
Le radici del polinomio associato sono *0* e *-2*, dunque:
$$
    y_o = c_1 + c_2 e^{-2t}.
$$

Applico il metodo di variazione delle costanti e risolvo il sistema:
$$
    \begin{cases}
        c'_1(t) + c'_2(t)e^{-2t} = 0 \\
        -2 c'_2(t) e^{-2t} = 1
    \end{cases}
$$

Calcolando $c_2$:
$$
    c'_2(t) = - \dfrac 1 2 e^{2t} \Longrightarrow
    c_2(t) = - \dfrac 1 2 \int e^{2t} dt = - \dfrac 1 4 e^{2t}
$$

Calcolando $c_2$:
$$
    c'_1 = -c'_2 e^{-2t} = \dfrac 1 2 e^{2t} e^{-2t} = \dfrac 1 2
$$
$$\Longrightarrow
    c_1(t) = \dfrac 1 2 \int dt = \dfrac 1 2 t
$$

La soluzione particolare è dunque:
$$
    y_*(t) = \dfrac 1 2 t - \dfrac 1 4
$$

La soluzione completa sarà dunque:
$$
    y(t) % = y_o(t) + y_{*}(t) = c_1 + c_2 e^{-2t} + \dfrac{1}{2} t \cancel{- \dfrac{1}{4}}
$$

\subsubsection{Caso particolare all'ordine II}
\paragraph{Omogenea}
$$
    y''(t) + \omega^2 y(t) = 0
    \Longrightarrow
    y(t) = A \sin(\omega t + \phi)
$$

\paragraph{Non omogenea $f(t)$ costante}
$$
    y''(t) + \omega^2 y(t) = a
    \Longrightarrow
    \begin{cases}
        y_o(t) = A \sin(\omega t + \phi) \\
        y_*(t) = \dfrac a{\omega^2}
    \end{cases}
$$
