# Reti di trasporto

*def.* Una rete si dice **di trasporto** se è atta a creare l'interconnessione tra reti di accesso.

Queste reti hanno come nodi dei commutatori telefonici e dati (router) in una topologia magliata e gerarchica. Tali nodi sono inteconnessi da linee as alta velocità (solitamente in fibra ottica).

## La trasmissione

La trasmissione dei dati sulle reti di trasporto avviene esclusivamente in digitale e si è evoluta dalla rete telefonica tradizionale. La trasmissione è strutturata secondo principi di multiplazione *gerarchica* a *divisione di tempo*.

Questo significa che i tributari non possono scrivere sul mezzo trasmissivo (o canale) in ogni momento ma dovrà aspettare il proprio turno in forza della multiplazione di tempo.

I sistemi di multiplazione gerarchica si sono evoluti a partire dalla Pleisochronous Digital Hierarchy (PDH). La PDH era pensata per canali da $6 kbps$ senza store and forward (come quasi tutti i protocolli di trasmissione a livello fisico) in un sistema quasi sincrono (il che significa che non è veramente sincrono, ma che rispetta un certo intervallo di tolleranza). Per mantenere il sincronismo, in particolare, era prevista l'inserzione/la rimozione dinamica di bit lungo il percorso: **bit stuffing**. Essendo pensata per il bitrate delle comunicazioni vocali, la velocità di trasmissione era limitata. Come tipico delle reti telefoniche, poi, ogni regione aveva un suo standard (USA, Europa, Giappone).

Viene introdotto negli anni ${}^\prime70$ lo standard SONET/SDH. Si tratta di una gererchia sincrona, prevede sempre una multiplazione di tempo. Questo standard, ancora in utilizzo, prevede anche la gestione di trame più complesse che, tra le altre cose, permettono di gestire gli errori.
