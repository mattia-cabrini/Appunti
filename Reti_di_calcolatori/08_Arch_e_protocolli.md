# Architetture e protocolli

*def.* Si definisce **protocollo** la descrizione formale delle procedure adottate per assicurare la comunicazione tra due o più oggetti dello stesso livello gerarchico.

Un protocollo prevede:

* tipologia: richieste o risposte;
* sintassi: struttura dei messaggi;
* semantica: significato di campi di bit dentro ai messaggi;
* temporizzazione: sequenze temporali di comandi e risposte.

*def.* Si definisce **architettura** di rete ciò che definisce il processo di comunicazione tra dispositivi diversi, le relazioni tra le entità coinvolte, le funzioni necessarie per la comunicazione e le modalità organizzative delle funzioni.

* * *
**Esempio**
Cosa si intende per *organizzazione delle funzioni* e *funzioni necessarie*.

Supponiamo la seguente architettura:

<div align="center">
  <img src="src/i4.svg" />
</div>

e supponiamo che il telefono debba inviare un'imagine al server.

In un primo momento il telefono invierà i dati all'antenna. L'antenna cosa farà con i dati dell'imagine? Certamente non si metterà a risostruire il file, del resto all'antenna non interessa cosa il telefono stia trasmettendo, l'unica cosa che le interessa è inviare i bit che ha ricevuto al successivo nodo di comunicazione. Al massimo, il sistema di controllo dell'antenna potrà verificare se i bit ricevuti siano o meno corretti se il protocollo prevede dei controlli di correttezza sull'invio dei singoli bit (e.g. controlli di parità).

Cosa interessa fare invece al nodo di comunicazione? Anche in questo caso la ricostruzione dell'immagine (nonché dell'intero messaggio) non è di interesse. L'unica cosa che interessa al nodo è effettuare la commutazione di pacchetto, dunque anche lo store and forward. Per effettuare lo store and forward è ovviamente necessario ricostruire i pacchetti, dunque questa sarà una funzione del nodo di comunicazione.

Solo il server, alla fine, si occuperà di ricostruire l'immagine (oltre che ricostruire i pacchetti a partire dei bit ricevuti).
* * *

Si usano **architetture stratificate**, ovvero il processo che trasmette un file non è un processo continuo, ma un processo che prevede passaggi successivi. Ogni passaggio è eseguito da un elemento (hw o sw) che modifica il file aggiungendo o toggliendo qualcosa, oppure segmentandolo o ricomponendolo. Il passaggio va inteso tra strati architetturali diversi.

* * *
Sull'esempio di prima:

* **strato 1**: scomposizione dell'immagine in pacchetti;
* **strato 2**: scomposizione dei pacchetti in bit e composizione di un bit stream da inviare all'antenna (non solo con i bit del pacchetto, ma anche con i bit di controllo e, in generale, come previsto dal protocollo di riferimento);
* **strato 3**: invio dei bit all'antenna.

Che non sono gli strati veri, ma danno un'idea del concetto.
* * *

## Il modello a strati

Vediamo i modelli a strati di riferimento che sono stati ideati nel corso degli anni.

|  ISO-OSI   |   DECNET    |    ARPA    |        SNA         |
|:-----------|:------------|:-----------|:-------------------|
|Application |User         |Application |Transaction Service |
|Presentation|Network Appl.|^           |Presentation Service|
|Session     |Session      |^           |Data Flow
|Transport   |End to End   |Service.    |Transaction Control
|Network     |Routing      |Internetwork|Virtual Route<br />Explicit Route<br />Transmission Group
|Data Link   |Data Link    |Network     |Data Link
|Physical    |Physical     |^           |Physical

Lo strato più alto è quello in cui viene elaborata l'informazione originale che viene modificato lungo i diveri livelli, fino a raggiungere lo stato *fisico*, avvero lo stato di *segnali elettrici*.

Di architetture nel tempo se ne sono susseguiti diversi, lo stendard *de facto*, oggi, è l'ISO-OSI, a volte nella forma TCP-IP. Vediamo di seguito una tabella riassuntiva di strati e protocolli ad essi associati.

|  ISO-OSI   |   TCP-IP    | Internet Protocol Suite [IETF]      |
|:-----------|:------------|:------------------------------------|
|Application |Application  | Telnet, HTTP, FTP, SMTP, SNMP, ...  |
|Presentation| ^           | XDR, ...                            |
|Session     | ^           | RPC, ...                            |
|Transport   |Transport    | TCP e UDP                           |
|Network     |Network      | IP, ICMP, ARP, RARP, ...            |
|Data Link   |Data Link    | ARP, ...                            |
|Physical    |Physical     | Non specificati, dipende dall'hw... |

In modo astratto, ogni nodo è un **sistema**. Ogni sistema è collegato a un altro attraverso mezzi trasmissivi, ed eventualmente attraverso altri sistemi.

### Il sistema

Ogni sistema è composto da sottosistemi. Ogni sottosistema realizza le funzioni specifiche di uno strato tramite delle entità.

Con entità si intende in modo concreto un processo in esecuzione, con uno specifico task; generalmente si chiamano *N-entità* laddove *N* è il numero dello strato.

Gli strati sono numerati da *1* a *N* a partire dal livello fisico, a salire.

Ovviamente due entità di strati diversi possono "parlarsi", in modo da consentire la *discesa* dei dati dallo strato più alto a quello più basso. In questo senso, ogni strato:

* offre servizi allo strato superiore;
* usa le sue entità;
* sfrutta i servizi offerti dallo strato inferiore.

Come in ogni contesto informatico, l'offerta di un servizio da parte di uno strato allo strato supriore avviene attraverso un'API. Gli utenti dello strato *N*, le *(N + 1)-entità* cooperano e comunicano usando l'*(N)-servizio* fornito dall *(N)-fornitore di servizio*.
$$
  N-strato \;\;\;
  \begin{matrix}
    \xleftarrow[]{\;\;\;usa\;\;\;} \\
    \xrightarrow[fornisce]{} \\
  \end{matrix} \;\;\;
  (N+1)-strato
$$

Il servizio si classifica poi in:

* **connection-oriented [CO]**, ovvero si stabilisce una connessione allocando delle risorse, si trasmettono i dati e si rilascia la connessione (§Circuito virtuale, anche se non lo è necessariamente);
* **connectionless [CL]**, ovvero i dati vengono immessi in rete senza un accordo preliminare e sono trattati in modo indipendente (§Datagram).

**Astrazione**
Si noti che gl istrati iferiori all'applicazione sono *a black box*, ovvero l'utente finale non sa neanche che esistano. In generale, dal punto di vista dello strato *N*, non c'è consapevolezza rispetto a cosa ci sia dallo strato *N - 1* allo trato *1*: per lo strato *N* i sottostrati sono *in a black box*; lo strato *N* è consapevole solo delle API offerte dalo strato *N - 1*.

Ma in tutto questo come si incastrano i protocolli?

### Strati e protocolli

Il protocollo non definisce il modo in cui gli strati di un sistema comunicano.
> Se spedissi una lettera a un amico in Germania, che parla solo tedesco, il postino (API) dovrebbe conoscere il tedesco per poter consegnare la lettera? Ovviamente no.

I protocolli definiscono il modo con cui vengono scambiate invormazioni tra due strati omologhi, ovvero lo scambio di informazioni tra *N-entità* omologhe di sistemi diversi.

Per capirci, lo strato Trasporto del sistema A parla TCP e UDP, esattamente come lo strato Trasporto del sistema B.

Il TCP e l'UDP del sistema A vengono impacchettati in buste dagli strati inferiori al Trasporto sul sistema A e inviati, tramite i sottosistemi al sottosistema di strato *1* del sistema B, che ricomporrà i dati per il sottosistema di strato *2*, e così via fino a tornare ad essere TCP o UDP per il sottosistema di strato Trasporto del sistema B.

Tuttavia, tra strati diversi non si parla alcun protocollo specifico, bensì protocolli differenti a seconda di quale sia il SO.

### Connessione

*def.* Si definisce **connessione** una relazione esistente tra SAP diversi (sullo stesso strato) per lo scambio di dati tra interfacce. I sottosistemi di strato *N* possono trovarsi a gestire connessioni diverse, dunque occorre che gli strati superiori possano specificare a quale connessione in particolare un certo pacchetto è riferito (vedi più avanti il concetto di **numero di porta**).

## Creazione PDU

In un sistema a strati ci sono diversi tipi di pacchetto, in particolare i dati allo strato *N* sono impacchettati in **N-SDU**, ovvero in Service Data Unit di strato N. Lo strato *N* aggiunge le proprie informazioni di controllo ai dati RAW, dette **N-PCI**, Protocl Control Information, più comunemente detta *intestazione*.

*N-PCI* + *N-SDU* formano una *N-PDU*.

Ogni strato inferiore tratta la *N-PDU* come una "busta chiusa" e la processa aggiungendo un'intestazione (dopo averla eventualmente frammentata) creando un certo numero di *(N-1)-PDU*.

<div align="center">
  <img src="src/i5.svg" />
</div>

In fase di riceszione, da uno strato all'altro, le intestazioni verranno poi tolte anziché aggiunte.

### I limiti di lunghezza, usiami i pacchetti!

I pacchetti per definizione servono a non inviare i dati così come sono, ma suddivisi. L'operazione di suddivisione dei pacchetti si chiama **segmendazione** [TX], l'operazione che ricompone i dati segmantati **concatenazione** [RX].

In particolare esistono dei limiti di lunghezza. Ogni strato può avere il suo. Quando un segmanto (un SDU) è troppo lunga, essa viene segmentata in due o più N-PDU di lunghezza minore e nelle cui N-PCI sarà contenuta l'informazione per deterinare qule N-PDU sia il primo, quale il secondo, eccetera.

<div align="center">
  <img src="src/i6.svg" />
</div>

Ovviamente segmantare i dati significa anche aumentare l'overhead e i bit trasmessi (§Commutazione di pacchetto). In generale si definisce una **MTU** (Maximum Trasnfert Unit), cioè la massima lunghetta di un PDU. Definita una MTU, il livello più altro si deve impegnare a segmentare i dati in pacchetti di lunghezza non superiore alla MTU in modo tale che i sottostrati non debbano ri-segmentare.

* * *
**Coffee break**
Strato = Livello
* * *

### ISO-OSI, iniziamo a parlarne

#### Livello fisico

* Fornisce i mezzi meccanici, fisici, funzionali e procedurali per attivare, mantenere e disattivare le connessoni fisiche.
* Ha il compito di effettuare il trasferimento delle cifre binarie scambiate dalle entità di strato di collegamento.
* Le unità sono bit o simboli.
* Definizione di codifiche di linea, connettori, livelli di tensione.

#### Data link

* Fornisce mezzi funzionali e procedurali per il trasferimento delle unità dati tra entità di strato rete e per fronteggiare malfunzionamenti dello strato fisico.
* Funzioni fondamentali:
  * delimitazione delle unità dati, quali bit fanno parte di un 1-PDU e quali del successivo/precedente;
  * rivelazione e recupero degli errori di trasmissione;
  * controllo di flusso, posso trasmettere o sta già trasmettendo qualcun'altro e devo aspettare?

#### Network

* Fornisce i mezzi per instaurare, mantenere e abbattere le connessioni di rete tra entità di strato trasporto.
* Funzioni fondamentali:
  * instradamento;
  * controllo di flusso e congestione;
  * tariffazione.

#### Transport

* Colma le carenze di qualità di servizio delle sonnessioni di strato rete.
* Funzioni fondamentali:
  * controllo di errore;
  * controllo di frequenza;
  * controllo di flusso.
* Esegue multiplazione e demultiplazione di connessioni.
* Esegue la segmentazione dei dati in pacchetti (MTU) e la loro ricomposizione a destinazione.

#### Sessione

* Assicura al livello superiore una connessione.
* Organizza il colloquio tra le entità di presentazione.
* Struttura e sincronizza lo scambio di dati in modo da poterlo sospendere, riprendere e terminare ordinatamente.
* Maschera le interruzioni del servizio trasporto.
* Spesso integrato nelle funzioni dei livelli superiori.

#### Presentazione

* Risolve i problemi di compatibilità per quanto riguarda la rappresentazione dei dati da trasferire.
* Risolve i problemi relativi alla trasformazione della sintassi dei dati.
* Può fornire servizi di cifratura delle informazioni.
* Spesso integrato nelle funzioni dei livelli superiori.

#### Applicazione

Fornisce ai processi applicativi i mezzi per accedere all'ambiente OSI.

#### Esempio di servizio

* Trasferimento di file
* Terminale virtuale (Telnet, SSH)
* Posta elettronica

## Protocolli a finestra

I protocolli a finestra sono particolari protocolli che compaiono in tutte le reti di TLC moderne e passate. Il loro scopo è **il recupero di errori di trasmissione**, **il controllo di flusso i trasmissione** e **il controllo della sequenza**.

Per esempio, supponendo che un pacchetto sia andato perso (laddove per *perso* si intende *trasmesso con errori*), come fa il trasmittente a sapere quale pacchetto riinviare? O ancora, come fa a sapere quai pacchetti sono arrivati con errori?

Ricordiamo che nessuna trasmissione e esente da errori, a seconda del canale l'errore potrebbe semplicemente essere più o meno probabile. Tale probabilità varia da $10^{-12}$ (fibra ottica) a $10^{-3}$ (canale radio rumoroso). Questo, senza esserne sorpresi, significa che occorre sviluppare delle tecniche di:

* **rilevamento degli errori**;
* **correzione degli errori**.

La tecnica generalmente usata è la codifica di canale. Un'idea potrebbe essere la seguente: su un pacchetto di un certo **utente OSI** (chiamiamo così un'entità di un certo livello) aggiungiamo all'intestazione una serie di **bit di parità**. Il numero di bit di parità dipende dallo scopo del protocollo. Il termine **bit di parità**, poi, non indica necessariamene il controllo di parità, ma genericamente una sequenza di bit che ha lo scopo di verificare se il paccetto trasmesso sia o meno integro.

### Esempi di protezione dagli errori

#### Bit di parità

* Riconosce gli errori in numero dispari;
* non serve a coreggere;
* si utilizza se un errore è più probabile di k errori.

|  Bit of Word  |Bit P|
|:-------------:|:---:|
|1 1 0 1 0 0 1 0|  0  |
|0 1 0 1 0 0 1 0|  1  |

#### Codice a ripetizione

* Decisione a maggioranza;
* permette di correggere gli errori;
* maggiore ridondanza.

|Word #|  Bit of Word  |
|:----:|:-------------:|
| 1    |1 1 0 1 0 0 1 0|
| 1/P1 |0 1 0 1 0 0 1 0|
| 1/P2 |0 1 0 1 0 0 1 0|
| 2    |0 1 0 1 0 0 1 0|
| 2/P1 |0 1 0 1 0 0 1 0|
| 2/P2 |0 1 0 1 0 0 1 0|

#### Parità di riga e colonna

* Permette la correzione di errori singoli;
* minore ridondanza rispetto dl codice di ripetizione;
* maggiore ridondanza rispetto al bit di parità su riga o colonna.

|  Word #  |  Bit of Word  | Bit P/R |
|:--------:|:-------------:|:-------:|
| 1        |1 1 0 1 1 0 1 0| 1       |
| 2        |0 0 0 1 1 1 0 1| 0       |
| 3        |0 1 0 1 1 0 0 0| 1       |
| 4        |1 1 0 0 1 1 1 0| 1       |
| 5        |0 1 1 0 0 1 0 0| 1       |
| 6        |1 1 0 1 0 0 1 0| 0       |
| 7        |1 1 0 1 1 1 0 1| 0       |
| 8        |1 1 0 0 1 1 0 1| 1       |
| Word P/C |1 1 1 1 0 1 1 1| 1       |

Se si verifica un errore su un solo bit, è possibile la correzione incrociando gli errori sulla colonna parità e sulla parola di parità:
|  Word #  |  Bit of Word  | Bit P/R |
|:--------:|:-------------:|:-------:|
| 1        |1 1 0 1 1 0 1 0| 1       |
| 2        |0 0 0 <span style="background-color: #fbb">0</span> 1 1 0 1| <span style="background-color: #fbb">0</span>       |
| 3        |0 1 0 1 1 0 0 0| 1       |
| 4        |1 1 0 0 1 1 1 0| 1       |
| 5        |0 1 1 0 0 1 0 0| 1       |
| 6        |1 1 0 1 0 0 1 0| 0       |
| 7        |1 1 0 1 1 1 0 1| 0       |
| 8        |1 1 0 0 1 1 0 1| 1       |
| Word P/C |1 1 1 <span style="background-color: #fbb">1</span> 0 1 1 1| 1       |

Non è invece possibile correggere *errori rettangolari*:
|  Word #  |  Bit of Word  | Bit P/R |
|:--------:|:-------------:|:-------:|
| 1        |1 1 0 1 1 0 1 0| 1       |
| 2        |0 0 0 <span style="background-color: #fbb">0</span> 1 1 <span style="background-color: #fbb">1</span> 1| <span style="background-color: #beb">0</span>       |
| 3        |0 1 0 1 1 0 0 0| 1       |
| 4        |1 1 0 0 1 1 1 0| 1       |
| 5        |0 1 1 0 0 1 0 0| 1       |
| 6        |1 1 0 1 0 0 1 0| 0       |
| 7        |1 1 0 <span style="background-color: #fbb">0</span> 1 1 <span style="background-color: #fbb">1</span> 1| <span style="background-color: #beb">0</span>       |
| 8        |1 1 0 0 1 1 0 1| 1       |
| Word P/C |1 1 1 <span style="background-color: #beb">1</span> 0 1 <span style="background-color: #beb">1</span> 1| <span style="background-color: #beb">1</span>       |

### Per i protocolli e nei pacchetti

In generale quello che si fa è introdurre dei bit di ridondanza nelle intestazioni dei pacchetti che fungano da bit di controllo. Raramente – per non dire mai – si usano i bit di parità. Alcune delle tecniche utilizzate maggiormente sono:

* **CRC** (Cyclic Redundantcy Check);
* **Internet CHecksum**.

Per quel che riguarda il **recupero** degli errori, si posoono impiegare due tecniche:

* **FEC** (**F**orward **E**rror **C**orrection): i bit di parità (tanti) consentono di analizzare il trasmesso e di correggere i dati errati. Sono implementazioni di questa tecnica le intestazioni con bit di parità per riga o colonna, bit di parita per riga *e* colonna, il codice di ripetizione, e altri;
* **ARQ** (**A**utomatic **R**etransmission re**Q**uest): i bit di ridondanza (pochi) sono usati per cercare di rilevare gli errori e permettere al ricevitore di chiedere la ritrasmizzione della PDU.

Quale delle due tecniche sia preferibile dipende dal contesto, non è decidibile in modo assoluto.

Noi ci occuperemo di ARQ:

### ARQ

> Implementato attraverso un protocollo finestra, non è un protocollo finestra.

ARQ rappresenta un controllo congiunto su una connessione di:

* errori;
* sequenza;
* flusso.

Si introducono nel PCI, sia i **bit di ridondanza**, sia i **bit di parità**.

ARQ consiste nel modellare lo scambio di dati tra due attori nel seguente modo:

* il trasmettitore invia la PDU composta da PCI + SDU;
* il ricevitore, dopo aver ricevuto la PDU, trasmette al trasmettitore una PCI, un pacchetto di controllo [**ACK**], che contiene:
  * bit di ridondanza;
  * N(t), numero di sequenza;
  * indirizzi.

#### Stop and Wait

Il primo – nonché più semplice – protocollo di ARQ è lo Stop&Wait. Esso consiste nelle seguenti regole:

* il **trasmettitore** ha il compito di:
  * fare una copia della PDU per possibile ritrasmissione;
  * inviare la PDU;
  * attivare un orologio (tempo di *timeout*);
  * porsi in attesa della conferma di ricezione (acknowledgment — **ACK**):
    * conntrollarne la correttezza;
    * controllarne il numero di sequenza;
    * se l'ACK è corretto e relativo all'ultima PDU inviata, inviare la prossima PDU;
    * se l'ACK non è in sequenza, viene ignorato;
  * se cade il timeout prima dell'arrivo della conferma, ripetere la trasmissione della PDU riattivando il timeout;
* Il **ricevitore** ha il compito di:
  * verificare la correttezza della PDU ricevuta;
  * controllare il numero di sequenza;
  * se la PDU è corretta invia la conferma di ricezione;
  * se la PDU è quella attesa essa viene consegnata ai libelli superiori.

Ma la numerazione serve? E quanti bit servono per numerare i pacchetti?

**Alternating Bit Protocol (Stop and Wait)**
Un primo modo di approcciare il problema è di utilizzare un unico bit per numerare i pacchetti. Il primo pacchetto sarà `0`, il secondo `1`, il terzo `0`, e così via.

Notare come fino ad pra si stia suponendo un architettura del tipo:

<div align="center">
    <img src="src/i7.svg" />
</div>

Ma sorgono problemi nel momento in cui il protocollo finestra viene utilizzato a un livello superiore al primo con architetture generiche del tipo:

<div align="center">
    <img src="src/i8.svg" />
</div>

ovvero su canali non sequenziali.

Ipotiziamo il seguente scenario
> NON REVISIONATO, POTREBBE CONTENERE ERRORI

|Tempo|Ciclo|V(T)|Operazione|V(R)|Stato|Note
|:-:|:-:|:-:|:-|:-:|:-:|:-
|0|1|0|TX PDU #1|0|In corso
|1|1|0|RX PDU #1|1|Eseguita
|2|1|0|TX ACK #1|1|In corso
|3|1|0|TIMEOUT|1|Ok
|4|1|0|TX PDU #1|1|In corso
|5|1|0|RX PDU #1|1|Eseguita|Scarto
|6|1|0|TX ACK #1|1|In corso
|7|1|1|RX ACK #1|1|Eseguita
|8|2|1|TX PDU #2|1|In corso
|9|2|1|RX PDU #2|0|Eseguita
|10|2|1|TX ACK #2|0|In corso
|11|2|0|RX ACK #2|0|Eseguita
|12|3|0|TX PDU #3|0|In corso
|13|3|1|RX ACK #1|0|Eseguita|<span style="color: red">Fallisce</span>
|14|3|1|TX PDU #4|0|In corso|<span style="color: red">Errore</span>
|15|3|1|RX PDU #4|0|Eseguita|<span style="color: red">Scartata</span>
|16|3|1|TX ACK #3 N(R) = 0|0|In corso
|17|3|0|RX ACK #3 N(R) = 0|0|Eseguita|<span style="color: red">Interpretata come successo</span>
|18|\*|\*|<span style="color: red">TX/RX ERR</span>|\*|\*|<span style="color: red">Interpretata come successo</span>
|19|\*|\*|<span style="color: red">TX/RX ERR</span>|\*|\*|<span style="color: red">Interpretata come successo</span>
|20|\*|\*|<span style="color: red">TX/RX ERR</span>|\*|\*|<span style="color: red">Interpretata come successo</span>
|21|\*|\*|<span style="color: red">TX/RX ERR</span>|\*|\*|<span style="color: red">Interpretata come successo</span>
|22|\*|\*|<span style="color: red">TX/RX ERR</span>|\*|\*|<span style="color: red">Interpretata come successo</span>

Il bit singolo, alternato, non è necessariamente la scelta migliore. Malgrado questo problema, resta comunque adatto per trasmissioni a livello fisico in cui il calale è sequenziale (ovvero tutti i casi in cui il canale sia costituito dal solo mezzo trasmissivo).

Il problema come si può capire è docuto alla numerazione, non al protocollo in sé. Dunque vediamo se può funzionare una numerazione in modulo 4, ovvero con due bit utilizzati per la sequenza.

**Stop&Wait con sequenza in modulo 4**
Riproponiamo lo stesso scenario:
|Tempo|Ciclo|V(T)|Operazione|V(R)|Stato|Note
|:-:|:-:|:-:|:-|:-:|:-:|:-
|0|1|0|TX PDU #1|0|In corso
|1|1|0|RX PDU #1|1|Eseguita
|2|1|0|TX ACK #1|1|In corso
|3|1|0|TIMEOUT|1|Ok
|4|1|0|TX PDU #1|1|In corso
|5|1|0|RX PDU #1|1|Eseguita|Scarto
|6|1|0|TX ACK #1|1|In corso
|7|1|1|RX ACK #1|1|Eseguita
|8|2|1|TX PDU #2|1|In corso
|9|2|1|RX PDU #2|2|Eseguita
|10|2|1|TX ACK #2|2|In corso
|11|2|2|RX ACK #2|2|Eseguita
|12|3|2|TX PDU #3|2|In corso
|13|3|2|RX PDU #3|3|Eseguita
|14|3|2|TX ACK #3|3|In corso
|15|3|3|RX ACK #3|3|Eseguita
|16|4|3|TX PDU #4|3|In corso
|17|4|3|RX PDU #4|0|Eseguita
|17|4|3|TX ACK #4|0|In corso
|18|4|0|RX ACK #4|0|Eseguita
|19|5|0|TX PDU #5|0|In corso|<span style="color: red">Fallisce</span>
|20|5|1|RX ACK #1 N(R) = 1|0|Eseguita|<span style="color: red">Errore</span>
|21|6|1|TX PDU #6|0|In corso|In corso
|22|6|1|RX PDU #6|0|Eseguita|<span style="color: green">Scartata, Invio ACK #0</span>
|23|6|1|TX ACK #0|0|In corso
|24|6|1|RX ACK #0|0|In corso|<span style="color: green">ACK fuori sequenza</span>
|25|6|1|TIMEOUT|0|Ok
|26|6|1|TX PDU #6|0|In corso|In corso
|27|6|1|RX PDU #6|0|Eseguita|<span style="color: green">Scartata, Invio ACK #0</span>
|28|6|1|TX ACK #0|0|In corso
|29|6|1|RX ACK #0|0|In corso|<span style="color: green">ACK fuori sequenza</span>

Sicuramente il malfunzionamento è meno probabile rispetto al caso del single bit. In particolare, è meno probabile perché perdere l'*ACK N(R) = n* e la *PDU N(T) = n - 1* è un evento con una certa probabilità se i *N(x) ∈ [0, 1]*, ma un aprobabilità minore se *N(x) ∈ [0, 3]*. Se infatti l'ACK fuori sequenza con *N(R) = n* arriva dopo l'invio della PDU con *N(T) = n - 1*, verrebbe trattata semplicemente come una semplice ACK fuori sequenza.

Si può porre rimedio al problema in due modi:

* il primo è rendere improbabile l'avvenimento (i.e. aumentare i bit di sequenza);
* il secondo è introdurre un tempo di vita per le ACK:
  * inserendo un TTL (numero di nodi attraversati);
  * inserendo un timestamp (se ho inviato una PDU al timestamp 10399824, non può tornare indietro un ACK con timestamp minore, come 10397775).

#### Go Back N / 1

> Una rivoluzione, se Stop&Wait viè sembrato complicato, preparatevi.

**L'idea**
Trasferire più PDU e solo dopo averne trasferite *N* fermarsi ad aspettare gli ACK.

#### La finestra

*def.* Si definisce **finestra di trasmissione** il numero massimo di paccheti trasferibili su un canale senza aspettare un riscotro (ACK). La finistra si indica con $W_T$.

Per ragioni evidenti la finestra è limitata dalla quantità di memoria allocata in trasmissione.

*def.* Si definisce **finestra di ricezione** la sequenza di PDU che il ricevitore è disposto ad accettare e memorizzare. La finistra si indica con $W_R$.

Per ragioni sempre evidenti la finestra è limitata dalla quantità di memoria allocata in ricezione.

#### Go Back N / 2

> I comportamenti

Il trasmetticore con finestra $W_T > 1$:

* invia fino a $N = W_T$ PDU, facendo di ognuna una copia;
* attiva un solo orologio per le N PDU (che viene resettato ad ogni invio di PDU);
* per ogni ACK in sequenza ricevuto, fa scorrere in avanti la finestra di tanti pacchetti quanti sono i pacchetti confermati;
* se scade il timeout prima della conferma di ricezione relativa alla PDU che ha impostato il timeout, ripete la trasmissione di tutte le PDU non ancora confermate.

Il ricevitore, invece, ha un funzionamento molto simile al ricevitore per Stop&Wait:

* controlla la correttezza della PDU;
* controlla il numero di sequenza;
* se la PDU è corretta invia la conferma di ricezione;
* se la PDU contiene il primo nomero di sequenza non ancora ricevuto, viene consegnata ai livelli superiori.

* * *
**Coffee Break**
> O Tips&Triks

L'acknowledgement *n* ha lo scopo di confermare che il pacchetto numerato *n* è stato ricevuto con successo. Questo, implicitamente, significa che sono stati ricevuti anche tutti i pacchetti da *n - 1* a *0*. Dunque, se a causa di un canale non sequeziale, fosse arrivato prima l'*ACK N* dell'*ACK N - 1*, il trasmettitore interpreterebbe implicitamente che tutti i pacchetti precedenti (e compreso) *N* sono stati ricevuti con successo. Quando poi arriveranno gli *ACK N -1*, *ACK N - 2*, *ACK N - 3*, ecc., il trasmettitore, semplicemente, li ignorerà.

> Dare poi uno sguardo alla L12@30:00${}^+$
* * *

#### Semantica degli ACK

Un ACK può voler dire diverse cose... dobbiamo metterci d'accordo. Gli ACK possono essere:

* **cumulativi**: l'ACK numerato *N* informa il destinatario (dell'ACK) che i paccheti da *0* a *N* (escluso) sono stati ricevuti;
* **selettivi**: l'ACK numerato *N* informa il destinatario (dell'ACK) cje il pacchetto numerato *N* è stato ricevuto.
* **ACK Negativo** [**NAK**]: il NAK numwerato *N* informa il tramettitore che il pacchetto *N* non è stato ricevuto (oè stato ricevuto corrotto).

Non c'è un modo *buono* e uno *cattivo*, solo modi diversi gestire la trasmissione.

#### Piggybacking

Nel caso di flussi di informazione bidirezionaliu, è sovente possibile scrivere l'informazione di riscontro (ACK) nell'intestazione delle PDU di informazione che viaggiano nella direzione opposta.

Questo meccanismo consente di riparmiare pacchetti di ACK.

#### Posizioni relative e corrette tra $W_T$ e $W_R$

Se la trasmisione procede come dovrebbe, $W_T$ e $W_R$ posson trovarsi solo nelle segunti posizioni reciproche (<span style="color: green">verdi</span>), altre danno luogo a malfunzionamenti (<span style="color: red">rosse</span>).

|9|10|11|12|13|14|15|16|17|
|:-|:-|:-|:-|:-|:-|:-|:-|:-|
|||$W_T$|$W_T$|$W_T$|$W_T$|$W_T$|||||
|<span style="color: red">$W_R$</span>||<span style="color: green">$W_R$</span>
||<span style="color: red">$W_R$</span>||<span style="color: green">$W_R$</span>
|||||<span style="color: green">$W_R$</span>||||<span style="color: red">$W_R$</span>
||||||<span style="color: green">$W_R$</span>
|||||||<span style="color: green">$W_R$</span>
||||||||<span style="color: green">$W_R$</span>

**Attenzione alla numerazione!!!**
Non è possibile usare una $W_T$ che coincida con l'intero intrvallo di numerazione. Immaginiamo uno scenario semplice: in una finestra di trasmissione vengono inviati tutti i pacchetti possibili – per k it di numerazione, da *0* a *2<sup>k</sup> - 1*) – e ogni pacchetto viene perso. Il ricevitore invierà ACK 0 e il trasmettitore lo interpreterà come se i pacchetti fossero arrivati tutti.

Dunque in generale, la finestra di trasmissione e lo spazio di numerazione sono legati dalla relazione $W_T < 2^k$.

#### Go Back N / 3

Il trasmettitore è significativamente più complesso rispetto al caso dello Stop&Wait per:

* quantità di memoria;
* gestione dell'orologio;
* algoritmi.

Il ricevitore resta invece inalterato anche grazi alla possibilità di utilizzare conferme multiple (su gruppi di PDU) grazie alla semantica cumulativa e al meccanismo dell'orologio al ricevitore (il ricevitore potrebbe decidere di utilizzare un orologio pr non inviare un ACK ad ogni pacchetto ma ogni *N*).

La finestra di trasmizzione nun può avere dimensioni arbitrarie.

#### Selective Repeat

L'idea alla base è che implementando il protocollo *Go Back N* il ricevitore può accettare solo PDU in sequenza. Accettare PDU fuori corrette fuori sequenza può tuttavia migliorare le prestazioni di trasmissione; il protocollo **Selective Repeat** usa finestra di trasmissione e finestra di ricezione di dimensioni $> 1$ e può essere implementato in diversi moti che si differenziano per:

* l'uso di ACK selettivi o cumulativi;
* l'uso di timer associati alle singole PDU o alla finestra;
* comportamenti del trasmettitore e del ricevitore.

**I comportamenti**
Del trasmettitore:

* invia fino a $N = W_T$ PDU facendo di ognuna una copia;
* attiva un solo orologio per le $N$ PDU;
* si pone in attesa degli ACK;
* per ogni ACK in sequenza ricevuto, fa scorrere in avanti la finestra di tanti panti pacchetti quanti sono stati confermati;
* se scade il timeout prima della conferma di ricezione relativa alla PDU che ha settato il timeout, ripete la trasmissione di tutte le PDU non ancora confermate.

Nulla di nuovo ripetto a *Go Back N*.

Del ricevitore:

* riceve una PDU;
* ne contorlla la correttezza;
* controlla il numero di sequenza;
  * se la PDU è corretta ed in sequenza la consegna al livello superiore (eventualmente insieme ad altre ricevute in sequenza);
  * se la PDU è corretta ma non in sequenza:
    * se è entro la finestra di ricezione la memorizza;
    * se è fuori la scarta;
* invia comunque un ACK relativo all'ultima PDU inricevuta in sequenza.

Anche per selective repeat vale un relazione tra le dimensioni delle finestre e il numero di bit *k* di sequenza dei pacchetti: $W_T + W_R ≤ 2^k$.

#### Efficienza del protocollo

*def.* Si dice **efficienza di un protocollo** o **di una trasmissione** in un intervallo di tempo, il rapporto trail tempo in cui (nell'intervallo) il trasmettitore è stato effettivamente impegnato a trasmettere e la durata dell'intervallo.

* **Efficienza Stop&Wait** ideale e senza errori:
$$
  \eta = \dfrac{T_{TX}}{T_{TX} + 2t_p} =
  \dfrac{1}{1 + 2a}.
$$

* **Efficienza Go Back N** ideale e senza errori:
$$
  \eta = \begin{cases}
    1,\\
    \dfrac{W_T}{1 + 2a},
  \end{cases} \begin{matrix}
    con \; W_T \geq 1 + 2a \\
    con \; W_T < 1 + 2a
  \end{matrix}.
$$

#### Throughput su canale

$$
  \theta = \eta C \propto \dfrac {W_T}{a}
$$
