# Before Application

An application-leve protocol must refer to to a transport-layer protocol. Which one to choose?

We might need:

* Data integrity:
  * some apps reqire reliable data transfer;
  * other apps don't need a 100% reliability, but they can tollerate a certain ldata loss.
* Timing:
  * low delay;
  * long dalay.
* Throughput:
  * some apps require a minimum throughput to work well;
  * other apps don't.
* Security:
  * does your app need encryption? Data Integrity?
  * or not?

<table>
<tr> <td>Protocol</td> <td>Data Integrity</td> <td>Timing</td> <td>Throughtput</td> <td>Security</td> </tr>
<tr> <td>TCP</td> <td>X</td> <td>X (it depends)</td> <td>UDP</td> <td>X</td> </tr>
<tr> <td>UDP</td> <td></td> <td>X (it depends)</td> <td>UDP</td> <td>Usually not</td> </tr>
</table>
