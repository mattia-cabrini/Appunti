# NAT: Network Address Traslation

Some address ranges are reservd for private use. The idea is to enable private users to use the same addresses to set up their private networks.

* **Class A**: 10.0.0.0/8
* **Class B**: 172.{16|...|31}.0.0/16;
* **Class C**: 192.168.*.0/24.

NAT protocol allow users to do this:

* set up a private network using private IP address ranges;
* set up a gateway having a public IP;
  * configure the gateway to manage {in | out}coming traffic for multiple devices uning that single public IP address.

<div align="center">
    <img src="src/i18.svg" width="430px" />
</div>

## How?

As you can see in the picture, it is not a big deal to transmit data to a public server. The problem is for the server to reply.

The solution is to let the router to change the IP address of level three packets and then when the server replies, to use a conversion table to forward the packet replied to the right host.

<div align="center">
    <img src="src/i19.svg" width="430px" />
</div>
