# Lo strato fisico

## I mezzi fisici trasmissivi

I tipi di mezzi trasmissivi, oggi, sono di tre tipi: elettrici (doppino, cavo coassiale, ...), ottici (fibra ottica) e radio (fenomeni magnetici). Il mezzo trasmissivo — cosiddetto — ottimale è caratterizzato da:

* resistenza, capacità parassite e impedenze basse;
* restenza alla trazione;
* flessibilità;
* facilità d collegamento di ricetrasmettitori.

### I mezzi elettrici

I mezzi elettrici sono caratterizzati da basse resistenze, capacità parassite e impedenze, nonché da una buona resisteza alla trazione. Essi sini flessibili (lo sappiamo anche dalla vita di ogni giorno) e sono molto facili da collegare ai ricetrasmettitori.

Le caratteristiche di questi mezzi sono date dalla **geometria** con cui essi sono costruiti, dal **numero di conduttori** e dalla distanza reciproca che essi hanno. Anche il **tipo di isolante** e il **tipo di schermatira** hanno influenza sulle caratteristiche del mezzo.

La **schermatura**, in particolare, è una sorta di gabbia di Faraday che, posta attorno al mezzo trasmissivo, evita che fenomeni elettromagnatici esterni possano alterare le caratteristiche elettriche del mezzo.

#### Il doppino

Il doppino è uno dei cavi più utilizzati nella telefonia. Esso è composto da due fili di rame ritorti (binati, twisted) per ridurre le interferenze elettromagnetiche usando tecniche trasmissive differenziali.

La **tecnica di trasmizzione differenziale** consiste nell'intendere l'informazione non come il valore della tensione letto su un filo piuttosto che un altro, bensì come differenza delle tue tensioni:
$$
    V = V_2 - V_1.
$$

L'idea alla base di questo metodo è che un eventuale fenomeno eletromagnetico andrebbe ad alterare entrambe le tensioni trasformandole come:
$$
    V_1 \longmapsto V^\prime_1 = V_1 + e;
$$
$$
    V_2 \longmapsto V^\prime_2 = V_2 + e.
$$

A questo modo $V$ resta sempre invariata:
$$
    V = V^\prime_2 + e - V^\prime_1 - e = V_2 - V_1
$$

A tale scopo è importante che la binatura sia stata eseguita in modo corretto. Il fatto che entrambe le tensioni siano alterate di un fattore $e$ è dato dall'esposizione di entrambi i fili al fenomeno elettromagnetico interferente. Se i fili non fossero binati, uno la tensione di uno verrebbe alterata più della tensione dell'altro. Se la binatura è ottimale, tagliando il doppino in un punto qualsiasi, la distanza tra i conduttori dovrebbe essere sempre $\Delta x$.

Il doppino non è usato solo in telefonia: i cavi che utiliziamo per connettere i computer a internet (comunemente chiamati cavi **Ethernet**) è un cavo composto da quattro doppini collegati a due connettori (RJ45).

#### Il cavo coassiale

Il cavo coassiale era molto in voga negli anni '60. Oggi è desueto per le comunicazioni in laboratorio o a lunga distanza. Il suo utilizzo è oggi limitato alla comunicazione tra un antenna che riceve un segnale EM e il dispositivo che esegue la demodulazione di tale segnale (parleremo di modulazione e demdulazione tra poco).

Questo tipo di cavo è composto da un unico conduttore centrale, avvolto in un meteriale isolante, a sua volta avvolto in una calcìza di metallo che funge da schermatura e poi da una guaina di materiale plastico (gomma).

Questo tipo di cavo ha una maggiore schermatura da fenomeni elettromagnetici esterni, ma anche costi di produzione elevati e maggiore difficoltà di installazione. La velocità trasmissiva si aggita intorno alle centinaia di Mbps.

#### Codifica unipolare

Uno dei metodi che si utilizzano per inviare dati sul mezzo elettrico è la codifica unipolare, in cui una tensione positiva (per esempio $3V$) rappresenta un simbolo ($1$) e il riferimento di terra il simbolo complementare ($0$). Il problema fondamentale è che se i clock non sono esattamente allineati, si potrebbero siscontrare problemi di tx/rx. In generale per trasmettere un siimbolo si porta la tensione al valore di riferimento per un certo tempo prestabilito (temo di simbolo).

#### Codifiche polari

Si utilizzano due livelli di tensione con polarità diverse (riduzione della componente continua). Tre varianti:

* **NRZ** (Non-Return-to-Zero): non c'è transizione su tensione nulla al passaggio di due bit consecutivi;
* **RZ** (Return-Zero): transizione su tensione nulla tra due bit consecutivi [bitrate elevato];
* **Bifase** (codifica Manchester): ogni bit è rappresentato da due livelli di tensione differenti [bitrate elevato].

#### Codifiche bipolare

Chiamate anche *AMI* (Alternate Mark Inversion) o 8B6I (otto bit con sei inversioni), permettono l'utilizzo di simboli ternari ($-1$, $0$, $+1$). Avendo a disposizione tre simboli, è possibile inviare otto bit con solo sei simboli. Questo significa ridurre il bitrate ed evitare la condizione in cui si inviano $n$ bit uguali consecutivi.

#### Codifiche nBmB

Simboli di $n$ bit sono rappresentati da simboli di $m$ bit, con $n < m$. Sono molto popolari perché richiedono meno banta, permettono il controllo sulla scelta delle partole di codice limitando quelle con troppi $0$ o $1$ consecutivi, limitano la componente continua e forniscono caratteri speciali per delimitare i pacchetti, per la trasmissione idle e per padding.

### Il mezzo ottico

**Il** perché ne esiste solo uno: la **fibra ottica**. Si tratta di un sottile filamento di vetro flessibile costituito da due parti (il *core* e il *cladding*) con indici di rifrazione diversi. Grazie ai diversi indici di rifrazione e con il filamento costruito in modo opportuno, per via della legge di Snell, il raggio luminoso introdotto nella fibra entro un certo *angolo di accettazione* rimane confinato nel core, venendo riflesso dal cladding.

Il vantaggio principale della fibra ottica è la totale immunità da disturbi elettromagnetici, nonché la capacità trasmissiva nell'ordine di decine di Tbps, la bassa attenuazione di ~$0,1\dfrac{dB}{km}$ (anche se dipende dalla lunghezza d'onda $\lambda$) e i costi contenuti.

Annotiamo però anche degli svantaggi:

* adatte solo a collegamenti punto-punto;
* difficili da collegare sia tra loro, sia con connettori;
* ridotto raggio di curvatura (dovuto alla legge di Snell);
* soffre le vibrazioni.

Non tutte le onde luminose possono essere utilizzate per la trasmissione via vibra ottica. In particolare si identificano tre finestre di lavoro centrate attorno a lunghezze d'onda di $0,8 \mu m$, $1,3 \mu m$ e $1,55 \mu m$.

Si ricorda la relazione tra *frequenza* del fenomeno EM e *lunghezza d'onda*:
$$
    f = \dfrac{c}{\lambda}.
$$

I cavi in fibra ottica, come quelli di rame, si è detto che soffrono dell'attenuazione del segnale. Per questo motivo anche questi cavi, se posati per diversi $km$ richiedono amplificatori ottici ogni $30—50km$.

### Il canale radio

Si tratta del canale che supporta la propagazion di segnali mediante l'uso di antenne. Se almeno uno tra i TX e gli RX è in movimento, si parla di canale **radiomobile**.

#### Modulazioni digitali

Essendo il campo elettromagetico definito attraverso funzioni periodiche (sinusoidi in fase), non si possono applicare le codifiche utilizzate in linea. Tutte le informazioni vengono trasmesse su un segnale detto **portante**. Il segnale portante è una sinusoide che è definita da una frequenza, un'ampietta e una fase. Si può inviare questo segnale modificando una delle tre caratteristiche per trasmettere un segnale "informativo" *sulla portante*.

Modificare l'ampiezza della sinusoide per trasmettere dati significa modulare l'ampiezza della portante (AM, Amplitude Modulation). Modificarne la frequenza significa modulare in frequenza (FM, Frequency Modulation). È possibile anche modificare la fase della portante (PM, Phase Modulation).

AM, FM, PM sono però termini utilizzati nelle trasmissioni analogiche. Per le trasmissioni digitali la terminologia corretta è ASK, FSK, PSK (Amplitude | Frequency | Pase Shift Keying) e QAM (Quadrature Amplitude Modulation).

Nel singolo tempo di simbolo, è possibile trasmettere più di un simbolo. Tuttavia ci sono dei limiti dovuti ai fenomeni di decadenza, doppler ed echi.

Una modulazione **8PSK**, per esempio, è una modulazione che distingue otto possibili simboli; una **64-QAM** è unamodulazione di fase/ampiezza a sessantaquattro simboli (sei bit per simbolo).
