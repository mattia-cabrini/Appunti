# SIP

> Long-Term Vision

* All telephone calls, video conference calls take place over internet;
* People identified by names or a-mail addresses, rather than bt phone numbers;
* Can reach callee, no matter where callee roams, no matter what IP device callee is currently using.

Requirements:

* Call set-up:
  * For caller to let callee know she wants to establish a call;
  * So caller, callee can agree on media encoding;
  * To end call.
* Determine current IP address of callee:
  * Maps   mnemonic identifier to current IP address.
* Call Management:
  * Add new media streams during call;
  * Change encoding during call;
  * Invite others;
  * Transfer, hold cards.

## SIP Message Example

> Source: <https://datatracker.ietf.org/doc/html/rfc3261#section-16.12.1>

```sip
INVITE sip:callee@u2.domain.com SIP/2.0
Contact: sip:caller@u1.example.com
Record-Route: <sip:p2.domain.com;lr>
Record-Route: <sip:p1.example.com;lr>
```

```sip
SIP/2.0 200 OK
Contact: sip:callee@u2.domain.com
Record-Route: <sip:p2.domain.com;lr>
Record-Route: <sip:p1.example.com;lr>
```

## Entities

### Registrar

*def.* A server that offer the service of registration for users.

## Network support for multimedia

<table>
    <tr>
        <td>Approach</td>
        <td>Granularity</td>
        <td>Guarantee</td>
        <td>Mechanisms</td>
        <td>Complex</td>
        <td>Deployed</td>
    </tr>
    <tr>
        <td>Making best of best effort service</td>
        <td>All traffic treate equally</td>
        <td>None or soft</td>
        <td>No network support (all at application)</td>
        <td>low</td>
        <td>everywhere</td>
    </tr>
    <tr>
        <td>Differenciated service</td>
        <td>Traffic "class"</td>
        <td>None of soft</td>
        <td>Packet market, schedulig, policing</td>
        <td>med</td>
        <td>some</td>
    </tr>
    <tr>
        <td>Per-connection QoS</td>
        <td>Per-connection flow</td>
        <td>Soft or hard after flow admitted</td>
        <td>Packet market, scheduling, policing, call admission</td>
        <td>high</td>
        <td>little to none</td>
    </tr>
</table>
