# DNS

> Domain Name System

## What is it?

It is a protocol (as well as an infrastructure) which allow us to not use IP Addresses directly but to use name.

The idea is to have a distributed database implemented in hierarchy of many servers. This database maps a domain of name to a domain of IP Addresses.

<div align="center">
    <img src="src/i13.svg" />
</div>

Server must be always up to date; Fortunatly this is not a big deal, since informations don't change often.

## Local DNS Server

It does not belong to hierarchy, it is implemented by ISPs. Every time a host makes a request, the local DNS server asks the hierarchy to furnish the IP adrress. This IP is then written in a DNS cache in order to not disturb the hierarchy when a second host asks for the same name.

## RFC 2136

A local DNS server and an authorative DNS server send messages to each other in order to keep cache lines update.

> Never implemented.
> The cache just expire after a certaint time.

## DNS Record

### Resource Record

* Name;
* Value;
* Type:
  * A: hostname — IP Address;
  * AAAA: hostname — IPv6 Address;
  * NS: domain — hostname;
  * CNAME: alias — real name;
  * MX: mailserver associated with name;
* TTL: **T**ime **T**o **L**ive in a DNS cache.

### PTR

As you can ask for an IP address by a hostname or alias or service, you can ask for the hostname associated to a certain IP address.

### DNS Queries and Replies

Flags:

* query or reply;
* recursion desired;
* recursion available;
* reply is authoritative: the reply came from the authoritative DNS server.
