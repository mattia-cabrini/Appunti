# Electronic Mail

Three major componets:

* user agents;
* mail servers;
* a protocol: SMTP.

## Scenario

> Alice sends a message to Bob

1. Alice uses a UA to compose a message to `bob@someschool.edu`;
2. Alice's UA sends message to her mail server; message placed in queue;
3. Client side of SNMP opens TCP connection with Bob's mail server;
4. SMTP client sends Alice's message over the TCP connection;
5. Bob's mail server places the message in Bob's mailbox;
6. Bob invokes his user agent to read message.

## SMTP Example

> Source: <https://it.wikipedia.org/wiki/Simple_Mail_Transfer_Protocol#Esempio_di_comunicazione_SMTP>

```smtp
S: 220 smtp.example.com ESMTP Postfix
C: HELO relay.example.com
S: 250 smtp.example.com
C: MAIL FROM: <bob@example.com>
S: 250 OK
C: RCPT TO: <alice@example.com>
S: 250 OK
C: DATA
S: 354 End data with <CR><LF>.<CR><LF>
C: From: "Bob" <bob@example.com>
C: To: "Alice" <alice@example.com>
C: Date: Tue, 15 January 2008 16:02:43 -0500
C: Subject: Messaggio di prova
C: 
C: Ciao, questo è un messaggio di prova
C: .
S: 250 Ok: queued as 12345
C: QUIT
S: 221 Bye
```

## Multimedia Extension

To add multimedia objects in a mail we introduce the MIME standard (RFC 2045, 2056). In the header additionl line are written to instruct the server a multimedia object is to be received.

## Mail Access

We have described how a e-mail messsage is delivered. Now we want to access our e-mail.

* POP[3]: authorization, download;
* IMAP: more sophisticated, more features, including multi-client mailbox sync;
* HTTP: web-based mail clients

### POP[3]

* Authorization (access).
* List email messages (their UID (or ID) and their size).
* Retrieve a message.
* Delete a message from the server.
* Quit: close connection.

### IMAP

* Keeps all messages on the server.
* Allows user to organize messages in folders.
* Keep user state across sessions (folders names and mapping IDs-folders).
