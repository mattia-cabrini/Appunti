# Servizi di telecomunicazione

*def.* Ciò che viene offerto da un gestore pubblico o privato ai propri clienti al fine di soddisfare una specifica esigenza di telecomunicazione. Mettiamo ordine...

Rispetto ai servizi offerti, le reti di TLC possono essere:

* **dedicate** (unico servizio): radio o televisione;
* **integrate** (moltitudine di servizi): accesso a internet.

Distinguiamo poi:

* **servizi portanti**: trasmissione di segnali tra interfacce utente (e.g. collegamento ADSL, circuito direto numerico punto-punto);
* **teleservizi** o **over-the-top**: fornisce la completa possibilità di comunicare tra utenti, includedo le funzioni degli apparati di utente, secondo protocollo concordati da gestori pubblici o privati (telefonia, fax, web browsing) e possono essere *servizi di base* o *servizi supplementari*.

Distinzione dei servizi in base al flusso di informazioni:

* **bidirezionale simmetrico**: telefono, chat;
* **bidirezionale asimmetrico**: Netflix, Youtube, Instagram, Facebook, Twitch, ecc;
* **unidirezionale**: televisione, radio.

Oppure:

* **punto-punto**: telefonia;
* **punto-multipunto**: radio, televisione, AP;
* **multipunto-multipunto**: reti di sensori.

Classificazione di servizi over-the-top.

* Interattivi:
  * **conversazionali**: forum;
  * **messaggistica**: WhatsUp, SMS;
  * **consultazione/reperimento**: Wikipedia, DNS Resolver.
* Diffusivi:
  * **senza controllo di presentazione dell'utente**: si tratta di un servizio diffusivo in cui l'utente può scegliare una *banda laterale* del canale per modificare la fruizione del servizio (e.g. rivedere un goal su Sky premendo il tasto verde del telecomando);
  * **con controllo di presentazione dell'utente**: si tratta di un servizio diffusivo in cui l'utente non può scegliere di modificare la fruizione del servizio.
* Modelli:
  * **client-server**: modello tale per cui un server offre un servizio e un client ne usufruisce:
    * il server non conosce i client e li serve solo nel momento in cui un client lo interroga;
    * il client conosce l'identità del server e inizia il collegamento con esso quando gli serve usufruire di un servizio;
    * è un modello esemplare dei flussi di informazione bidirezionali - il server invia al client molte più informazioni di quelle che il client invia al server;
  * **peer-to-peer**: cade la distinzione di ruoli tra client e server. Diveri computer si scambiano informazioni in modo paritetico. La telefonia è un esempio storico di modello peer-to-peer.

* * *

## Piccola parentesi

Come si misurano le **distanze** nelle reti? Non si musurano in metri o chilometri, ma in **hop**, «salti», ovvero la distanza tra un dispositivo e un altro è data dal numero di nodi posti fra i due dispositivi lungo il canale.
* * *
