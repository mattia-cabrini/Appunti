# Trasmissione

*madare attraverso*
*Trasmettere* un'informazione significa alterare lo stato di un mezzo trasmissivo da una trasmittente in modo tale che una ricevente possa “leggere“ l'alterazione del mezzo e “ricostruire“ l'informazione inviata dalla trasmittente.

Le informazioni inviate possono essee di tipi diversi:

* **analogiche**: l'informazione è trasferita per mezo di un segnale elettrico:
  * continuo;
  * limitato;
  * di infiniti valori possibili.
* **numerica**: l'informazione è trasferita per mezzo di un segnale elettrico:
  * discontinuo;
  * limitato;
  * con un numero finito di valori possibili.

Spesso è necessario trasformare le informazioni analogiche in informazioni digitali per poterle inviare attraverso i mezzi trasmissivi. Il processo di trasformazione si chiama *numerizzazione*.

Citiamo brevemente i concetti di trasmissione *seriale* e *parallela*:

* **seriale**: i bit vengono inviati su un unico bus trasmissivo, uno dopo l'altro;
* **parallela**: i bit di una stessa parola vengono trasmessi in parallelo su diversi bus trasmissivi (modalità desueta).

Per entrambi il problema è sincronizzare la trasmissione: quando ha inizio la trasmissione? Vedremo sistemi per evitare l'utilizzo di bus trasmissivi aggiuntivi.

## Multiplazione e accesso multiplo

Come si condivide un canale tra più nodi?

Il problema di **multiplazione** è quello in cui tutti i flussi sono disponibili in un unico punto; il problema di **accesso multiplo** non è molto diverso, ma è complicato dal fatto che i flussi sono disponibili in ogni punto.

Per eseguire queste funzioni si possono utilizzare le seguenti modalità.

* **Frequenza** (FDM - FDMA)
Se il canale utilizzabile in frequenze diverse, il problema è risolto (e.g. servizio radio). Ogni emittente trasmette su frequenze diverse, concordate con il ricevente. Si divide dunque il canale trasmissivo in bande di frequenza (e in bande di guardia) e ogni emittente si impegna a trasmettere su un'unica banda.
* **Tempo** (TDM - TDMA)
Tutti gli utenti trasmettono sullo stesso canale ma in tempi diversi. Nella radio AM, per esempio, l'emittente radio trasmette sulla stessa banda degli altri una elevatissima quantità di bit al secondo, ma per un periodo di tempo limitato.

> i primi due approcci possono essere misti

* **Codice** (CDM - CDMA) [2.5G, Edge, 4G, LTE, 5G]
Si utilizza un “linguaggio diverso“ in modo tale che il ricevente sia in grado di discriminare una lingua da un'altra.
* **Spazio**
Una multiplazione ovvia. Se due interlocutori sono posti in punti sufficientemente distanti l'uno dalll'altra, essi possono usare la stessa porzione di dominio di frequenza/tempo/codice.

Esiste poi una multiplazione statistica. Poniamo caso che un dominio sia suddivisibile in N bande e che su questo dominio debbano trasmette 2N utenti. E' possibile permettere agli N uteni di trasmettere? La risposta è sì se gli utenti sono intermittenti e sono prevedibili [o noti] i tempi in cui essi trasmetteranno.

La chiamiamo “multiplazione statistica“ perché mi permette di tenere in conto delle variazioni di traffico da parte di emittenti intermittenti, ed è - di fatto - una scommessa.

### Condivisione di un nodo

Distinguiamo tra commutazione di “circuito“ e di “pacchetto“:

* **commutazione di circuito**: se i flussi sono continui;
* **commutazione di pacchetto**: se i flussi sono intermittenti.

## Commutazione di circuito

*def.* Si definisce **commutazione di circuito** il processo di interconnessione di unità funzionali, canali di trasmissione o circuiti di telecomunicazione per il tempo necessario al trasferimento di segnali.

In altre parole è un processo di allocazione di risorse (cavi, elementi intermedio come e.g. gli amplificatori di segnale). Il circuito così creato è di uso dei due attori che stanno comunicando finché - da protocolo - uno dei due non chiude la comunicazione, rilasciando le risorse. Il circuito ha la caratteristica di essere continuo, ovveor avviene anche se la comunicazione in termini logici non è in atto: quando e.g. un utente è altelefono ma non parla.

Per capire: due circuiti diversi possono utilizzare lo stesso canale in multiplazione.
* * *
           ┍—————————————┑          ┍——————————————┑          ┍—————————————┑
    ———————│             │——————————│   Rete di    │——————————│             │—————>
    ———————│ Interfaccia │——————————│ connessione  │——————————│ Interfaccia │—————>
    ———————│     di      │——————————│      o       │——————————│     di      │—————>
    ———————│  ingresso   │——————————│  Matrice di  │——————————│   uscita    │—————>
    ———————│             │——————————│ commutazione │——————————│             │—————>
           ┕—————————————┙          ┕——————ˆ———————┙          ┕——————ˆ——————┙
                  │                        │                         │
                  │                        │                         │
                  │                        │                         │
                  │                 ┍——————┙———————┑                 │
                  │                 │   Sistema    │                 │
                  ┕————————————————>│     di       │—————————————————┙
                                    │   comando    │
                                    ┕——————————————┙
* * *

Come abbiamo accennato all'inizio dl corso, l'utente che segnala la sua presenza, richiede l'apertura di una chiamata e lo fa attraverso l'interfaccia di ingresso; nterviene il sistema di comando, che seleziona il canale con cui la chiamata dovrà essere gestita, ovvero determina come il circuito dovrà essere costituito, e ne comanda la creazione alla rete di connessione [matrice di commutazione].

**Vantaggi**:

* banda costante garantita;
* ritardi di trasferimento [latenze] costanti;
* trasparenza del circuito (formati, velocità, protocolli);
* bassi ritardi nell'attraversamento dei nodi.

**Svantaggi**:

* risorse dedicate a una comunicazione;
* efficienta buona solo per sorgenti non intermittenti;
* tempo di apertura del circuito;
* nessuna conversione di formati, velocità, protocollo;
* tariffazione in base al tempo di esistenza del circuito.

### Commutazione di pacchetto

Idea:

* non si allocano risorse per l'uso esclusivo di due o più utenti;
* studiata espressamente per sorgenti intermittenti;
* funzionamento simile al sistema postale.

**Il pacchetto**
Le informazioni non vengono inviate così come sono, ma vengono segmentate in *unità di dati* [PDU] che comprendono informazioni di utente e di controllo.

Enunciamo alcuni acronimi:

* **PDU**: Protocol Data Unit;
* **PCI**: Protocol Control Information;
* **SDU**: Service Data Unit;

* * *
La forma generica di pacchetto PDU:

    ┍——————————┯————————————————————————————————————┑
    │          │                                    │
    │ PCI      │ SDU                                │
    │ HEADER   │ DATA                               │
    │          │                                    │
    ┕——————————┷————————————————————————————————————┙
* * *

* * *
Per cultura:
*unità dati* è una terminologia IDO, altri termini utilizzati sono:

* pacchetto (packet) [il più comune];
* cella;
* datagramma (datagram);
* segmento (segmant);
* messaggio (messaggio);
* trama (frame).

* * *

Le unità dati sono consegnate insieme, e ogni nodo ha il compito di:

* memorizzare il pacchetto;
* elaborare il pacchetto e determinare il canale su cui inoltrarlo;
* mettere il pacchetto in coda per la trasmissione sul canale.

In tre parole, il funzionamento è **store and forward**.

**Modo di funzionamento dominante**
Occorre disporre dell'intestazione prima di poter effettuare l'instradamento. L'instradamento richiede tempo e occorre anche verificare che non siano stati commessi errori durante la trasmissione, nonché tenere in considerazione le diverse capacità dei mezzi trasmissivi che richedono trasmissioni a bit rate diversi.

Se un attraverso il mezzo trasmissivo non è possibile smaltire i pacchetti in tempo reale (per quanto si possa dire reale, diciamo “presente esteso“), si forma una coda di pacchetti. Come se tutte le auto di un autostrada a tre corsie si riversasse su una stada normale. L'immissione dà origine alla coda, sia per la quantità di auto che devono immetersi, sia per via della riduzione del limite di velocità.

La memorizzazione entra in gioco però anche in ingresso: non tutti i pacchetti possono essere sempre elaborati velocemente, può essere che la CPU del nodo che dovrebbe smaltire i pacchetti sia sovraccarica e che alcuni pacchetti restino in coda per essere elaborati.

* * *
         ┍––––––———————┑   ┍———┑   ┍——————————————┑   ┍———┑   ┍—————————————┑
         │             │   │ Q │   │   Rete di    │   │ Q │   │             │
         │ Interfaccia │   │ u │   │ connessione  │   │ u │   │ Interfaccia │
    ——/——│     di      │—/—│ e │—/—│      o       │—/—│ e │—/—│     di      │——/——>
         │  ingresso   │   │ u │   │  Matrice di  │   │ u │   │   uscita    │
         │             │   │ e │   │ commutazione │   │ e │   │             │
         ┕—————————————┙   ┕———┙   ┕——————ˆ———————┙   ┕———┙   ┕—————————————┙
                │            │            │             │           │
                ┕————————————┙            │             ┕———————————┙
                         │                │                 │
                       ┍———————————————————————————————————————┑
                       │                                       │
                       │         Sistema di controllo          │
                       │                                       │
                       ┕———————————————————————————————————————┙
* * *

Come anticipato, siccome le informazioni da inviare potrebbero essere molto grandi, a fronte di pacchetti di dimensione fissa (o variabile, ma comunque di lunghezza esigua) e che devono essere memorizzati in apparati di rete, l'informazione da inviare va suddivisa in diversi pacchetti e verrà ricostruita dal destinatario.

**Ritardi dorante la commutazione**
Ogni pacchetto, per essere trasmesso, impega del tempo. Il tempo che impiega un pacchetto ad essere ricevuto dal destinatario è detto **ritardo** o **latenza**.

Su ogni canale esiste un ritardo:

* di trasmissione: funzione della dimensione in bit del pacchetto e della velocità di trasissione;
* di propagazione: funzione della dimensione in metri del canale.

In ogno nodo esiste invece un ritardo:

* di elaborazione: funzione della velocità con cui si eseguono le procedure di instradamento e di eventuali controlli sul pacchetto;
* di accodamento: funzione del traffico generato da tutti gli utenti.

### Definizioni e formule

*def.* Si definisce misura di un pacchetto è la dimensione in bit P data da $P = PCI + SDU$

*def.* Tempo di trasmissione $T_{TX}$ di un pacchetto:

* varia da canale a canale;
* la velocià di trasmissione è data da $T_{TX} = \dfrac{P}{V_{TX}} = [s]$

*def.* la dimensione in metri M di un pacchetto su un canale è dato da: $M = \dfrac{2}{3} \; c \; T_{TX}$

### Parallelizzazione - La pipeline

Supponiamo che il tempo di propagazione e di elaborazione siano nulli [trascurabili] e che anche le intestazioni lo siano. Siccome stiamo parlando di commutazione di pacchetto, consideriamo che i nodi lavorino con paradigma store & forward.

*Domanda* Qual è il tempo necesario per trasmettere un pacchetto da 10 kB? In particolare, è meglio inviare un unico pacchettone da 10 kB oppure 10 pacchetti da 1kB?

<img src="src/l6_01.svg" />

#### Pacchettone

$$
t_{TX, 1} = \dfrac{P}{V_{TX}} = \dfrac{10\;kB}{80\;kb/s} = \dfrac{80000}{80000} = 1s
$$

Ovvero A trasmette a B, il quale riceve il pacchetto dopo 1s. Successivamente, B trasmette a C, il quale riceve il pacchetto dopo 1s. Ripetere per C e D. Il pacchetto **impiega 3s per arrivare a destinazione**.

#### Pacchettini

$$
t_{TX, 1} = \dfrac{P}{V_{TX}} = \dfrac{10\;kB}{80\;kb/s} = \dfrac{8000}{80000} = 0.1s
$$

Ovvero A trasmette a B, il quale riceve il pacchetto dopo 0.1s. Ma ora, mentre un pacchetto è ricevuto da B, il successivo può essere inviato da A.

|   Tempo      |  Nodo A  |  Nodo B  |  Nodo C  |  Nodo D  |
|--------------|:--------:|:--------:|:--------:|:--------:|
|$t_0$         |  TX P1   |          |          |          |
|$t_0 + 0,1s$  |  TX P2   | RX/TX P1 |          |          |
|$t_0 + 0,2s$  |  TX P3   | RX/TX P2 | RX/TX P1 |          |
|$t_0 + 0,3s$  |  TX P4   | RX/TX P3 | RX/TX P2 | RX/TX P1 |
|$t_0 + 0,4s$  |  TX P5   | RX/TX P4 | RX/TX P3 | RX/TX P2 |
|$t_0 + 0,5s$  |  TX P6   | RX/TX P5 | RX/TX P4 | RX/TX P3 |
|$t_0 + 0,6s$  |  TX P7   | RX/TX P6 | RX/TX P5 | RX/TX P4 |
|$t_0 + 0,7s$  |  TX P8   | RX/TX P7 | RX/TX P6 | RX/TX P5 |
|$t_0 + 0,8s$  |  TX P9   | RX/TX P8 | RX/TX P7 | RX/TX P6 |
|$t_0 + 0,9s$  |  TX P10  | RX/TX P9 | RX/TX P8 | RX/TX P7 |
|$t_0 + 1,0s$  |          | RX/TX P10| RX/TX P9 | RX/TX P8 |
|$t_0 + 1,1s$  |          |          | RX/TX P10| RX/TX P9 |
|$t_0 + 1,2s$  |          |          |          | RX/TX P10|

Il pacchetto **impiega 1,2s per arrivare a destinezione**.

#### Sui pacchetti

La lunghezza dei pacchetti è determinata da

* possibilità di parallelizzazione (pipeline);
* ritardo di pacchettizzazione.

Pacchetti brevi riducono il ritardo di pacchettizzazione (importante per traffico veloce su rete a pacchetto). Attenzione però, ogni pacchetto richiede un'intestazione, dunque non si può ridurre troppo la dimensione dei pacchetti.

Bisogna scegliere una lunghezza che renda minima la percentuale di informazione di controllo data da $\dfrac{i}{s + i}$, con $i = length(PCI)$ e $s = length(SDU)$.

Pacchetti brevi riducono la probabilità di errore, ricordare che esistono sempre meccanismi di controllo sull'integrità della trasmissione. Un errore (un bit) su un pacchetto comporta lo scarto del pacchetto. Se il pacchetto è breve, è facile reinviare il pacchetto senza grossi problemi, se il pacchetto è lungo, la ritrasmissione occuperà più tempo sul canale.

Dato un paccheto da $n$ bit, un canale con errori indipendenti, e la probabilità $p$ che un pacchetto sia errato, la probabilità che un pacchetto venga trasmesso correttamente è $(1 - p)^n$

#### Pacchetti e commutazione

Rispetto alla commutazione di circuito, quella basata sui pacchetti ha alcuni vantaggi non trascurabili: (riassunto)

* utilizzo efficiente delle risorse anche in presenza di traffico intermittente;
* possibilità di controlo della correttezza delle informazioni trasmesse lungo il percorso (anziché solo a destinazione: se un pacchetto è corrotto, viene scartato dai nodi);
* possibilità di conversioni di velocità, formati, protocolli;
* tariffazione in funzione del traffico trasmesso (anziché in base al tempo impiegato).

Svantaggi non trascurabili:

* difficile ottenere garanzie di banda;
* elaborazione di ogni pacchetto in ogni nodo;
* ritardo di trasferimento variabile.

## Datagram & Circuito virtuale

Quella che abiamo visto fin ora è una forma di commutazione di pacchetto chiamata *datagram*. Non è l'unica forma di commutazione di paccheto esistnte, esiste anche un'altra modalità chiamata *circuito virtuale* che serve per emulare il più possibile il concetto di commutazione di circuito. L'idea di base è di trovare un modo per commutare sì a pacchetto, ma con una fase preliminare — nel senso di antecedente alla trasmissione vera e propria dei dati — in cui si segnala alla rete l'intento di aprire di comunicare i dati verso un altro utente.

La comunicazione con circuito vituale avviene dunqua in tre fasi: (macro)

* apertura connessione (segnalazione);
* trasferimento data;
* chiusura connessione (segnalazione).

I due concetti chiave di questo tipo di trasmissione sono i seguenti:

* **esiste un accordo preliminare tra i due interlocutori e il fornitore del servizio (rete)**: questo significa che non si prenota il percorso — il canale — in modo esclusivo, ma il nodo sorgente informa i nodi che dovrà attraversare che verranno inviati dei pacchetti con una particolare etichetta (contenuta nell'intestazione) che dovranno essere tutti inviati lungo lo stesso percorso;
* pacchetti diversi, con uguale destinazione seguono lo stesso percorso.

Questo tipo di commutazione non è in grado di garantire la larghezza di banda come farebbe la commutazione di circuito, ma garantisce che ogni pacchetto di una certa connessione seguirà lo stesso percorso.

### Vantaggi

* Mantedimento della sequenza;
* minor variabilità dei ritardi;
* instradamento solo in fase di apertura.

### Informazioni di indirizzamento

**Datagram**
Nel caso di comunicazione datagram, occorre identificare ogni pacchetto con la coppia sorgente-destinazione
**Circuito virtuale**
Nel caso di circuito virtuale, non è sufficiente identificare ogni pacchetto con la coppia sorgente-destinazione, ma serve identificare anche anche la connessone (l'etichetta di cui sopra).
