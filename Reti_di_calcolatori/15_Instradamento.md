# Instradamento

Tre elementi:

* protocolli di instradamento: definisce il modo con cui i router si scambiano informazioni sulla rete;
* algoritmo di instradamento:
  * cercare un buon percorso per inviare i dati;
  * scrivere le tabelle di routing;
* procedura di forwarding.

## Algoritmo

Gli algoritmi di instradamento devono tentare, a partire dalle informazioni scambiate tra i router, di astrarre la topologia di rete con un grafo e scegliere un buon percorso per inoltrare un certo pacchetto al prossimo hop.

### Costo

Gli archi del grafo sono caratterizzati da un peso, il costo. Esso dipende dalle latenze, dal costo monetario che l'utilizzo di un ramo comporta, il livello di congestione.

Il costo può infatti essere statico o dinamico (in genere statico).

### Algoritmi semplici

#### Random

Scelgo a caso la porta di uscita.

#### Flooding

Instrado verso tutte le porte di uscita.

#### Deflessione o hot potatoe

Su topologie regolari instrada verso la prima porta corretta, se libera. Se occupata, instrada verso un'altra porta.

> Si tratta di algoritmi che non prevedono di conoscere la topologia della rete.

### Calcolo di persorso

#### Centralizzato

Un nodo si occupa di raccogliere l'informazione da tutti gli altri nodi, calcola i i percorsi e redistribuisce le tabelle di routing compilate a tutti gli altri nodi.

#### Distribuito

Tutti i nodi si scambiano informazione tra loro (utilizzando protocolli di instradamento) e calcolano i percorsi (indipendentemente o in base a quanto fatto dai nodi adiacenti).

* **globale**: tutti i nodi conoscono la topologia completa, compresi i costi dei canali; algoritmi **link-state** -> **OSPF**;
* **parziale**: i nodi conoscono solo i nodi a loro fisicamente adiacenti; algoritmi **distance vector** -> **RIP**, **BGP**. 

### Dinstance Vector

Si tratta di algoritmi iterativi che continuano fino a quando i nodi non scambiano più informazioni e terminano in modo autonomo.

Essenzialmente ogni nodo comunica al proprio vicino la propria tabella di routing. Il routing ricevente utilizza questa tabella di touting oer adeguare la propria. Se un nodo sbaglia a compilare la tabella, per qualuncue motivo, l'errore potrebbe essere propagato nella rete verso altri router.

Non è molto scalabile perché funziona bene finché le destinazioni raggiungibili dal router non sono troppe. C viene in aiuto il meccanismo di aggregazione degli indirizzi.

#### Implementazione

* Struttura dati: tabella distanze;
* ogni nodo possiede la propria:
  * ogni riga per ogni possibile destinazione;
  * una colonna per ogni nodo adiacente;

**Esempio** Nel nodo $X$, per la destinazione $Y$ attraverso il nodo adiacente $Z$:

$$
    D^X(Y, Z) = c(X, Z) + \text{min}_w\{D^Z(Y, w)\}.
$$

### Confronto

#### Complessità messaggi

Con $M$ nodi, $E$ canali per nodo

* LS: ogni nodo invia $O(M)$ messaggi, ciascuno lungo $O(E)$;
* DV: ogni messaggio contiene tutte le destinazioni O(M), ed è mandato a $O(E)$ vicini $O(E M)$.

#### Velocità di convergenza

* LS: ogni volta che un link state è propagato, ho nuova topologia: convergenza immediata.
* DV: scelte nodo dipendono da scelte nodi vicini; si richiedono piùs cambi di messaggi: tempo di convergenza variabile.
