# Topologia

*def.* Una topologia di rete è un insieme di nodi e segmanti.
Un *nodo* è un punto in cui avviene una commutazione, mentre un *segmanto* è un mezzo trasmissivo che connette più nodi.

Esistono poi diversi tipi di canali:

* **punto-punto**: un canale connette due nodi e viene utilizzato in modo paritetico;
* **multi-punto**: più nodi sono collegati allo stesso canale: un dono in particolare, il *master* coordina l'utilizzo del canale da parte di n nodi *slave*;
* **canale broadcast**: un canale utilizzato da ogni punto della rete (per esempio letere su cui lavora la rete telefonica):
  * l'informazione inviata è potenzialmente leggibile da ogni nodo;
  * se i nodi posseggono un indirizzo, si realizza *di fatto*, ovvero *a livello logico*, una trasmissione punto-punto.

Data la definizione di topologia, l'insieme dei nodi e canali, e la loro disposizione, costituisce la topologia della rete.

Dal punto di vista concettuale, rappresentiamo una rete come un **grafo** in cui i vertici sono i nodi della rete e gli archi sono i canali. In particolare il grafo è **orientato** in quanto alcuni mezzi trasmissivi (e.g. la fibra ottica) sono unidirezionali).

Definiamo:

* **V** l'insieme dei vertici;
* **A** l'insieme dei canali;
* **N = |V|** il numero dei vertici;
* **C = |A|** il numero dei canali.

## Alcune tipologie

### Maglia completa

Ogni nodo della rete è collegato a ogni altro nodo.
**Vantaggi**: molto tollerante ai guasti; esistono molti percorsi alternativi tra due nodi, anche se un solo percorso diretto; esiste una scelta ovvia di percorso minimo.
**Svantaggio**: il numero di canali è molto elevato: $C = \dfrac{N(N - 1)}{2}$, e dunque può essere utilizzata solo se i nodi sono pochi, per esempio per i nodi di commutazione della rete nazionale.

### Albero

Per ogni coppia di nodi esiste solo un persorso possibile.
**Vantaggo**: basso numero di canali: $C = N - 1$.

**Svantaggio**: vulnerabilià ai guasti: la compromissione di un singolo canale può causare l'incomunicabilità con la maggior parte della rete.
In generale è utilizzata quando è necessario ridurre il numero di canali.

### Stella attiva

Ogni nodo è collegato agli altri mediant un unico canale.
**Vantaggio**: basso numero di canali.
**Svantaggio**: vulnerabilità ai guasti del cenro stella.
Il numero di canali è $C = N$ e ogni nodo è collegato attraverso un mezzo trasmissivo a un *centro stella* (switch) che commuta le informazioni inviate da un mittente al corretto riceventa: la complessità nella scelta dei percorsi è demandata al centro stella. Questa topologia è utilizzata nelle reti locali, nelle reti via satellite e nelle reti radio cellulati.

### Stella passiva

&Egrave; simile alla stella attiva, salvo per il fatto che il centro stella (hub) non è intelligente e non commuta le informazioni in base al destinatario ma ogni informazione che riceve la invia a tutti. Dato che ogni informazione viene inviata a tutti, consideriamo $C = 1$, e un unico canale di broadcast.

### Maglia semplice (o gerarchica)

Ogni nodo è conesso al meno a un altro nodo.

**Vantaggio**: tolleranza ai guasti e numero di canali selezionabile a piacere purché rispetti
$$
N - 1 < C < \dfrac{N (N - 1)}{2}.
$$

**Svantaggio**: topologia non regolare.
Questa è - di fatto - la topologia più utilizzata (internet, telefonia).

### Anello

I nodi sono ordinati e ogni nodo è connesso al precedente e al successivo. Il nodo può essere unidirezionale o bidirezionale. L'ultimo nodo è connesso al primo. Il numero di canali è:

* caso unidirezionale

$$
C = \dfrac{N}{2};
$$

* caso bidirezionale

$$
C = N.
$$

Nel caso bidirezionale esistono due percorsi possibili per ogni nodo, nel caso unidirezionale, uno solo. Questa topologia è molto utilizzata per reti locali e metropolitane.

### Bus passivo

Esiste un unico canale di broadcast, per alcuni mezzi trasmissivi i nodi devono sincronizzarsi in modo tala da poter trasmettere informazioni.

### Bus attivo

Si tratta in realtà di un caso particolare di albero in cui i nodi sono - di fatto - connessi in sequenza. Può anche essere visto come un anelllo bidirezionale a cui manca un canale.

## Topologia fisica o logica?

Supponiamo che Alice telefoni a Bob: l'effetto ottenuto è che due telefoni cellulari sono in grado di comunicare. Dal **punto di vista logico**, i telefono comunicano *direttamente* mediante l'utilizzo di un canale; ma dal **punto di vista fisico**, non è così. Il canale, infatti, *fisicamente* è un insieme di mezzi trasmissivi: il telefono di Alice sta comunicando attraverso l'etere con un antenna, dall'antenna la comunicazione viene deviata - attraverso una serie di cavi nell'area metropolitana - alla rete nazionale dalla quale viene ulteriormente deviata verso l'area metropolitana in cui si trova Bob e a questo punto passata all'antenna più vicina a Bob che trasmettera al telefono di Bob. Fisicamente le cose sono più articolate e complesse, fortunatamente in molte applicazioni non è necessario tenere conto della struttura fisica della rete.

In generale definiamo la topologia logica la *topologia percepita*. Si può dire anche che la topologia logica permette di comprendere il funzionamento della rete.

## Perché parliamo di topologia?

> Vedere esempi lezione 03 / Marchetto

La scelta della topologia ha ricadute sulle prestazioni della rete. In particolare, *a pari capacità disponibile sui canali, la quantità di traffico smaltibile da una rete dipende dalla media della distanza tra ogni coppia di nodi della rete, pesata dalla quantità di traffico scambiata tra i due nodi*. Per traffico uniforme e topologie regolari, il traffico smaltibile è inversamente proporzionale alla distanza media.
