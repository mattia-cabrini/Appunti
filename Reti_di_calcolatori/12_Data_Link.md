# Data Link

* Delimitazione della trama:
  * delimitatori espliciti;
  * indicatori di lunghezza;
  * lunghezza fissa;
  * silenzi tra pacchtti.

* Multiplazione: se allo strato superiore ci sono diverse entità, ci sono modi per permettere al ricevitore se i dati vadano consegnati a una certa entità o a un'altra.
* Indirizzamento locale: dato che questo strato (2) può definire il destinatario dei dati (indirizzo MAC, nelle reti TCP/IP).
* Rilavamento errori: aggiungendo CRC, bit di parita, FEC o altro.
* Controllo di flusso (protocolli a finestra).
* Controllo di sequanza (protocolli a finestra).
* Correzione dell'errore (protocolli a finestra).
* Protocolli di accesso multiplo (solo per canali condivisi): multiplazioni.
* Controllo di flusso sull'interfaccia (verso livelli superiori).

## Protocolli

Ricordiamo i protocolli IBM e derivati:

* SDLC (IBM);
* HDLC (High Data-Link Control): standard ANSI, compatibile con SDLC;
* Da HDLC deriva:
  * LAPB, obsoleto;
  * LAPD, obsoleto;
  * LAPF, obsoleto;
  * LLC 802.2 (Logical Link Control — LAN), usato;
  * PPP (Point-to-Point Protocol), usato;
  * LAPDm (LAP for the mobile D chanel — GSM), usato.

Questi protocolli sono utilizzati

* in reti pubbliche di accesso, derivati di SDLC;
* in reti private (locali) er l'interconnessione di apparati in ambienti circoscritti con prevalenza di collegamenti bradcast;
* reti pubblica di trasporto: ATM, Frame Relay.

## Notazione

### DTE

> Data Terminal Equipment

Apparato utente

### DCE

> Data Circuit-terminating Equipment

Apparati del gestore

## Caratteristiche dei protocolli Data-Link

### Privati e pubblici

Si utilizzano diversi protocolli per la coomunicazione sulla rete privata e sulla rete pubblica.

### Caratteristiche comuni

Formato generico delle PDU nelle reti pubbliche e private:

```text
              |------------------FRAME-------------------|
Contenuto:    |01111110|ADDRESS|CONTROL|DATA|CRC|01111110|
Length (bit): | 8      | 8     | 8/16  | >=0| 16| 8      |
```

Entrabni sono protocolli orientati al bit o al byte e hanno flag delimitatore che non può compatire nella PDU per garantire la trasparenza dei dati. Il fatto che non debba comparire il flag significa che occore eseguire bit stuffing o byte stuffung. Per protocolli orientati al bit, la sequenza `01111110` viene inviata come `01111110` e il ricevitore dovrà correttamente interpretare tale sequnza. Per i byte oriented si utilizzano ivece dei byte di escape (un po' come backslash per le stringhe in `C`).

L'indirizzo serve per le configurazioni multiple di tipo master-slave.

Il campo di controllo differenzia le PDU.

### PPP

> RFC 1661

Serve per eseguire trasmissioni Point-to-Point, facile da gestire e suddiviso in tre sottoprotocolli:

* incapsulamento;
* link connection protocol (LCP);
* network control protocol (NCP).

#### Obiettivi di PPP

* Delimitazione delle PDU.
* Trasparenza del contenuto (byte stuffing).
* Riconoscimento (NON la correzione) degli errori) ,non utilizza protocolli a finestra.
* Multiplazione di più protocolli di strato di rete (livello tre).
* Controllo dell'attività sul collegamento.
* Negoziazione dell'indirizzo di livello di rete (livello tre, tipicamente IP): i nodi ai due estremi del collegamento apprendono o configurano i propri indirizzi di rete.
* Semplicità.

#### Cosa non fa PPP

* Correzione o recupero degli errori.
* Controllo di flusso.
* Mantenimento della sequenza (inutile data la netura del collegamento punto-punto).

(Non menziono la non genstione dei collegamenti multi-punto).

#### Struttura di una trama PPP

* Flag
* Address: senza significato, mantenuto per retrocompatibilità con HDLC.
* Control: come per Address.
* Protocol: il protocollo di livello superiore cui destinare i dati (simile a IEEE 802.2 LLC).

```text
|01111110|11111111|00000011|PROTOCOL|INFO|CHECK|01111110|
| FLAG   | ADDRESS| CONTOL |        | SDU|     | FLAG   |
| 1B     | 1B     | 1B     | 1-2B   | ?B | 2-4B| 1B     |
```

#### PPP-LCP

Il protocollo PPP-LCP crea e abbatte il cllegamento PPP, negoziando le opzioni  (max frame length, authentication protocol, skip address and control fields). Inizia nello stato di DEAD e una volta stabilito il collegamento PPP e fatta l'autenticazione, è configurata la tipologia di connessione.

<div align="center">
  <img src="src/i9.svg" />
</div>

## ATM

> Asynchronous Transfert Mode

Si tratta di una tecnologia nata per tutt'altro. È rimasta come soluzione per lo strato Data-Link nel tratto che va dall'armadio di centrale alla centrale.

Obiettivi:

* velocità elevate: ~&22 Mbps;
* bassa latenza;
* uso di PDU dette celle, di dimensione fissa pari a 53 byte (48 byte di dati).

L'idea di avere celle di dimensione fissa rende le operazioni di commutazione estremamente rapide, mentre l'dea della cella piccola rende possibile

* bassa latenza e
* basso ritardo di pacchettizzazione della voce.

Esistono due formati un po' diversi:

* UNI: User Network Interface;
* NNI: Network-to-Network Interface.

Essendo la rete ATM una vera e propria rete magliata, nell'intestazione è possibile inserire informazioni sulla provenienza e sulla destinazione delle celle. Alcuni bit dell'intestazione servono per identificare l'utente.

Cella UNI:
<table style="text-align: center; border: 1px solid #BBBBBB; border-collapse: collapse;">
        <tr style="border: 1px solid #BBBBBB; border-collapse: collapse;">
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" width="35px">Byte</td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" width="35px">b1</td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" width="35px">b2</td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" width="35px">b3</td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" width="35px">b4</td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" width="35px">b5</td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" width="35px">b6</td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" width="35px">b7</td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" width="35px">b8</td>
        </tr>
        <tr style="border: 1px solid #BBBBBB; border-collapse: collapse;">
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;"><b>1</b></td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" colspan="4">GFC</td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" colspan="4">VPI</td>
        </tr>
        <tr style="border: 1px solid #BBBBBB; border-collapse: collapse;">
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;"><b>2</b></td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" colspan="4">VPI</td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" colspan="4">VCI</td>
        </tr>
        <tr style="border: 1px solid #BBBBBB; border-collapse: collapse;">
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;"><b>3</b></td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" colspan="8">VCI</td>
        </tr>
        <tr style="border: 1px solid #BBBBBB; border-collapse: collapse;">
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;"><b>4</b></td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" colspan="4">VCI</td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" colspan="3">PT</td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" colspan="1">CLP</td>
        </tr>
        <tr style="border: 1px solid #BBBBBB; border-collapse: collapse;">
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;"><b>5</b></td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" colspan="8">HEC</td>
        </tr>
    </table>

Cella NNI:
<table style="text-align: center; border: 1px solid #BBBBBB; border-collapse: collapse;">
        <tr style="border: 1px solid #BBBBBB; border-collapse: collapse;">
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" width="35px">Byte</td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" width="35px">b1</td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" width="35px">b2</td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" width="35px">b3</td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" width="35px">b4</td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" width="35px">b5</td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" width="35px">b6</td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" width="35px">b7</td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" width="35px">b8</td>
        </tr>
        <tr style="border: 1px solid #BBBBBB; border-collapse: collapse;">
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;"><b>1</b></td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" colspan="8">VPI</td>
        </tr>
        <tr style="border: 1px solid #BBBBBB; border-collapse: collapse;">
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;"><b>2</b></td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" colspan="4">VPI</td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" colspan="4">VCI</td>
        </tr>
        <tr style="border: 1px solid #BBBBBB; border-collapse: collapse;">
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;"><b>3</b></td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" colspan="8">VCI</td>
        </tr>
        <tr style="border: 1px solid #BBBBBB; border-collapse: collapse;">
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;"><b>4</b></td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" colspan="4">VCI</td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" colspan="3">PT</td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" colspan="1">CLP</td>
        </tr>
        <tr style="border: 1px solid #BBBBBB; border-collapse: collapse;">
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;"><b>5</b></td>
            <td style="border: 1px solid #BBBBBB; border-collapse: collapse;" colspan="8">HEC</td>
        </tr>
    </table>

<br />

Le sigle:

* VPI: Virtual Path Identifier;
* VCI: Virtual Circuit Identifies (a chi appartiene l'armadio?).
* GFC: Generic Flow Control;
* PT: Payload Type;
* CLP: Cell Loss Priority;
* HEC: Header Error Code (essendo generalmente una trasmissione su fibra ottica, l'errore capita con probabilità $P(E) = 10^{-12}$).

Per identificare la fine di un pacchetto di livello superiore, il terzo bit del campo PT viene posto a $1$.

## Livello 2 nelle LAN

Per LAN (Local Area Network) sono reti con minima estensione geografica. Nelle reti locali è frequente l'utilizzo di canali tramissivi comuni; se si utilizza un mezzo trasmissivo condiviso, può trasmettere un solo nodo alla volta in quanto:

* il traffico è impulsivo, se ogni utente avesse a disposizione una frequenza i canali sarebberoo male utilizzati;
* trasmissione broadcast: serve definire degli indirizzi per trasmissioni unicast.

Il problema della condivisione del canale non è un affare scontato. Per riuscire effettuare una multiplazione efficiente senza un protocollo ausiliario o senza nodi extra che facciano da moderatori, si è cercato di imitare nelle reti il comportamento umano. Esempii:

* un moderatore (non nodo extra) decide chi parla;
* allocazion su alzata di mano (prenotazione);
* accesso libero;
* accesso libero, ma educato (se qualcuno parla, mi taccia);
* passaggio ciclico di testimone (multiplazione di tempo).

### Parametri per studiare l'efficienza del protocollo

* Capacità e traffico smaltito (throughput)
* Equità
* Ritardo (accesso, propagazione, consegna)
* Numero di nodi (stazioni), lunghezza della rete, topologia, facilità di realizzazione.

### Classificazione dei protocolli

* A contesa o accesso casuale (Ethernet e WiFi)
* [In disuso] Ad accesso ordinato o a token (Token Ring, Token Bus, FDDI)
* [In disuso] A slot con prenotazione (DQDB)

### Topologie

* Bus
* Anello
* Stella
* Stella gerarchica
* Bus monodirezionale

### Protocolli ad accesso casuale

Quando un nodo vuole trasmettere, esso trasmette la trama alla velocità $R$ del canale senza coordinarsi con altri nodi. Se due o più nodi trasmettoro contemporaneamente, essi collidonno. I protocolli MAC possono specificare modi con cui ridurre la probabilità di collisione, come riconoscere una collisione e come fare un recupero dei dati trasmessi in caso di collisione.

#### Protocollo Aloha, per esempio

Ogni stazione trasmette sul canale blu e riceve sul canale osso. Se avviene un collisione, il trasmettitore deve reinviare il pacchetto. Attenzione però, il timeout deve essere casuale, se non lo fosse e se fosse uguale per tutti i trasmettitiri, le collisioni persisterebbero. Non ci sono `ACK`, ma il master ritrasmette sul canale rosso tutte le trame ricevute.

Il canale rosso e il canale blu sono multiplati in frequenza.

Uno dei problemi i questo protocollo è che ha back-off esponenziale; ciò significa che, potenzialmente, il timeout potrebbe raddoppiare ogni volta che il medesimo pacchetto collide.

<div align="center">
  <img src="src/i10.svg" width="350px" />
</div>

Se visualizzassimo su un grafico il traffico smaltito normalizzato evolversi nel tempo, prima vedremo una fase di ascesa (lineare) che, nella migliore delle ipotesi, raggiungerebbe il $18\%$ (saturazione) e poi una fase di instabilità che porterebbe l'efficienza verso lo $0\%$.

#### Slotted Aloha, sempre per esempio

Cerchiamo di salvare il salvabile nel seguente modo: dividiamo il tempo in istanti fissi, detti slot. Il nodo non può trasmetttere quando vuole, ma deve trasmettere solo all'interno di uno slot. Solo in apparenza rimuove il concetto di casualità che di fatto viene conservato in quanto gli slot sono casuali.

Slotted Aloha raggiungie il $37\%$ di efficienza prima di instabilità.

La casualità dello slot è dovuta al fatto che lo slot si apre se il nodo vuole trasmettere deve lanciare un dado. Se esce un certo valore, trasmette, altrimenti niente.

#### CSMA

> Carrier Sense Multiple Access, per una trasmissione più educata.

La bassa efficienza dei protocolli precedenti è dovuta al fatto che gli attori si parlano sopra. L'ida alla base di CSMA è di stare in ascolto sul canale e inviare pacchetti solo quando il canale è libero.

* CSMA 1-Persistemte: aspetto che si liberi il canale e poi trasmetto;
* CSMA Non-Persistente: riprovo a sentire il canale dopo un tempo casuale, se libero trasmetto;
* CSMA P-Persistente: aspetto che si liberi il canale e trasmetto con probabilita $P$ o rimando la trasmissione con probabilità $1-P$.

> Note a margine:
> 
> * le collisioni sono **inevitabili**;
> * la distanza ioca un ruolo fondamentale nella probabilità di collisione;
> * con trame di grandi dimensioni (nel tempo), a parità di traffico trasmesso, il numero di contese diminuisce, e dunque anche il numero di collisioni.

Purtroppo anche questo protocollo, ad alto carico soffre un'intabilità.

#### CSMA-CD

> CSMA Collision Avoidance, adatto per canali radio.

Mentre trasmesso ascolto anche il canale. Se sento solo la mia trasmissione, proseguo, se sento anche un'altra collisioone, aspetto. Non è un modo per evitare le collisioni, ma solo per ridurre lo spreco di tempo dovuto alla trasmissione inutile.

Efficace per:

* reti piccole;
* reti piccole rispetto alla dimensione della trama, parametro `a` piccolo;
* velocità di trasmissione bassa.

#### CSMA-CA

> CSMA Collision Detection, adatto per mezzi cablati.

Il protocollo CSMA-CD è applicaile solo per reti cablate, in quando il suo funzionamento prevede implicitamente l'appartenenza di tutti i TX e RX nello stesso dominio di collisione. Nel contesto del canale radio, non è così facile. Un trasmettitore potrebbe sentire un altro trasmettitore vicino che trasmette in collisione, ma non potrebbe sentire un trasmettitore più lontano, sempre in trasmissione e in collisione. Nella figura, `A` e `B` si sentono collidere a vicenda, ma non sentono `C`.

<div align="center">
  <img src="src/i11.svg" width="350px" />
</div>

L'idea è di prevedere le collisioni prima che avvengano. Una stazione che voglia trasmettere deve prima ascoltare il canale:

* se il canale è libero per un periodo detto **DIFS**, la stazione inizia la trasmissione;
* se il canale è occupato (o lo diventa durante DIFS), la stazione sospende la procedura di trasmissione per un tempo casuale di back-off.

Una stazione decrementa il back-off solo mentre il canale rimane libero. Quando il backoff arriva a $0$, ripete procedura di TX.

Per inviare l'`ACK` il ricevitore attende un tempo $SIFS < DIFS$.

Se il trasmettitore non riceve l'`ACK`:

* back-off;
* ritente la trasmissione.

Efficiente per reti piccole.

Protocollo adottato da Wi-Fi.
