# Introduzione

## Definizioni ed esempi

**Comunicazione** definiamo *comunicazione* il trasferimento di informazioni secondo convenzioni prestabilite.
**Telecomunicazione** qualsiasi trasmissione e ricezione di segnali che rappresentano segni, scrittura, immagini, suono; informazioni di qualsiasi natura, attraverso cavi, radio o altri sistemi ottici.

Notare la parola **convenzione**: non solo dobbiamo inviare delle informazioni, dobbiamo anche stabilire il modo con cui inviarle, un insieme di regole. Se due attori condividono le stesse regole per trasmettere e ricevere informazioni, allora possono comunicare. Questo non è un concetto lontano dalla vita quotidiana: se due persone parlano la stessa lingua, riescono a comunicare, altrimenti no.

Pensiamo a un esempio più tecnico: gli **apparecchi telefonici**. La percezione di un utente qualcunque è quella di un dispositivo connesso tramite un cavo a una rete telefonica. Il nostro obiettivo è capire come funziona la *rete telefonica*, e più in generale come funziona un generico *servizio di telecomunicazione*.

**Servizio di telecomunicazione** ciò che viene offerto da un gestore pubblico o privato ai propri clienti al fine di soddisfare una specifica esigenza di telecomunicazione.
**Funzioni in una rete di telecomunicazione** operazioni svolte all'interno di una rete di telecomunicazioni.

### Esempio

Per iniziare a capire le TLC, partiamo da esempi di vita quotidiana: per esempio, chi a casa possiede un telefono fisso collegato alla rete telefonica, può provare ad alzare la cornetta e a portarla all'orecchio. Quello che sentirà è il cosiddetto *tono di libero*. Tale tono non è casuale, è già una comunicazione: sentire il tono di libero significa essere certi che il telefono è collegato alla rete telefonica ed è pronto per effettuare una chiamata. Dunque possiamo comporre un numero di telefono. Dunque quali sono le funzioni?

- **Segnalazione di utente** la prima funzione permessa dalla rete è la *segnalazione di utente*, ovvero lo scambio di informazioni che riguardano l'apertura, il controllo e la chiusura di connessioni e la gestione di una rete di telecomunicazioni. Attenzione: le informazioni non sono quelle che il mittente vuole inviare al destinatario, ma quelle che l'utente scambia con la rete: alzando la cornetta, l'utente segnala alla rete la sua presenza.
- **Commutazione (switching)** una volta fornito il numero di telefono del destinatario, la rete individua le risorse necessarie per collegare i due utenti e stabilisce un *circuito* per il tempo necessario al trasferimento dei segnali. Le risorse che il processo di commutazione individua sono le *unità funzionali*, i *canali di trasmissione* e i *circuiti di telecomunicazione*. Ovviamente la costruzione del circuito richiede uno scambio di informazioni di controllo internamente alla rete.
- **Trasmissione** il trasferimento di segnali da un punto a un altro (da un utente a un altro).
- **Segnalazione di utente/2** l'utente abbassa la cornetta e il circuito viene rilasciato.
- **Gestione** la vera e propria manutenzione della rete di telecomunicazioni: allacciamento dei nuovi utenti; evoluzione tecnologica (sostituzione apparati) riconfigurazione per guasti; monitoraggio prestazioni; controllo apparati.

Il fatto di ricevere un tono di libero non è scontato, agli albori delle TLC, la segnalaione di utente avveniva via voce: l'utente avvisava una centralinista della propria presenza, richiedendo di aprire una comunicazione con uno specifico utente. A quel punto la centralinista tentava di creare un circuito, ovvvero una vera e propria continuità elettrica, che permetteva agli utenti di entrare in comunicazione. I primi selettori automatici entrarono in funzione nel 1868 grazie a Strowger.

**Continuiamo con le definizioni...**
**Mezzo trasmissivo** mezzo fisico in grado di trasportare segnali tra due o più punti. Tali segnali sono grandezze fisiche che rappresentano informazioni; possono essere segnali elettrici, elettromagnetici, ottici, ecc.
**Canale** singolo mezzo trasmissivo o concatenazione di mezzi trasmissivi. Perché concatenazione? Per esempio, quanto effetuiamo una chiamata tramite telefono cellulare, la chiamata viene posta in essere utilizzando l'etere per arrivare al più vicino ripetitore, dal ripetitore viene trasferita con altri mezzi trasmissivi (per esempio una cavi in fibra ottica) fino al ripetitore più vicino al destinatario e da lì di nuovo attraverso l'etere fino al telefono cellulare del destinatario. Quindi quello che era un segnale elettromagnetico viene trasformato in elettrico o luminoso e poi di nuovo eletromagnetico.
**Banda** quantità di dati per unita di tempo [bps = bit al secondo], si trova anche sotto il nome di *bit rate*.
**Capacità di un mezzo trasmissivo** massima velocità trasmissivo [b/s]. La differenza tra capacità e banda è che la banda può essere manipolata (limitata o aumentata) arbitrariamente a seconda della macchina, la capacità è invece un limite intrinseco al mezzo di trasmissione. Essa dipende dalla tecnologia con cui sono realizzati trasmettitore e ricevitore.
**Capacità di un canale** definizione analoga a quella di *capacità del mezzo costruttivo*, ma riguarda l'intero canale; in generale la capacità di un canale è pari all'inferiore tra le capacità dei mezzi di trasmissione di cui il canale è composto.
**Treffico offerto**: quantità di dati per unità di tempo che una sorgente cerca di inviare in rete.
**Traffico smaltito**: anche detto throughput, è la porzione di traffico offerto che riesce ad essere consegnata correttamente alla destinazione. E' immediato che:

- throughput ≤ capacità del canale;
- throughput ≤ traffico offerto.
