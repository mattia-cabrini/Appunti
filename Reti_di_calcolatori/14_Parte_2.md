# Pate 2

> Link layer issue

As far as we learnt, link-layer seems to be quite inefficient. In particular,  we notice;

* inefficient link-redundancy management (we saw the spanning tree protocol);
* filtering database saturation on switches in large-size networks: route aggregation is not possible;
* layer-2 broadcast traffic propagation: useull but it must be limited.

How to we get an efficient route aggregation? Let's assume to have three cities connected together. We'd like to tell the switch to route all the packets directed to Turin on a certain path of the architecture, and on another path when packets are directed to Milan. As far as we know, it is impossible to do this on layer 2.

We introduce the Level Three.

## Network Layer

We need to:

* transport segment from sending to receiving host;
* on sending side encapsulates segments into datagrams;
* on receiving side delivers segmants to transport layer;
* network layer protocols in every host, router;
* router examines header fields in all IP datagrams passing throughput.

### Key functions

#### Forwarding

To move packets from router's input to appropriate output.

#### Routing

> Requires *routing algorithms*.

Determine route taken by packets from source to dest.

### IP?

IP is not the only level three protocol. There are a planty. The point is that IP has been recognized as the most suitable protocol for people's demand.

For sure, it is simplier than alternatives (like ATM or B-ISDN). It is not evolved from telephony but designed to create amert ecosystems and to ensure data exchange between computers.

## Protocols

We can find many protocols in layer three:

* IP protocol;
* ICMP protocol;
* routing protocols.

## IP Packet

<div align="center">
  <img src="src/i12.svg" width="350px" />
</div>

Note that error detection is provided for header only. For payload error[s] the level 4 protocol uses its own checksum.

## IP Addressing

Different level-two networks have to be connected trough a router.

### IP Adress

For IPv4 (Internet Protocol version 4) the address is a 32bit string. Since binary numbers are not practical to ba managed, IPv4 addresses are represented as four dot-separated decomal number each of which is 8bit of the address:

```text
11000000.10101000.00000000.00000001
·····192.·····168.········0.······1
```

An IP address is the concatenation of a subnet part and a host part. The subnet part identifies a subnetwork, the host one a single host in that network.

In a TCP/IP network we assume that it exists a biunivocal correspondence between physycal networks and subnets.

### Special Addresses

* Host part equals to 0: it represent the whole subnet;
* All `1`s: limited broadcast (limited to local net, since we need to libit broadcast, see above);
* Host to all 1s: broadcast for the subnet;
* `127.*.*.*`: loopback (the host itself).

### Hierachical

IP addresses define a hierarchical structure, while MAC doesn't.

## ARP

> How does level two know to which MAC address send an IP packet?

Goal: to find a way to determine what MAC address has a certain IP host.

The idea is send an ARP packet to all level two layers (broadcast) to ask which MAC address a certain IP host has.

So, when a host needs to send a packet to another, firstly it sends an ARP Request to ask the MAC address and then, when it receive the ARP Reply, it asks the level two to send the IP packet to the correct MAC.

Note that ARP Request is a broadcast packet, ARP Reply is a unicast packet.

## Access Another LAN

If a host needs to step outside the LAN, it must do it via a router. In particular each host is configured in order to to know the IP address of the **first stop router**, or **default gateway**.

* * *
Let's skip classful / subnetting / classess.
* * *

## Hierarchical Addressig Plans

How can I tell a router to send all the packets to 180.9.0.0/24 and 180.0.0.0/9 to be forwarded on a certain port without wasting memory?

One way is to not assign IP randomly, but due to geographycal region. A certain region is described with a single prefix.

So an *backbone* router can forward an entire range of addresses to a single port.

### How do IP routing work?

```c
Net Id = IP Address & NetMask
```

Do I need to send a packet throw the gateway?

```c
myNetId <- myIP & myMask.
destNetId <- destIP & destMAsk.

If: myNetId <> destNetId,
Then:
  sendToGateway().
Else:
  sentOnLAN().
```

How do a router select th right port to forward a packet?

In practice, what a router does is to find a match on the routing table and forward the packet on the port defined on the table. If no entry is found, the router send to the sender an ICMP Destination Unreachable packet.

```c
function sendPacket(packet):
  For each entry in routingTable:
    If packet.DestIP & entry.Mask = entry.Network,
    Then:
      sendPacketVia(packet, entry.Port).
      return.
  
  sendIcmpDesUnreachable(packet.Sender, packet.DestIP).
```

### Longest prefix matching

> Actually, when the routing table produces more than one match, we choose the longest mask network.

```c
function sendPacket(packet):
  For each entry in routingTable.SortedByDescendingNetLength:
    If packet.DestIP & entry.Mask = entry.Network,
    Then:
      sendPacketVia(packet, entry.Port).
      return.
  
  sendIcmpDesUnreachable(packet.Sender, packet.DestIP).
```

### Where Do Route Came From?

Routes can be:

* direct: destination network is directly linked to the router;
* static: manually defined and ummutable routing table entry;
* dynamic: route discovered by a routing protocol. 

## Routing Table

<table>
<tr> <td>Type</td>  <td>Destination</td>    <td>Next Hop</td>         <td>Metrics</td> </tr>
<tr> <td>S</td>     <td>10.0.0.0/27</td>    <td>172.16.13.1</td>      <td>1</td> </tr>
<tr> <td>S</td>     <td>192.168.0.0/24</td> <td>172.17.13.1</td>      <td>2</td> </tr>
<tr> <td>D</td>     <td>11.91.3.0/28</td>   <td>192.168.255.253</td>  <td>2</td> </tr>
</table>

The **Next Hop** must be an IP address of a network directly connected to the router.

## DHCP - Dynamic Host Configuration Protocol

> IP Address: how do I get one?

The first thing we can do is to manually configure the network card to have a static IP address.

Of course, users don't even know what an IP Address is, so we can't ask them to configure one.

We introduce the protocol DHCP, which generate the following packets:

* DHCP discover: the host that needs an IP address sends to `255.255.255.255:67`, from source `0.0.0.0:68`, transaction id: `x`;
* DHCP offer: the server sends to broadcast an IP address with the same transaction id `x`;
* DHCP request: the client accept the request, we need this step because in a large network a host could encounter more than one DHCP server. This packet is a broadcast packet to inform every other server that a particular offer has been accepted;
* DHCP ack: needless to say.

Every IP address has a lifetime after which the IP could be either renew by the client or reused if the client is not connected any more.
