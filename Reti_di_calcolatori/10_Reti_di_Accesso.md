# Reti di accesso

Un utente si collega ad una rete sfruttando:

* la rete di accesso:
  * apparati e mezzi trasmissivi che collegano l'utente don il nodo di accesso del gestore di servizi di TLC;
  * residenziale, mobile, istituzionale;
* la rete di trasporto:
  * mezzi trasmissivi appartenenti a uno o più gestori di servizi di TLC e destinati al transito di dati tra due nodi;
  * metropolitane, nazionali, sovranazionali.
* usano tecnologie e standard differenti.

Le reti di accesso sono reti che devono essere vicine all'utente, ecco perché vengono anche dette "ultimo miglio" (o "local loop", in inglese). Le principali tecnologie dinelle reti di accesso sono:

* **DSL**, Digital Subsriber Loop;
* **PON**, Rete di accesso ottica;
* **HFC**, ibride ottiche/coassiali (in disuso, o obsolete);
* Accesso **Broadband Mobile**, reti cellulari.

Tecnologie secondarie:

* reti via radio Wi-MAX;
* ISDN;
* satellitari.

## Nel dettaglio

### DSL

È una famiglia di tecnologia: **xDSL**. La più diffusa è l'*ADSL*, ognuna di queste reti è caratterizzata da una asimmetria tra velocità di upload (uplink) e download (downlink). Velocità massime teoriche:

| Stream     | ADSL     | ADSL2    | ADSL 2+  |
|:-----------|:--------:|:--------:|:--------:|
| Downstream | 6 Mbps   | 8 Mbps   | 24 Mbps  |
| Upstream   | 1,5 Mbps | 3,5 Mbps | 3,5 Mbps |

La prima implementazione consisteva nell'impiegare un MODEM che convertisse le richieste di rete in segnali sonori da inviare attraverso la rete telefonica. Successivamente, senza cablaggi ex novo, si è passati alla modulazione al di fuori della banda sonora, sempre tramite il trasporto attraverso la rete elettrica. Si noti che nel secondo caso, sullo stesso caso si può trasportare voce e dati per mezzo di modulazione di frequenza.

### PON

Rete in fibra ottica che è complemendo della rete i rame, è sull'ultimo miglio.

### HFC

> Hybrid Fiber Coax

Diffusa nei paesi in cui era in voga la TV via cavo (USA e Nord Europa). La rete è costituita di cavi coassiali e in FO.

### Broadband Mobile

> Rete cellulare

Il principio di base è la copertura di diverse aree del territorio da diverse celle (stazioni radio, termine improprio).

#### Roaming

*def.* Rintracciabilità dell'utente ovunque esso si trovi.

#### Handover

*def.* Continuità della tasmissione dal passagio da una cella a un altra.

#### Struttura

La struttura di una cella è:

* un'antenna (LTE);
* un eNodeB: apparato che riceve i segnali dall'antenna, li trasforma in bit e li trasmette alla rete via cavo, e vice versa;

Cinque tecnologie:

* sistemi sperimentali;
* **1G**: *TACS* e *AMPS*;
* **2G**: *GSM*, *GPRS* ed *EDGE*, $800-900-1800-1900 Mhz$;
* **3G**: *EDGE*, *UMTS*, *HSPA* e *HSPA+*;
* **4G**: *HSPA+*, *LTE* ed *LTE+*, $2,6GHz$;
* **5G**: *LTE+*, $3,6GHz$, $> 20GHz$, 1024-QAM:
  * mMTC: massive Machine-Type Communication;
  * eMBB: enhanced Mobile BroadBand;
  * URLLC: Ultra Reliable Low Latency Communication.

Generalmente sui dispositivi mobili si possono utilizzare tutte le tecnologie. Per i dati mobili si tenderà ad utilizzare LTE+, LTE o inferiori, mentre per le telefonate si tende ad utilizzare GSM, GPRS, EDGE o superiori.
