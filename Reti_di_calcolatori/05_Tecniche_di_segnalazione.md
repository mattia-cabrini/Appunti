# Tecniche di segnalazione

*def.* Si definisce **segnalazione** lo scambio di informazioni che riguardano l'apertura e il controllo di connessioni e la gestione di una rete di telecomunicazione.

Possiamo iniziare a distinguere:

* la **segnalazione d'utente**, ovvero lo scambio di dati tra il terminale dell'utente e il nodo della rete;
* la **segnalazione internodale**, ovvero lo scambio di informazioni tra i nodi; 
* la **segnalazione associata al canale**:
  * **in banda**: la segnalazione e la trasmissione vera e propria avvengono sulla stessa banda (per esempio sulla stessa radiofrequenza);
  * **fuori banda**: la segnalazione e la trasmissione dei dati avvengono su bande differenti (per esempio radiofrequenze diverse);
* **a canale comune**.

## Segnalazione associata a canale

Esiste un acorrispondenza biunivoca tra il canale controllante e il canale controllato. Laddove il canale controllante è il canale su cui avvengono le segnalazioni e il canale controllato è quello su cui vengono effettivamente trasmesse le informazioni.

NB: dal punto di vista logico, il canale controllante e il canale controllato non sono distinti.

Questo tipo di segnalazione è utilizzata, per esempio, nella telefonia mobile.

<div align="center">
	<img src="src/i2.svg" />
</div>

#### Segnalazione a canale comune 
Un canale controlla più canali di informazioni di utente e il canale funziona a pacchetto. 

<div align="center">
	<img src="src/i3.svg" />
</div>
