# Qualità del servizio

L'implementazione della rete prevede la definizione delle caratteristiche secondo le quali le informazioni sono emesse dalle sorgenti nell'ambito di un servizio di telecominicazione.

**Esempio**
Un operatore telefonico offre ai clienti 10GB/mese. Certo 10GB sono chiari, ma con quale qualità? A 56kbps il servizio potrà essere utilizzato in un certo modo, a 20Mbps a un altro!

#### Problema di progetto

Richieste date:

* richieste di servizio;
* qualità del servizio.

Richieste da determinare: risorse necessarie. Queste sono da determinare perché dipendono:

* dal parco di utenti;
* dal numero di canali disponibili;

E impattano su:

* capacità complessiva della rete;
* numero di canali impiegati;
* numero di nodi attivi;
* altro... .

#### Problema di analisi

Date:

* richieste di servizio;
* risorse disponibili.

Determinare:

* qualità del servizio (a volte abbreviata QoS).

* * *
**Coffee break**
Per svolgere il processo di analisi occorrono modelli matematici rivolti allo studio delle richieste di servizio, alla descrizione dell'interazione tra le attività e le risorse e a calcolare la qualità del servizio. Tali modelli non vegono affrontati in questo corso, ma in corsi specialistici.
* * *

### Le sorgenti

La qualità del servizio viene erogata anche a seconda delle sorgenti analogiche in oggetto. Distinguiamo le sorgenti:

* **analogiche** caratterizzate dalle loro caratteristiche spettrali;
* **digitali**, **numberiche** o **numerizzate** che si caratterizzano dalla velocità di cifra e dalla loro impulsività.
Possono essere:

  * a *velocità costante*, o *constant bitrate* [CBR];
  * a *velocità variabile*, *variable bitrate* [VBR].

#### CBR

* velocità (b/s) e dimensione del dato;
* durata (media stimata);
* processo di generazione delle trasmissioni (più chiamate la sera o il pomeriggio).

#### VBR

* velocità di picco (b/s);
* velocità (b/s);
  * OPPURE
* grado di intermittenza = <velocità di picco>/<velocità media>;
* durata (s);
* processi di generazione delle trasmissioni.

### Indici di qualità

Con la premessa che diversi tipi di informazione richiedono ala rete prestazioni diverse, alcuni indici di qualità possono essere:

* ritardo (valor medio, percentile, tempo reale);
* velocità;
* probabilità di errore;
* probabilità di perdita;
* probabilità di blocco.

* * *

**Esempio**
Telefonia (CBR):

* ritardo massimo tollerabile: 0.1s;
* velocità: 64kb/s;
* probabilità d'errore: 1% o poco più;
* probabilità di blocco: bassa.

E-mail (VBR):

* ritardo massimo tollerabile: diversi minuti;
* velocità: bassa;
* probabilità d'errore: trascurabile;
* probabilità di blocco: trascurabile.

* * *
