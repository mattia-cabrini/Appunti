# Transport Layer

Two main protocol: **TCP** and **UDP**.

## TCP

> To be sure that packets reach their destinatination.

We'd like to achieve:

* reliable data transfer: did packets arrive to the destination?;
* flow control: if packets continue to be rejected we can adjust windows dimension accordingly;
* congestion.

UDP is a connectionless protocol, it sends packets without knowing if they arrived to destination.

TCP is a connecion-oriented protocol. Sender and receiver must send each other signaling packets before sending data. When signaling packets have been sent the connection is *open* and the end devices can transfer packets and acknowledgements.

The idea of layer four is to split application data into many packet in order to not exceed MTU.

**Service not available** It is important to remember that level four can't guarantee bandwidth or delay time.

## Multiplexing & Demultiplexing

> Level four can multiplex and demultiplex.

Level four protocols can multiplex and demultiplex using a port. Each packet is sent not only to a device `192.168.0.1` but also to specific port on that device, the port is identified by a 16 bit integer, e.g.: `80`. Device and port togheter represent the destination of a packet, this joint is calld a *socket*: `192.168.0.1:80`. The port is needed to identify which process on destination device the packet is directed to.

## Internet Corporation for Assigned Names and Numbers

> Well-known ports.

## TCP: Reliability

### Overview

* Point-To-Point;
* Reliably in-order byte stream;
* Pipelined;
* Full duplex data: bi-directional data flow in th same connection;
* Connection-Oriented;
* Flow-Control.

<div align="center">
    <img src="src/i14.png" />
</div>

Note that the acknowledgment number is not a counter of packets, but the amount of byte received.

### Flow Control & Piggybacking

See Chapter 3.

### Timeout and Estimated RTT

#### Method 1

$$
    timeout = eRTT = (1 - \alpha)\cdot eRTT + \alpha\cdot sampleRTT
$$

#### Method 2

$$
    devRTT = (1 - \beta)\cdot devRTT + \beta\cdot|sampleRTT-eRTT|
$$

$$
    timeout = eRTT + 4 \cdot devRTT
$$

#### Usually (according to statistical analysis)

$$
    \alpha;\; \beta = 0,125;\; 0,25.
$$

### Handshake

> From the lessons' slides

<div align="center">
    <img src="src/i15.png" width="500px" />
</div>

### Closing

> From the lessons' slides

<div align="center">
    <img src="src/i16.png" width="430px" />
</div>

## TCP: Congestion Control

> TCP works fine just as it is, BUT THIS IS NOT ENAUGH FOR US!

The idea now, is that it would be fine to adjust transmission rate if the network is overloaded.

We introduce the **end-to-end congestion control**.

If timeout are triggered or duplicated acknowledgements are received, sender and receiver deduce that the network is approaching a state of congestion.

What alogorithms do is to **addictive increas** the transmission window. In particular, a device increase the window size linearly.

We are increasing $W_T$; If a timeout is triggered we do **multiplicative decrease**. Since the device deduces a congestion is likely to happen soon, it cut th $W_T$ by a half.

<div align="center">
    <img src="src/i17.svg" width="430px" />
</div>

**A conservative approach** When the connection starts the device cannot know if the network is overloaded or not. So that the device starts with $W_T = 1 \cdot MSS$ and then increases exponentially instead of linear in order to approach the congestion window size as soon as possible. When a timeout is triggered or a duplicated acknowledgement is detected, the $W_T$ is cut and then it makes the $W_T$ growing linearly.
