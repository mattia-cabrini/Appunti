# Multimedia Networking

Three application types:

* Streaming stored;
* Streaming live;
* Conversational: **V**oice or **V**ideo over **IP** (VoIP).

## Stored

### UDP

* Constant rate;
* Short playout delay;
* Error recovery: application-level, time-permitting;
* May not go through firewalls.

### HTTP

* Rate fluctuates due to TCP congestion control;
* Error recovery on tranport-level;
* Suitable to nat traversal.

### DASH: **D**ynamic **A**daptive **S**treaming over **H**TTP

* Server:
  * File sent in chunks;
  * Each chunk is stored, encoded at different rates.
  * Introduces **manifest file** that provides to the client URLs for different chunks.
* Client:
  * Periodically measures server-to-client bandwidth;
  * Consulting manifest, request one chunk at a time:
    * Chooses maximum coding rate sustainable given current bandwidth;
    * Can choose different coding rates at different points in time (depending on available bandwidth at time).

## Live

Main requirements:

* Low delays;
* Proper throughput;
* Small packet loss ratio to guarantee proper video quality.

Solutions:

* UDP-based: better for delay and throughput, worse for loss recovery;
* TCP-based: better for loss recovery, really worse for daly and throughput:
  * DASH.

## CDN

**Challenge** To stream content to hundred of thousands of simultaneous users.

**Option I** Single, large "mega-server":

* single point of failure;
* point of network congestion;
* long path to distant clients;
* multiple copies of video sent over outgoing link.

**Option II** Store/Serve multiple copies of video at multiple geographically distributed sites:

* enter deep: push CDN server deep into many access networks;
* bring home: smaller number of larger clusters near (but not within) access networks.

## VoIP

**Motivations** Added-value services cost reduction, free calls for users.

**Main requirements** Low quality end-to-end delay, needed to maintain "conversational" aspect.

**Session initialization** How does callee advertise IP address, port number, encoding algorithms.

## RTP

This protocol is suitable to send audio sample over IP.

RTP packet struckture is designed specifically to carry audio or video data.

Since we don't want to use TCP, we cannot provide automatic packet retransmission or congestion control, nevertheless we can provide RTP over UDP with some additional data in order to recognize the packet sequnce.

> This packet that I am reading follows or precedes the one I did already consume.

Since this protocol is supposed to carry real time multimedia streams, which includes old telephone protocol, it is provided with a payload type:

<table>

<tr> <td>Code</td>  <td>Codec</td> </tr>

<tr> <td>0</td>     <td>PCM</td> </tr>
<tr> <td>3</td>     <td>GSM</td> </tr>
<tr> <td>7</td>     <td>LPC</td> </tr>
<tr> <td>26</td>    <td>Motion JPEG</td> </tr>
<tr> <td>31</td>    <td>H.261</td> </tr>
<tr> <td>33</td>    <td>MPEG2 Video</td> </tr>

</table>
