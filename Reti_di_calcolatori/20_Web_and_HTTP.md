# Web and HTTP

## First things first

A web page is a set of objects. Objet can be an HTML file, an image, a file, etc.. An HTML-script can include several references to other objects.

Each object is adrresed to via a URL.

```
https://it.wikipedia.org/wiki/Hypertext_Transfer_Protocol
```

* `https`: HTTP over SSL, the protocol;
* `://`: a separator;
* `it.wikipedia.org`: a domain name or TCP socket;
* `/`: a separator;
* `wiki/Hypertext_Transfer_Protocol`: a query string (which page am I looking for?).

## Peculiarity

HTTP:

* uses TCP;
* is stateless;
* can be persistent (multiple objects can be retrieved over the same connection) or not-persistent (one connection, one object);

## HTTP Request

```http
GET /wiki/Hypertext_Transfer_Protocol HTTP/2
Host: it.wikipedia.org
User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:89.0) Gecko/20100101 Firefox/89.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: it-IT,it;q=0.8,en-US;q=0.5,en;q=0.3
Accept-Encoding: gzip, deflate, br
Referer: https://www.google.com/
DNT: 1
Connection: keep-alive
Cookie: WMF-Last-Access-Global=11-Jun-2021; WMF-Last-Access=11-Jun-2021; itwikiwmE-sessionTickLastTickTime=1623423000634; itwikiwmE-sessionTickTickCount=9; GeoIP=IT:25:Milan:45.47:9.20:v4; itwikimwuser-sessionId=<censored>; itwikiel-sessionId=8dbcde9b4bfcb1fa5cd3
Upgrade-Insecure-Requests: 1
Cache-Control: max-age=0
TE: Trailers
```
