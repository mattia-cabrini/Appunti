#!/bin/sh

ls $1 | grep .md | cut -f 1 -d "." > tmp

while read row
do
	cat $1/$row.md | script/replace_latex.py $1/src/ $1/src/ > _$row.md
	rm $1/$row.md
done < tmp
rm tmp
