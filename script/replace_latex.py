#!/usr/bin/python3

from os import popen
import sys, hashlib

def writeFormula(seq, folder, prefix, type):
    n = hashlib.sha256(bytearray(seq, 'ASCII')).hexdigest()

    f = open("temp.tex", "w+")
    f.write(seq)
    
    p = popen(f"tex2svg \"{seq}\" > {folder + n}.svg")

    if type == 1:
        sys.stdout.write(f"<img src=\"{prefix + n}.svg\" />")
    else:
        sys.stdout.write(f"<div align=\"center\"><img src=\"{prefix + n}.svg\" /></div>")

    return

def main():
    if len(sys.argv) < 3:
        sys.stderr.write("Arguments count < 3.\n")
        exit(1)

    folder = sys.argv[1]
    prefix = sys.argv[2]

    S_NONE = 0
    S_E = 1
    S_UNDEF = 2
    S_DOL = 3
    S_DOLDOL = 4
    S_DD_OUT = 5

    S_NOW = S_NONE
    S_PREV = S_NONE

    seq = "" # Equation
    type = 0 # 1 = "$"; 2 = "$$".
    i = 0

    src = sys.stdin.read(1)
    while src != "":
        if S_NOW == S_NONE:
            # sys.stdout.write(src)
            if src == "\\":
                S_PREV = S_NOW
                S_NOW = S_E
                sys.stdout.write(src)
            elif src == "$":
                S_PREV = S_NOW
                S_NOW = S_UNDEF
                seq = ""
            else:
                sys.stdout.write(src)
        
        elif S_NOW == S_E:
            if S_PREV == S_DOL or S_PREV == S_DOLDOL or S_PREV == S_UNDEF:
                seq = seq + src
            else:
                sys.stdout.write(src)

            S_NOW = S_PREV
            S_PREV = S_E

        elif S_NOW == S_UNDEF:
            if src == "$":
                S_PREV = S_NOW
                S_NOW = S_DOLDOL
            elif src == "\\":
                S_PREV = S_NOW
                S_NOW = S_E
                seq = src
            else:
                S_PREV = S_NOW
                S_NOW = S_DOL
                seq = src

        elif S_NOW == S_DOL:
            if src == "$":
                S_PREV = S_NOW
                S_NOW = S_NONE
                writeFormula(seq, folder, prefix, 1)
            elif src == "\\":
                S_PREV = S_NOW
                S_NOW = S_E
                seq = seq + src
            else:
                seq = seq + src

        elif S_NOW == S_DOLDOL:
            if src == "$":
                S_PREV = S_NOW
                S_NOW = S_DD_OUT
            elif src == "\\":
                S_PREV = S_NOW
                S_NOW = S_E
                seq = seq + src
            else:
                seq = seq + src

        elif S_NOW == S_DD_OUT:
            if src == "$":
                S_PREV = S_NOW
                S_NOW = S_NONE
                writeFormula(seq, folder, prefix, 2)
            else:
                sys.stderr.write(f"Unexpected token '{src}' at index {i}.\n")
                exit(1)

        src = sys.stdin.read(1)
        i = i + 1

main()