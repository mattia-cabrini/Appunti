#!/bin/bash

cmark=/Users/mattia/Downloads/cmark/build/src/cmark

ls $1 | grep .md | cut -f 1 -d "." > tmp

while read row
do
	orig="$1/$row"
	newf="$1/_$row"
	$cmark --unsafe "$orig.md" > "$newf.html"
	rm "$orig.md"
	mv "$newf.html" "$orig.html"
done < tmp

rm tmp
