# Appunti

Gli appunti sono scritti da me, per me; alcune schede potrebbero essere pi&ugrave; o meno precise a seconda della mia utilit&agrave;. Non fate riferimento a questo repository come a un libro o un pacchetto di dispense, ma per quello che &egrave;: un blocco di appunti.

Si suppone che gli appunti siano consultati con Visual Studio Code (1<sup>st</sup> class citizen) o con Joplin. Per VS Code &egrave; suggerita l'installazione di:

* [Markdown All in One](https://open-vsx.org/extension/yzhang/markdown-all-in-one), ID: `yzhang.markdown-all-in-one`;
* [markdownlint](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint), ID: `davidanson.vscode-markdownlint`.